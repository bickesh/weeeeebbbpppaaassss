// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  apiUrl: "http://192.168.1.14:5500/",
  // apiUl:"http://epaas.fssai.gov.in/backend/",
  uploadUrl: "http://localhost:5500/uploadNabl/",
  // uploadUrl1: "http://localhost:5500/uploadManufacturing/"

  // apiUrl:"https://fssai.demosl-02.rvsolutions.in/backend/api/",
  // uploadUrl:"https://fssai.demosl-02.rvsolutions.in/backend/"


  // apiUrl:"http://tsg-pms.demosl-02.rvsolutions.in/backend/api/",
  // uploadUrl:"http://tsg-pms.demosl-02.rvsolutions.in/backend/"
  // apiUrl:"http://117.239.182.92:8080/api/",
  // uploadUrl:"http://117.239.182.92:8080/"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
