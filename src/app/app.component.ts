import { Component, Renderer2 } from '@angular/core';
import { Router,NavigationStart,NavigationEnd } from '@angular/router';

import { CommonService } from './services/common.service';

import { navigationConstants } from "src/app/constants/navigationConstant";
import { validationMessage } from "src/app/constants/validationMessage";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'webepaas';
  showheader:boolean=false;
  showMenu:boolean=false;
  showFooter:boolean=false;
  mainClass:string='';

  constructor(
    private renderer: Renderer2,
    public router:Router,
    private commonService:CommonService,
  ){
    router.events.forEach((event) => {
      if(event instanceof NavigationEnd) {
        let currentUrl=router.url;
        let currentUrlArr=currentUrl.split('/');
        let mainpath=currentUrlArr[1];
        if(mainpath=='' || mainpath=='login' || mainpath=='reset-password' || mainpath=='register'){
          this.showheader=false;
          this.showMenu=false;
          // if(mainpath=='login'){
            this.mainClass='login-box';
            this.renderer.addClass(document.body, 'login-page');
            this.renderer.removeClass(document.body, 'sidebar-mini');
            this.renderer.removeClass(document.body, 'layout-navbar-fixed');
          // }
        } else {
          this.mainClass='wrapper';
          this.renderer.removeClass(document.body, 'login-page');
          this.renderer.addClass(document.body, 'sidebar-mini');
          this.renderer.addClass(document.body, 'layout-navbar-fixed');
          // this.renderer.addClass(document.body, 'sidebar-collapse');

          this.showheader=true;
          this.showMenu=true;
          // let token=this.commonService.getToken();
          // if(token==null || token==undefined || token==''){
          //   this.commonService.notification(validationMessage.toasterClass.error, validationMessage.common.tokenInvalid);
          //   this.commonService.clearSession();
          //   this.commonService.redirectToPage(navigationConstants.LOGIN);
          // }
        }
      }
      // NavigationEnd
      // NavigationCancel
      // NavigationError
      // RoutesRecognized
    });
  }

  ngOnInit(): void {
  }

}
