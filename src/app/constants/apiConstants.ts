export const APIConstant = {
  DUMMY:'v1/dummy',

  // configurations
  STATES:'v1/master/state',
  DISTRICT:'v1/master/district/{stateID}',
  TYPEMASTER:'v1/master/typemaster',
  GET_CONFIGURATION:'v1/master/configuration',
  POST_CONFIGURATION:'v1/master/configuration',
  GET_TYPE_DETAILS:"v1/master/tyedetails/{typeMasterCode}",

  // Auth and user URL
  REGISTER: 'saveuserdetails',
  LOGIN: 'v1/login',
  FBO_LOGIN: 'v1/fboLogin',
  // FBO_LOGIN_GET_DATA: 'api/v1/fboLogin',
  OTP_SEND: 'sendOTP',
  OTP_VERIFIED: 'v1/otp_verified',
  CHANGE_PASSWORD: 'v1/changepassword',
  FORGET_PASSWORD: 'v1/forgetpassword',
  UPDATE_PASSWORD: 'v1/updatepassword/{tokenNumber}',
  USER_DETAILS:'v1/details',
  APP_CONFIG:'v1/congigurations',
  USER_KEY_DETAIL:'v1/resetpasswordcheck/{tokenNumber}',
  USER_LIST:'v1/users',
  POST_USER:'v1/users',
  DELETE_USER:'v1/users/{userID}',
  USER_PERMISSION:'v1/permission/users/{userID}',
  POST_USER_DETAILS:'v1/user/profile',
  POST_USER_PERMISSION:'v1/permission/userpermission/{userID}',
  POST_USER_PERMISSION_RESET:'v1/permission/userpermissionreset/{userID}',


  ROLE_OFFICER_DATA:'v1/master/roles/roleOfficerData',

  // dashboard
  GET_DASHBOARD_RPT:'v1/dashboard',
  GET_DASHBOARD_MODEL_RPT:'v1/datewisereport',


  DASHBOARD_DASHBOARD_COUNT:'v1/dashboard/admin/allDashboardCount',
  GET_DASHBOARD_ADMIN_DASHBOARD_COUNT:'v1/dashboard/admin/dashboardCount',
  GET_DASHBOARD_ADMIN_DASHBOARD_DETAIL:'v1/dashboard/admin/dashboardDetail',
  GET_DASHBOARD_APPLICATION_HISTORY:'v1/dashboard/admin/applicationHistory',
  GET_DASHBOARD_LIST_DETAIL:'v1/dashboard/dashboardListDetail',
  APPLICATION_PROCESS:'v1/dashboard/admin/applicationProcess',
  RECOMENDEND_PROCESS:'v1/dashboard/admin/applicationProcessRec',
  // reports
  GET_DATE_WISE_RPT:'v1/reports/datewise',
  GET_TIMEWISE_RPT:'v1/reports/timewise',
  GET_SMS_STATUS_RPT:'v1/reports/sms-status',
  GET_TIMEWISE_FAILED_RPT:'v1/reports/timewise-failed',
  GET_COWIN_STATUS_RPT:'v1/reports/cowin-status',
  GET_COWIN_MODEL_RPT:'v1/reports/cowin-modal',
  GET_COWIN_MODEL_DETAILS_RPT:'v1/reports/cowin-modal-details',
  GET_COWIN_MODEL_DETAILS_HISTORY:'v1/reports/cowin-modal-details-history',

  // role master
  GET_ROLE_MASTER:'v1/master/roles',
  POST_ROLE_MASTER:'v1/master/roles',
  DELETE_ROLE_MASTER:'v1/master/roles/{roleID}',
  GET_ROLE_PERMISSION:'v1/permission/roles/{roleID}',
  POST_ROLE_PERMISSION:'v1/permission/rolespermission/{roleID}',
  GET_ELEMENT_PERMISSION:'v1/permission/elements/{moduleID}/{subModuleID}/{permissionID}',
  POST_ELEMENT_PERMISSION:'v1/permission/permissionelement/{permissionID}',

  // mdoule master
  GET_MODULE_MASTER:'v1/master/modules',
  POST_MODULE_MASTER:'v1/master/modules',
  DELETE_MODULE_MASTER:'v1/master/modules/{moduleID}',

  // subModule master
  GET_SUBMODULE_MASTER:'v1/master/submodules',
  POST_SUBMODULE_MASTER:'v1/master/submodules',
  DELETE_SUBMODULE_MASTER:'v1/master/submodules/{submoduleID}',

    // element master
    GET_ELEMENT_MASTER:'v1/master/elements',
    POST_ELEMENT_MASTER:'v1/master/elements',
    DELETE_ELEMENT_MASTER:'v1/master/elements/{elementID}',

    //  client project master
    GET_PROJECT:'v1/project/project',
    POST_PROJECT:'v1/project/project',
    DELETE_PROJECT:'v1/project/project/{projectID}',

    // project campaign
    GET_CAMPAIGN:'v1/project/campaign',
    POST_CAMPAIGN:'v1/project/campaign',
    DELETE_CAMPAIGN:'v1/project/campaign/{campaignID}',

    // project campaign template
    GET_TEMPLATE:'v1/project/template',
    POST_TEMPLATE:'v1/project/template',
    DELETE_TEMPLATE:'v1/project/template/{templateID}',
    GET_PROJECT_CAMPAIGN:'v1/project/project-campaign/{projectID}',
    // NORMAL_SMS_URL:'http://117.239.183.111/SMSSend/sendSmsNormal',
    // OTP_SMS_URL:'http://117.239.183.111/SMSSend/sendSmsOTP',

    NORMAL_SMS_URL:'http://192.168.11.12:8081/SMSSend/sendSmsNormal',
    OTP_SMS_URL:'http://192.168.11.12:8081/SMSSend/sendSmsOTP',


  // FBO_LOGIN:'https://foscos.fssai.gov.in/gateway/extranet/thirdpartyapi/licensevalidation'
  // FBO_LOGIN:'https://fcstraining.fssai.gov.in/gateway/extranet/thirdpartyapi/licensevalidation'
    //  FBO_LOGIN:  'https://fcstraining.fssai.gov.in/gateway/extranet/thirdpartyapi/licensevalidation',
    //  FBO_LOGIN:'https://fctest.fssai.gov.in/gateway/extranet/thirdpartyapi/fbovalidation/',
  // https://fcstraining.fssai.gov.in/gateway/extranet/thirdpartyapi/licensevalidation/10015051001281

  //For Add Application Form

  POST_APPLICATION_FORM:'updateFboDetails',
  GET_APPLICATION_DETAIL:'v1/application-form-get',
  GET_APPLICATION_PDF: 'v1/getApplicationDetailforPdf',
  POST_INGREDIENT_DETAILS:'updateIngredient',
  POST_ADDITIVE_DETAILS:'updateAdditive',

  POST_RAZORPAY_ORDER:'v1/razorpayCreateOrder',
  RAZORPAY_RESPONSE_STORE: 'v1/razorpayResponseStore',
  GET_DASHBOARD_ADMIN_DASHBOARD_DETAIL_FOR_INVOICE:'v1/dashboard/admin/dashboardDetailForInvoice',

};
