export const customStorage = {
  sessionKeys: {
    token: 'token',
    state: 'state',
    log_request_method: "log_request_method",
    max_login_attempt: "max_login_attempt",
    pageLength: "pageLength",
    token_validity: "token_validity",
    user: "user",
    permissions: "permissions",
    dashboardData: "dashboardData",
    reportFromDate: "reportFromDate",
    reportToDate: "reportToDate",
    reportType: "reportType",
    reportData: "reportData",
    smsCat: "smsCat",
    statusTypeCat: "statusTypeCat",
    dataTypeCat: "dataTypeCat",
    currentApplication: "current_application",
  },
  localKeys: {
    token: 'token'
  }
};
