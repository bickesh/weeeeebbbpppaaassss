import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

import { CommonService } from "src/app/services/common.service";
import { validationMessage } from "src/app/constants/validationMessage";

@Injectable({
    providedIn: "root"
  })
export class RequestInterceptor implements HttpInterceptor {
  validationMessage=validationMessage;
    constructor(
      private router: Router,
      private commonService: CommonService,
    ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = this.commonService.getToken();
        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }
        // if (!request.headers.has('Content-Type')) {
        //     request = request.clone({ headers: request.headers.set('Content-Type', 'application/json;charset=UTF-8') });
        // }
        request = request.clone({ headers: request.headers.set('Accept', '*/*') });

        const DISABLE_ORIGIN_FOR = [
          "https://infolnet.fssai.gov.in/infolnetv2/common/getAdditiveList",
          "https://infolnet.fssai.gov.in/infolnetv2/common/foodcategory/3"
        ];

        if(DISABLE_ORIGIN_FOR.includes(request.url) ){   
          request = request.clone({ headers: request.headers.set('Authorization', '') }); 
        }else{ 
        request = request.clone({ headers: request.headers.set('Access-Control-Allow-Origin', '*') });
        }
        // request = request.clone({ headers: request.headers.set('Connection', 'keep-alive') });
        // request = request.clone({ headers: request.headers.set('Accept-Encoding', 'gzip, deflate, br') });
        // request = request.clone({ headers: request.headers.set('Cache-Control', 'no-cache') });
        // request = request.clone({url: request.url.replace("http://", "https://")});

        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                  if(event.body && event.body.message){
                    this.commonService.notification(this.validationMessage.toasterClass.success, event.body.message);
                  }
                }
                return event;
            }),
            catchError((error) => {
                let handled: boolean = false;
                let showMsg: boolean = true;
                if (error instanceof HttpErrorResponse) {
                  if (error.error instanceof ErrorEvent) {
                    // console.error("Error Event");
                  } else {
                    console.log(`error status : ${error.status} ${error.statusText}`);
                    switch (error.status) {
                      case 401:      //unauthorise
                        this.commonService.logout();
                        this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.common.invalidUser);
                        handled = true;
                        break;
                      case 403:     //forbidden
                        this.commonService.logout();
                        this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.common.forbiddenAccess);
                        handled = true;
                        break;
                      case 500:     //server error
                      this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.common.applicationError);
                        handled = false;
                        showMsg=false;
                      break;
                    }
                  }
                  } else {
                    // console.error("Other Errors");
                  }

                  if (handled) {
                    console.log('return back ');
                    return of(error);
                  } else {
                    if(error.error && error.error.message && showMsg){
                      this.commonService.notification(this.validationMessage.toasterClass.error, error.error.message);
                    }
                    return throwError(error);
                  }

                // console.log('error intercepter')
                // console.error(error);
                // return throwError(error);
              })
        );
    }
}
