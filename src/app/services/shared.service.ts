import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  private permissions = new BehaviorSubject<any>({});
  globalPermissions = this.permissions.asObservable();

  private configStatus = new BehaviorSubject<any>({});
  globalConfigStatus = this.configStatus.asObservable();

  constructor() { }

  sharePermissionData(permissionData){
    this.permissions.next(permissionData);
  }

  shareConfigStatus(status){
    this.configStatus.next(status);
  }
}
