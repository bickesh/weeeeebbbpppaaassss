import { Component, OnInit, Renderer2 } from '@angular/core';

import { CommonService } from 'src/app/services/common.service';

import { navigationConstants } from "src/app/constants/navigationConstant";
import { ApiserviceService } from 'src/app/services/apiservice.service';

import { APIConstant } from "src/app/constants/apiConstants";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  thickMenu:boolean=true;

  constructor(
    private renderer: Renderer2,
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
  ) {
  }

  ngOnInit(): void {

  }

  toggleMenu(){
    if(this.thickMenu==true){
      this.thickMenu=false;
      this.renderer.addClass(document.body, 'sidenav-toggled');
    } else {
      this.thickMenu=true;
      this.renderer.removeClass(document.body, 'sidenav-toggled');
    }
  }
  logout(){
    this.commonService.logout();
  }

}
