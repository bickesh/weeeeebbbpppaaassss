import { Component, OnInit } from '@angular/core';
import { ApiserviceService } from 'src/app/services/apiservice.service';

import { CommonService } from 'src/app/services/common.service';
import { SharedService } from 'src/app/services/shared.service';
import { SessionStorageService } from 'src/app/services/session-storage.service';

import { APIConstant } from "src/app/constants/apiConstants";
import { customStorage } from "src/app/constants/storageKeys";
import { environment } from "src/environments/environment";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  private uploadUrl = environment.uploadUrl;

  directUrl:string='';
  parentUrl:string='';
  childUrl:string='';
  userData:any;
  permissionData:any;
  menuData:any={};
  profilePicURL:string='assets/images/default.jpg';

  constructor(
    private commonService: CommonService,
    private sharedService: SharedService,
    private apiserviceService: ApiserviceService,
    private sessionStorageService: SessionStorageService,
  ) { 
    this.sharedService.globalPermissions.subscribe(dataShared => {
      this.getMenuData();
    }); // if page not relaoded after login    
  }

  ngOnInit(): void {
    this.getMenuData();
    //console.log(this.menuData);
  }
  getMenuData(){
    const token: string = this.commonService.getToken();        
    if (token) {
      this.menuData=JSON.parse(this.sessionStorageService.getData(customStorage.sessionKeys.permissions));
      this.userData=JSON.parse(this.sessionStorageService.getData(customStorage.sessionKeys.user));      
      if(this.userData.userimage!=null && this.userData.userimage!='' && this.userData.userimage!=undefined){
        this.profilePicURL=this.uploadUrl+'public/'+this.userData.userimage;
        // this.profilePicURL=this.uploadUrl+this.userData.userimage;
      }
    }
  }

  singleMenu(menuItem){
    this.directUrl=menuItem;
    this.parentUrl=null;
  }
  treeMenu(menuItem){
    if(this.parentUrl==menuItem){
      this.parentUrl=null;
    } else {
      this.parentUrl=menuItem;
    }
  }
  childMenu(menuItem,parentMenu){
    this.childUrl=menuItem;
    this.parentUrl=parentMenu;    
  }
  logout(){
    this.commonService.logout();
  }

}
