import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.css']
})
export class BreadcrumbComponent implements OnInit {
  @Input() breadcrumbData: any;
  breadcrumbDataLength=0
  constructor() { }
  
  ngOnInit(): void {
    this.breadcrumbDataLength=this.breadcrumbData.urls.length-1;
    
  }

}
