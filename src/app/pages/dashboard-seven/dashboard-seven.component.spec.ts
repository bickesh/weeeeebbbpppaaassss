import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardSevenComponent } from './dashboard-seven.component';

describe('DashboardSevenComponent', () => {
  let component: DashboardSevenComponent;
  let fixture: ComponentFixture<DashboardSevenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardSevenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardSevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
