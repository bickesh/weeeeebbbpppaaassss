import { OnInit } from '@angular/core';
import { of } from 'rxjs';
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import {IngredientsServicesService} from "src/app/services/ingredients-services.service";
import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { environment } from "src/environments/environment";
import {HttpClient} from "@angular/common/http";
import { ActivatedRoute } from '@angular/router';
import { Component, ViewChild, ElementRef } from '@angular/core';  
// import * as jsPDF from 'jspdf';
//import { jsPDF } from 'jspdf';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit {
  @ViewChild('content', {static:true}) el: ElementRef<any>; 
 
  validationMessage = validationMessage;
  current_application_id='0';
  isIngredientsFormSubmitted = false;
  isApplicationFormStep1Submitted = false;
  isApplicationFormStep2Submitted = false;
  isApplicationFormStep3Submitted = false;
  isApplicationFormStep4Submitted = false;
  isApplicationFormStep5Submitted = false;
  isApplicationFormStep6Submitted = false;
  isApplicationFormStep7Submitted = false;
  isApplicationFormStep8Submitted = false;
  fileDocumentPreview: any;
  nabl_certificate_file_url:any;
  ilac_certificate_file_url:any;
  manufacturing_pro_file_url:any;
  consumption_purpose_file_url:any;
  agreement_copy_file_url:any;
  safetyInfo_file_url:any;
  additional_specific_3a_txt4_file_url:any;
  evidenceDemo_file_url:any;
  risk_assessment_review_file_url:any;
  safety_studies_conducted_file_url:any;
  ingredient_evidence_file_url:any;
  addSpecific_file_url:any;

  addSpecific_3c_1_file_url:any;
  addSpecific_3c_2_file_url:any;
  addSpecific_3c_3_file_url:any;
  addSpecific_3c_4_file_url:any;

  risk_assessment_file_url:any;
  toxicity_studies_file_url:any;
  allorgenicity_file_url:any;
  geographical_area_file_url:any;
  consumption_quantity_file_url:any;

  receipt_copy_file_url:any;
  receipt_copy_file1_url:any;
  receipt_file1_url:any;
  receipt_file3_url:any;
  receipt_copy_file2_url:any;
  receipt_file4_url:any;
  safety_recognised_url:any;  
  additional_specific_3d_template_file_url:any;
  additional_specific_4d_other_file_url:any;

  btnSpinner:boolean=false;
  oldFileDocumentPreview: any;
  fileDocumentPreviewName: any[];
  fileDocumentPreview1: any;
  fileDocumentPreviewName1: any[];
  fileDocumentPreview2: any;
  fileDocumentPreviewName2: any[];
  fileDocumentPreview2manufacturing_pro:any;
  fileDocumentPreviewName2manufacturing_pro: any[];
  fileDocumentPreview3: any;
  fileDocumentPreviewName3: any[];
  fileDocumentPreview4: any;
  fileDocumentPreviewName4: any[];
  fileDocumentPreview5: any;
  fileDocumentPreviewName5: any[];
  fileDocumentPreview6: any;
  fileDocumentPreviewName6: any[];
  fileDocumentPreview7: any;
  fileDocumentPreviewName7: any[];
  fileDocumentPreview8: any;
  fileDocumentPreviewName8: any[];
  fileDocumentPreview9: any;
  fileDocumentPreviewName9: any[];
  fileDocumentPreview10: any;
  fileDocumentPreviewName10: any[];
  fileDocumentPreview11: any;
  fileDocumentPreviewName11: any[];
  fileDocumentPreview12: any;
  fileDocumentPreviewName12: any[];
  fileDocumentPreview13: any;
  fileDocumentPreviewName13: any[];
  fileDocumentPreview14: any;
  fileDocumentPreviewName14: any[];
  fileDocumentPreview15: any;
  fileDocumentPreviewName15: any[];
  fileDocumentPreview16: any;
  fileDocumentPreviewName16: any[];
  fileDocumentPreview17: any;
  fileDocumentPreviewName17: any[];
  fileDocumentPreview18: any;
  fileDocumentPreviewName18: any[];
  fileDocumentPreview19: any;
  fileDocumentPreviewName19: any[];

  fileDocumentPreviewSafety: any; 
  fileDocumentPreviewNameSafety: any[];

  fileDocumentPreviewSafetyInfo: any;
  fileDocumentPreviewNameSafetyInfo: any[];

  fileDocumentPreviewEvidenceDemo: any;
  fileDocumentPreviewNameEvidenceDemo: any[];
  fileDocumentPreviewAddSpecific: any;
  fileDocumentPreviewNameAddSpecific: any[]; 
  fileDocumentPreviewAddSpecific_3c_1: any;
  fileDocumentPreviewNameAddSpecific_3c_1: any[];
  fileDocumentPreviewAddSpecific_3c_2: any;
  fileDocumentPreviewNameAddSpecific_3c_2: any[];
  fileDocumentPreviewAddSpecific_3c_3: any;
  fileDocumentPreviewNameAddSpecific_3c_3: any[];
  fileDocumentPreviewAddSpecific_3c_4: any;
  fileDocumentPreviewNameAddSpecific_3c_4: any[];

  fileDocumentPreviewadditional_specific_4d_other_file: any;
  fileDocumentPreviewNameadditional_specific_4d_other_file: any[];

  fileDocumentPreviewadditional_specific_3d_template_file: any;
  fileDocumentPreviewNameadditional_specific_3d_template_file: any[];


  myFiles: any[];
  projectForm: any[];
  userData:any;
  applicationData: any[];
  ingredient_ddl: any = '';
  addd = {};
  standarizeData: any = 'Yes';
  viewDataPDF: any;
  viewApplicationForOther: any;
  viewAuthorisedPersonOther: any;
  viewFoodNameOther: any;
  viewRegulatoryStatus: any;
  viewGenus: any;
  words:any; 
  wordsAnyOther:any; 
  SafetyInfoToolTip2 = 'Arrange all risk documents chronological  start from the latest';
  ApplicationForFoodType:any;
  
  closeModal: string;

  public uploadUrl = environment.uploadUrl+"public/";
  private apiUrl = environment.apiUrl;
  IngredientsStandarizeResult = {};

  stepZeroData:any; 
  breadcrumbObj:any;

  // breadcrumbObj = {
  //   title: 'Application Details',
  //   classBreadcrumb: 'fa-dashboard',
  //   urls: [
  //     { urlName: 'Home', linkURL: '/dashboard-' }
  //   ]
  // }

  savePdf(){
    // let content=this.content.nativeElement;
    let pdf = new jsPDF('p','pt','a2'); 
    pdf.setFontSize(10);
    pdf.text("content", 15, 15);
    // pdf.text("hello rajesh", 10,10);
    pdf.html(this.el.nativeElement,{
      callback: (pdf)=>{
        pdf.save("fssai_applicationform.pdf");
      }
    }); 
    
  }

  public openPDF(): void {
    let DATA: any = document.getElementById('content');  
    html2canvas(DATA).then((canvas) => {
      var imgData = canvas.toDataURL('image/png');
      var margin = 2;
      var imgWidth = 210 -2*margin;
      var pageHeight = 295;
      var imgHeight = canvas.height * imgWidth / canvas.width;
      var heightLeft = imgHeight;
      //window.jsPDF = window.jspdf.jsPDF;
      var doc = new jsPDF('p', 'mm' );
      var position = 0;

      doc.addImage(imgData, 'PNG', margin, position, imgWidth, imgHeight);
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
          position = heightLeft - imgHeight;
          doc.addPage();
          doc.addImage(imgData, 'PNG', margin, position, imgWidth, imgHeight);
          heightLeft -= pageHeight;
      }
      doc.save( 'fssai_applicationform.pdf');
    }); 
  }



  ingredientsData:any= [];
  additivesData:any=[];
  constructor(
    private router: Router,
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    public sessionStorageService: SessionStorageService,
    private spinner: NgxSpinnerService,
    private http:HttpClient,
    private ingredientsService: IngredientsServicesService,
    private activatedRoute: ActivatedRoute,

  ) {
    console.log('cunstructor from app-details');
    this.userData=JSON.parse(this.sessionStorageService.getData('user')); 

    this.breadcrumbObj = {
      title: 'Application Details',
      classBreadcrumb: 'fa-dashboard',
      urls: [
        { urlName: 'Home', linkURL: '/dashboard-'+ this.userData.role_id  }
      ]
    }
  }

   

  ngOnInit(): void {

    this.current_application_id = this.activatedRoute.snapshot.url[1].path;  
    //this.current_application_id = this.activatedRoute.snapshot.params.param; 
    if(this.current_application_id==undefined){
      this.sessionStorageService.setData(customStorage.sessionKeys.currentApplication,JSON.stringify('')
      );
    }
    this.getDataPDF();

  }


  getDataPDF() { 
  if(this.userData.id){ 
    let params = {
      "user_id": this.userData.id,
      "user_role":this.userData.role_id,
      "application_form_id": this.current_application_id,
      // "application-data":[this.applicationFormStep1,this.applicationFormControlStep2, this.applicationFormControlStep4]
    };
    this.spinner.show('dashboardDetailLoader');
    
    this.apiserviceService.get(APIConstant.GET_APPLICATION_PDF,params).subscribe((res) => { 
      if (res.data.dataGet) {
        // if(this.userData.id != res.data.dataGet.user_id){
        //   this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
        // }
        this.viewDataPDF = res.data.dataGet;  
      } else {
       this.applicationData = null;
       this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);

      }
      this.spinner.hide('dashboardDetailLoader');
    }, error => {
      this.applicationData = null;

    });
  }

  }

  }
