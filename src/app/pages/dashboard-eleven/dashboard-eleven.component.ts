//DASHBOARD DEO
import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import * as internal from 'events';


@Component({
  selector: 'app-dashboard-eleven',
  templateUrl: './dashboard-eleven.component.html',
  styleUrls: ['./dashboard-eleven.component.css']
})
export class DashboardElevenComponent implements OnInit {

  breadcrumbObj={
    title:'CEO Dashboard',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Home',linkURL:'/dashboard-11'}
    ]
  }
  validationMessage=validationMessage;
  rejectRemarks:any;
  closeResult: string;
  paginateConfig:any;
  permissionUser:any;
  roleOfficerDataAd= [];
  statusTypeVar:any;
  dataTypeVar:any;
  totalCoast:any;
  officerNameDec='';
  officerRecomended='';
  approveRemarks='';
  adminDashboardList = [];
  DashboardListData = [];
  roleOfficerData = [];
  adminDashboardApplicationHistory=[];
  pendingApplication=0;
  approveApplication=0;
  rejectApplication=0;
  userData:any;
  applicationListLabel="Application Details";
  applicationColumnLabel="Current State";
  applicationProcessStatus = [];

  officerName:any='';
  officerRemarks:any='';
  queryRemarks:any='';
  user_id:any='';
  application_id:any='';
  DashboardDataPaginationLoad:any='';
  searchTxt='';
  panelBgColor = 'bg-info';
officerRoleId:any=0;
statusLabel:any='';
  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private sessionStorageService: SessionStorageService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.permissionUser=this.commonService.getPermission('dashboard-11',null);
    this.setPermission();
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
    if(this.paginateConfig.itemsPerPage == null){ 
      this.paginateConfig={ id: 'server', itemsPerPage: 10, currentPage:1, totalItems:0};
    } 
  

this.userData=JSON.parse(this.sessionStorageService.getData('user'));
this.officerRoleId =this.userData.role_id
    // console.log(this.userData.role_id);
  }

  setPermission(){
    // console.log(this.permissionUser);
    if(!this.permissionUser.isview){
      // this.commonService.redirectToPage(navigationConstants.UNAUTHORISE);
    }
  }
  ngOnInit(): void {
    this.getDashboardCount();
    this.sessionStorageService.setData(
      customStorage.sessionKeys.dashboardData,
      JSON.stringify("all")
    );
    this.getDashboardDetail(this.paginateConfig.currentPage,'all');
    this.getRoleOfficerDetail(5);
    this.getRoleOfficerDetailAd(6);

  }
  getDashboardCount(){
    const params =
    {
      'status_id':this.userData.dashboardPermission.status_id,
      'role_id':this.userData.role_id,
      'assgined_to':this.userData.id,
      'current_user_id':this.userData.id
    };
    this.spinner.show('dashboardCounterLoader');
    this.apiserviceService.post(APIConstant.DASHBOARD_DASHBOARD_COUNT,params,{}).subscribe((res)=>{
      if(res.data){
        this.DashboardListData = res.data.dataGet.dashboardStatus;
      } else {
        this.approveApplication=0;
        this.rejectApplication=0;
        this.pendingApplication=0;
      }
      this.spinner.hide('dashboardCounterLoader');
    },error=>{
      this.spinner.hide('dashboardCounterLoader');
    });
  }

  detailApplication(applicationTypeData){
    // console.log(applicationTypeData);
    let applicationType = applicationTypeData.assigned_role_id
    if(applicationType==null){
      applicationType=0;
    }
    this.applicationListLabel=applicationTypeData.label_name +' Application Detail';
    this.panelBgColor=applicationTypeData.bg_color;
    this.statusLabel  = applicationTypeData.label_name;
    this.applicationColumnLabel="Action By";
    this.sessionStorageService.setData(
        customStorage.sessionKeys.dashboardData,
        JSON.stringify(applicationTypeData)
      );
    this.getDashboardDetail(1,applicationTypeData);

  }
  getDashboardDetail(page: number,type='all',searchText=""){
    this.DashboardDataPaginationLoad=JSON.parse(this.sessionStorageService.getData('dashboardData'));
    console.log(this.DashboardDataPaginationLoad);
    let params = {};
    if(this.DashboardDataPaginationLoad!='all'){
       params = {
         'page':page,
         'role_id':this.userData.role_id,
         'pageLength':this.paginateConfig.itemsPerPage,
         'application_process_code':this.DashboardDataPaginationLoad.app_code,
         'current_assign_role_id':this.DashboardDataPaginationLoad.assigned_role_id,
         'current_assign_user_id':this.userData.id,
         'assign_by_role_id':this.DashboardDataPaginationLoad.assigned_by_role_id,
        //  'assign_by_user_id':this.DashboardDataPaginationLoad.,
         'status_id':this.DashboardDataPaginationLoad.status_id,
         "searchText":searchText,

        };
    }else{
      // alert(4);
      params = {
        'page':page,
        'role_id':this.userData.role_id,
        'pageLength':this.paginateConfig.itemsPerPage,
        'application_process_code':this.DashboardDataPaginationLoad.app_code,
        'current_assign_role_id':this.DashboardDataPaginationLoad.assigned_role_id,
        'current_assign_user_id':this.userData.id,
        'assign_by_role_id':this.DashboardDataPaginationLoad.assigned_by_role_id,
        // 'status_id':this.DashboardDataPaginationLoad.status_id,
        "searchText":searchText
      };
    }
    this.spinner.show('dashboardDetailLoader');
    this.apiserviceService.post(APIConstant.GET_DASHBOARD_LIST_DETAIL,params,{}).subscribe((res)=>{
      if(res.data){
        this.adminDashboardList = res.data.dataGet.totalDataDetail;
        // alert(res.data.currentPage);
        this.paginateConfig.currentPage=res.data.currentPage;
        this.paginateConfig.totalItems=res.data.totalItems;
        this.paginateConfig.startIndex = res.startIndex;

      } else {
        this.adminDashboardList = [];
      }
      this.spinner.hide('dashboardDetailLoader');
    },error=>{
      this.spinner.hide('dashboardDetailLoader');
      this.adminDashboardList = [];
    });
  }
  viewApplicationHistory(userObj,modelContent){
    this.spinner.show('dashboardHistoryLoader');
    // alert(4);
    const params = {'applicationId':userObj.application_no};
    this.apiserviceService.post(APIConstant.GET_DASHBOARD_APPLICATION_HISTORY,params,{}).subscribe((res)=>{

      if(res.data){
        this.spinner.hide('dashboardHistoryLoader');
        this.adminDashboardApplicationHistory = res.data.dataGet.applicationHistory;
         this.modalService.open(modelContent,{
          size:"lg",
          backdrop : 'static',
          windowClass : "modalClass-700",
          keyboard : false,
          ariaLabelledBy: "modal-basic-title"
        }).result.then(result => {
            this.closeResult = `Closed with: ${result}`;
          },reason => {
            this.closeResult = `Dismissed `;

          }
        );

      } else {
        this.spinner.hide('dashboardHistoryLoader');
        this.adminDashboardApplicationHistory = [];
      }

    },error=>{
      this.spinner.show('dashboardHistoryLoader');
      this.adminDashboardApplicationHistory = [];
    });

  }
  viewData(userObj,modelContent){
    console.log(userObj);
    this.application_id = userObj.application_no;
    this.user_id = userObj.user_id;
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.officerName = '';
        this.officerRemarks = '';  
        this.officerNameDec = '';
      },reason => {
        this.closeResult = `Dismissed `;
        this.officerName = '';
        this.officerRemarks = '';  
        this.officerNameDec = '';
      }
    );


  }
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }

  getRoleOfficerDetail(role_id){
    let params = {'role_id':role_id};
    this.spinner.show('dashboardDetailLoader');
    this.apiserviceService.post(APIConstant.ROLE_OFFICER_DATA,params,{}).subscribe((res)=>{
      if(res.data){
        this.roleOfficerData = res.data.dataGet  .totalDataDetail;

      } else {
        this.roleOfficerData = null;
      }
      this.spinner.hide('dashboardDetailLoader');
    },error=>{
      this.spinner.hide('dashboardDetailLoader');
      this.roleOfficerData = null;
    });
  }
  getRoleOfficerDetailAd(role_id){
    let params = {'role_id':role_id};
    this.spinner.show('dashboardDetailLoader');
    this.apiserviceService.post(APIConstant.ROLE_OFFICER_DATA,params,{}).subscribe((res)=>{
      if(res.data){
        this.roleOfficerDataAd = res.data.dataGet  .totalDataDetail;

      } else {
        this.roleOfficerDataAd = null;
      }
      this.spinner.hide('dashboardDetailLoader');
    },error=>{
      this.spinner.hide('dashboardDetailLoader');
      this.roleOfficerData = null;
    });
  }
  applicationProcess(){
    // alert(this.userData.id); return;
    let params = {
      'assigned_to_role_id':5,
      'assigned_to_user_id':this.officerName,
      'remarks':this.officerRemarks ,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':1
    };
    this.spinner.show('forwardApplicationLoader');
    this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
      if(res){
        // this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
        this.modalService.dismissAll();
        this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        this.getDashboardCount();
      } else {
        this.applicationProcessStatus = null;
      }
      this.spinner.hide('forwardApplicationLoader');
    },error=>{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    });
  }
  recomendendProcess(){
    // alert(this.user_id); return;
    let params = {
      'assigned_to_role_id':6,
      'assigned_to_user_id':this.officerNameDec,
      'remarks':this.approveRemarks,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':this.officerRecomended
    };
    this.spinner.show('forwardApplicationLoader');
    this.apiserviceService.post(APIConstant.RECOMENDEND_PROCESS,params,{}).subscribe((res)=>{
      if(res){
        // this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
        this.modalService.dismissAll();
        this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        this.getDashboardCount();
      } else {
        this.applicationProcessStatus = null;
      }
      this.spinner.hide('forwardApplicationLoader');
    },error=>{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    });
  }
  applicationProcessTest(){
    this.spinner.show('forwardApplicationLoader');
    let params = {
      'assigned_to_role_id':5,
      'assigned_to_user_id':this.officerName,
      'remarks':this.officerRemarks ,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':1
    };
    let reponseFromPrcess = this.commonService.applicationProcessNew(params);
    if(reponseFromPrcess==true){
      this.modalService.dismissAll();
      this.getDashboardDetail(this.paginateConfig.currentPage,'all');
      this.getDashboardCount();
      this.spinner.hide('forwardApplicationLoader');
    }else{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    }


  }
  queryProcess(){
    let params = {
      'assigned_to_role_id':10,
      'assigned_to_user_id':this.user_id,
      'remarks':this.queryRemarks ,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':4 // Revert
    };
    this.spinner.show('forwardApplicationLoader');
    this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
      if(res){
        this.modalService.dismissAll();
        this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        this.getDashboardCount();
      } else {
        this.applicationProcessStatus = null;
      }
      this.spinner.hide('forwardApplicationLoader');
    },error=>{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    });
  }
  approveProcess(){
    // alert(this.user_id); return;
    let params = {
      'assigned_to_role_id':6,
      'assigned_to_user_id':this.userData.id,
      'remarks':this.approveRemarks ,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':2 // Approve
    };
    this.spinner.show('forwardApplicationLoader');
    this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
      if(res){
        // this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
        this.modalService.dismissAll();
        this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        this.getDashboardCount();
      } else {
        this.applicationProcessStatus = null;
      }
      this.spinner.hide('forwardApplicationLoader');
    },error=>{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    });
  }

  rejectProcess(){
    let params = {
      'assigned_to_role_id':11,
      'assigned_to_user_id':this.userData.id,
      'remarks':this.rejectRemarks ,
      'application_id':this.application_id,
      'assigned_by_user_id':this.userData.id,
      'assigned_by_role_id':this.userData.role_id,
      'status':3 // Reject
    };
    this.spinner.show('forwardApplicationLoader');
    this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
      if(res){
        // this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
        this.modalService.dismissAll();
        this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        this.getDashboardCount();
      } else {
        this.applicationProcessStatus = null;
      }
      this.spinner.hide('forwardApplicationLoader');
    },error=>{
      this.spinner.hide('forwardApplicationLoader');
      this.applicationProcessStatus = null;
    });
  }
  searchApplication(){
    // alert(this.searchTxt);
    this.getDashboardDetail(1,'all',this.searchTxt);
  }
  resetApplication(){
    this.searchTxt = '';
    this.getDashboardDetail(1,'all',this.searchTxt);
  }
}
