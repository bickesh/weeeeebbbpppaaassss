import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardElevenComponent } from './dashboard-eleven.component';

describe('DashboardElevenComponent', () => {
  let component: DashboardElevenComponent;
  let fixture: ComponentFixture<DashboardElevenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardElevenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardElevenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
