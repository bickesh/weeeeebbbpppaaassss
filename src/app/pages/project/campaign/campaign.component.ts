import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css']
})
export class CampaignComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Campaign',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master campaign',linkURL:'/master/manage-campaign'}
    ]
  }	
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  
  campaignForm: FormGroup;
  formEditable:boolean=true;
  isCampaignFormSubmitted = false;
  btnSpinner:boolean=false;
  campaign:any;
  userList:any;
  projects:any;
  userData:any;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: 5, currentPage:1, totalItems:0};
    this.userData=JSON.parse(this.sessionStorageService.getData('user'));
  }

  ngOnInit(): void {
    this.campaignForm = this.formBuilder.group({
      id:['',[]],
      user_id:['',[Validators.required]],
      project_id:['',[Validators.required]],
      campaign_name:['',[Validators.required]],
      campaign_code:['',[Validators.required]],
      campaign_id_nic:['',[]],
    });
    this.getUsers();
    this.getProjects();
    this.getCampaignList(this.paginateConfig.currentPage);    
  }

  get campaignFormControl() { return this.campaignForm.controls; }

  setUserData(){
    if(this.userData.role_id==1){
      this.campaignForm.get('user_id').setValue(null);
    } else {
      this.campaignForm.get('user_id').setValue(this.userData.id);
    }
  }

  getUsers(){
    const apiURL = APIConstant.USER_LIST;
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.userList=res.data.dataGet;
    },error=>{
      this.userList=[];
    });    
  }
  
  getProjects(){
    this.apiserviceService.get(APIConstant.GET_PROJECT,{}).subscribe((res)=>{
      if(res.data.dataGet){
        this.projects=res.data.dataGet;
      } else {
        this.projects=[];
      }
    },error=>{
      this.projects=[];
    });
  }

  getCampaignList(page: number){
    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage};
    // if(this.userData.role_id==2){
    //   params['user_id']=this.userData.id;
    // }

    this.apiserviceService.get(APIConstant.GET_CAMPAIGN,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;     
      this.paginateConfig.totalItems=res.data.totalItems;     
      this.campaign=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;     
      this.paginateConfig.totalItems=0;     
      this.campaign=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  addCampaign(modelContent) {
    this.setUserData();
    this.formEditable=true;
    this.campaignForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetCampaign();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetCampaign();
      }
    );
  }

  onCampaignFormSubmit(){    
    this.isCampaignFormSubmitted=true;
    if (this.campaignForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.campaignForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_CAMPAIGN,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getCampaignList(this.paginateConfig.currentPage);
      },error=>{
        this.btnSpinner=false;
      });
    }

  }

  deleteCampaign(campaignObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_CAMPAIGN.replace("{campaignID}",campaignObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getCampaignList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });      
    // }, function () {  
    //   return;
    // }); 
  }

  viewCampaign(campaignObj,modelContent){    
    this.formEditable=false;
    this.campaignForm.disable();
    this.campaignForm.patchValue(campaignObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetCampaign();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetCampaign();
      }
    );
  }

  editCampaign(campaignObj,modelContent){
    this.formEditable=true;
    this.campaignForm.enable();
    this.campaignForm.patchValue(campaignObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetCampaign();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetCampaign();
      }
    );
  }
  resetCampaign(){ 
    this.isCampaignFormSubmitted=false;
    this.campaignForm.reset();
    this.campaignForm.setErrors(null);
    this.campaignForm.markAsPristine();
    this.campaignForm.markAsUntouched();
    this.campaignForm.updateValueAndValidity();
  }

}
