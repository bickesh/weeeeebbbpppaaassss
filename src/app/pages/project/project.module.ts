import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { ProjectRoutingModule } from './project-routing.module';
import { SharedModule } from 'src/app/modules/shared.module';

import { ProjectsComponent } from './projects/projects.component';
import { CampaignComponent } from './campaign/campaign.component';
import { TemplatesComponent } from './templates/templates.component';

@NgModule({
  declarations: [
    ProjectsComponent,
    CampaignComponent,
    TemplatesComponent,
  ],
  imports: [
    CommonModule,
    ProjectRoutingModule,
    SharedModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ProjectModule { }
