import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { formatDate } from '@angular/common';

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-templates',
  templateUrl: './templates.component.html',
  styleUrls: ['./templates.component.css']
})
export class TemplatesComponent implements OnInit {
  breadcrumbObj = {
    title: 'Manage Template',
    classBreadcrumb: 'fa-dashboard',
    urls: [
      { urlName: 'Master Template', linkURL: '/master/manage-template' }
    ]
  }
  validationMessage = validationMessage;
  closeResult: string;
  paginateConfig: any;

  searchForm: FormGroup;
  templateForm: FormGroup;
  formEditable: boolean = true;
  isTemplateFormSubmitted = false;
  btnSpinner: boolean = false;
  btnSrchSpinner: boolean = false;
  campaigns: any = [];
  userList: any;
  projects: any;
  templates: any;
  userData: any;
  templetData: any;
  smsMobile: any;
  smsUrl: any;
  methodType: any = [];
  apiMethod = [
    { "id": 'JSON', "name": "JSON" },
    { "id": 'GET', "name": "GET" },

  ];
  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private sessionStorageService: SessionStorageService,
    private modalService: NgbModal
  ) {
    this.paginateConfig = { id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage: 1, totalItems: 0 };
    this.userData = JSON.parse(this.sessionStorageService.getData('user'));
  }

  ngOnInit(): void {
    this.templateForm = this.formBuilder.group({
      id: ['', []],
      user_id: ['', [Validators.required]],
      project_id: ['', [Validators.required]],
      campaign_id: ['', [Validators.required]],
      params: ['', [Validators.required]],
      message: ['', [Validators.required]],
      dlt: ['', []],
      Language: ['', [Validators.required]],
      Is_Otp: ['', [Validators.required]],
      Content_Id: ['', []],
    });
    this.searchForm = this.formBuilder.group({
      cid: ["", []],
      dlt: ["", []],
      searchProject: ["", []],
    });
    this.getUsers();
    this.getProjects();
    this.getCampaign();
    this.getTemplateList(this.paginateConfig.currentPage);

  }

  get templateFormControl() { return this.templateForm.controls; }

  setUserData() {
    if (this.userData.role_id == 1) {
      this.templateForm.get('user_id').setValue(null);
    } else {
      this.templateForm.get('user_id').setValue(this.userData.id);
    }
  }

  getUsers() {
    const apiURL = APIConstant.USER_LIST;
    this.apiserviceService.get(apiURL, {}).subscribe((res) => {
      this.userList = res.data.dataGet;
    }, error => {
      this.userList = [];
    });
  }

  getProjects() {
    this.apiserviceService.get(APIConstant.GET_PROJECT, {}).subscribe((res) => {
      if (res.data.dataGet) {
        this.projects = res.data.dataGet;
      } else {
        this.projects = [];
      }
    }, error => {
      this.projects = [];
    });
  }

  getCampaign() {
    this.apiserviceService.get(APIConstant.GET_CAMPAIGN, {}).subscribe((res) => {
      if (res.data.dataGet) {
        this.campaigns = res.data.dataGet;
      } else {
        this.campaigns = [];
      }
    }, error => {
      this.campaigns = [];
    });
  }

  getData() {
    this.spinner.show('dataTableLoader');
    let qry = this.searchForm.getRawValue();
    let params = { 'page': 1, 'pageLength': this.paginateConfig.itemsPerPage };
    let qryData = { ...qry, ...params };
    this.apiserviceService.get(APIConstant.GET_TEMPLATE, qryData).subscribe((res) => {
      this.paginateConfig.currentPage = res.data.currentPage;
      this.paginateConfig.totalItems = res.data.totalItems;
      if (res.data.dataGet) {
        this.templates = res.data.dataGet;
      } else {
        this.templates = [];
      }
      this.spinner.hide('dataTableLoader');
    }, error => {
      this.paginateConfig.currentPage = 0;
      this.paginateConfig.totalItems = 0;
      this.templates = [];
      this.spinner.hide('dataTableLoader');
    });
  }

  getTemplateList(page: number) {
    this.spinner.show('dataTableLoader');
    let params = { 'page': page, 'pageLength': this.paginateConfig.itemsPerPage };
    this.apiserviceService.get(APIConstant.GET_TEMPLATE, params).subscribe((res) => {
      this.paginateConfig.currentPage = res.data.currentPage;
      this.paginateConfig.totalItems = res.data.totalItems;
      if (res.data.dataGet) {
        this.templates = res.data.dataGet;
      } else {
        this.templates = [];
      }
      this.spinner.hide('dataTableLoader');
    }, error => {
      this.paginateConfig.currentPage = 0;
      this.paginateConfig.totalItems = 0;
      this.templates = [];
      this.spinner.hide('dataTableLoader');
    });
  }

  addTemplate(modelContent) {
    this.setUserData();
    this.formEditable = true;
    this.templateForm.enable();
    this.modalService.open(modelContent, {
      size: "lg",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
      this.resetTemplate();
    }, reason => {
      this.closeResult = `Dismissed`;
      this.resetTemplate();
    }
    );
  }

  onTemplateFormSubmit() {
    this.isTemplateFormSubmitted = true;
    if (this.templateForm.invalid) {
      return;
    } else {
      this.btnSpinner = true;
      let submitData = this.templateForm.getRawValue();
      this.apiserviceService.post(APIConstant.POST_TEMPLATE, submitData, {}).subscribe(res => {
        this.btnSpinner = false;
        this.modalService.dismissAll();
        this.getTemplateList(this.paginateConfig.currentPage);
      }, error => {
        this.btnSpinner = false;
      });
    }

  }
  onGetTemplateParam(type) {
    if(type=='GET'){

    let message = this.templateForm.get('message').value;

    let val = message.split("<");
    let paramString = '';
    if (val.length > 0) {
      for (var i = 1; i < val.length; i++) {
        let strcheckArray = (val[i].split(">"));
        paramString += '"<' + strcheckArray[0] + '>"' + '="temp' + strcheckArray[0] + '",';
      }
      paramString = paramString.slice(0, -1);
      this.templateForm.patchValue({
        params: paramString
      });
    }
  }else{
    let message = this.templateForm.get('message').value;
    let val = message.split("<");
    let paramString = '{';
    if (val.length > 0) {
      for (var i = 1; i < val.length; i++) {
        let strcheckArray = (val[i].split(">"));
        paramString += '"<' + strcheckArray[0] + '>"' + ':"temp' + strcheckArray[0] + '",';
      }
      paramString = paramString.slice(0, -1)+'}';
      this.templateForm.patchValue({
        params: paramString
      });
    }
  }
  }

  deleteTemplate(templateObj) {
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
    this.spinner.show('dataTableLoader');
    const apiURL = APIConstant.DELETE_TEMPLATE.replace("{templateID}", templateObj.id);
    this.apiserviceService.delete(apiURL, {}).subscribe(res => {
      this.spinner.hide('dataTableLoader');
      this.getTemplateList(this.paginateConfig.currentPage);
    }, error => {
      this.spinner.hide('dataTableLoader');
    });
    // }, function () {
    //   return;
    // });
  }

  viewTemplate(templateObj, modelContent) {
    this.formEditable = false;
    this.templateForm.disable();
    this.getCompaigns(templateObj.project_id);
    this.templateForm.patchValue(templateObj);
    this.modalService.open(modelContent, {
      size: "lg",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
      this.resetTemplate();
    }, reason => {
      this.closeResult = `Dismissed `;
      this.resetTemplate();
    }
    );
  }

  editTemplate(templateObj, modelContent) {
    this.formEditable = true;
    this.templateForm.enable();
    this.getCompaigns(templateObj.project_id);
    this.templateForm.patchValue(templateObj);
    this.modalService.open(modelContent, {
      size: "lg",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
      this.resetTemplate();
    }, reason => {
      this.closeResult = `Dismissed `;
      this.resetTemplate();
    }
    );
  }
  resetTemplate() {
    this.isTemplateFormSubmitted = false;
    this.templateForm.reset();
    this.templateForm.setErrors(null);
    this.templateForm.markAsPristine();
    this.templateForm.markAsUntouched();
    this.templateForm.updateValueAndValidity();
  }
  checkTemplate(templetObj, modelContent) {
    this.templetData = templetObj;
    this.smsMobile = '1234567890';
    if (this.templetData.Is_Otp) {
      this.smsUrl = APIConstant.OTP_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
    } else {
      this.smsUrl = APIConstant.NORMAL_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
    }
    this.modalService.open(modelContent, {
      size: "xl",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
    }, reason => {
      this.closeResult = `Dismissed`;
    }
    );
  }
  onSMSSent() {
    this.apiserviceService.getExt(this.smsUrl, {}).subscribe(res => {
      console.log(res);
    }, error => {
      console.log(error);

    });
  }
  updateMobile() {
    if (this.templetData.Is_Otp) {
      this.smsUrl = APIConstant.OTP_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
    } else {
      this.smsUrl = APIConstant.NORMAL_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
    }

    // this.smsUrl='http://117.239.183.111/SMSSend/sendSmsOTP?campaign_id='+this.templetData.campaignID+'&cid='+this.templetData.id+'&parm='+this.templetData.params+'&mobile='+this.smsMobile+'&sender_id=NHPSMS&OtherApplicationRequestTime='+formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
  }
  onProjectChange(projectObj: any) {
    console.log(projectObj);
    this.getCompaigns(projectObj.id);
  }
  getCompaigns(projectID) {
    let apiURL = APIConstant.GET_PROJECT_CAMPAIGN.replace('{projectID}', projectID);
    this.apiserviceService.get(apiURL, {}).subscribe(res => {
      if (res.data.dataGet) {
        this.campaigns = res.data.dataGet;
      } else {
        this.campaigns = [];
      }
    }, error => {
      this.campaigns = [];
    });
  }
  changeApiMethod() {
    if (this.templetData.Is_Otp) {
      this.smsUrl = APIConstant.OTP_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
    } else {
      if (this.methodType == 'GET') {
        this.smsUrl = APIConstant.NORMAL_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
      }
      else {

        // let value = this.templetData.params.replace(/[^\w:;"=,\s]/gi, '').toLocaleLowerCase();
        // let newVal = value.trim();
        let newVal = this.templetData.params
        // this.smsUrl = 'http://117.239.183.111/SMSSend/sendSmsNormal?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm=' + this.templetData.params + '&mobile=' + this.smsMobile + '&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
        this.smsUrl = APIConstant.NORMAL_SMS_URL+'?campaign_id=' + this.templetData.campaignID + '&cid=' + this.templetData.id + '&parm={'+newVal+'}'+ '&mobile='+this.smsMobile+'&sender_id=' + this.templetData.SmsUserName + '&OtherApplicationRequestTime=' + formatDate(new Date(), 'MM/dd/yyyy h:mm:ss', 'en');
      }

    }
  }
}
