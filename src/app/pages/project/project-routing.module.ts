import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { ProjectsComponent } from './projects/projects.component';
import { CampaignComponent } from './campaign/campaign.component';
import { TemplatesComponent } from './templates/templates.component';

const routes: Routes = [
  {path:'',redirectTo:'manage-project',pathMatch:'full' },
  {path:'manage-project' , component:ProjectsComponent},
  {path:'manage-campaign' , component:CampaignComponent},
  {path:'manage-template' , component:TemplatesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProjectRoutingModule { }