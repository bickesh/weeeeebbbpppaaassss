import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";
import { SessionStorageService } from "src/app/services/session-storage.service";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Project',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Project',linkURL:'/master/manage-project'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;

  projectForm: FormGroup;
  formEditable:boolean=true;
  isProjectFormSubmitted = false;
  btnSpinner:boolean=false;
  projects:any;
  userList:any;
  userData:any;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
    this.userData=JSON.parse(this.sessionStorageService.getData('user'));
  }

  ngOnInit(): void {
    this.projectForm = this.formBuilder.group({
      id:['',[]],
      project_name: ["",[Validators.required]],
      description: ["",[Validators.required]],
      user_id: ["",[Validators.required]],
      campaign_id: ["",[]],
      SmsUserName: ["",[]],
      SmsPin: ["",[]],
      SmsUrl: ["",[]],
      Dlt_entity_id: ["",[]],
      SenderId: ["",[]],
    });
    this.getUsers();
    this.getProjectList(this.paginateConfig.currentPage);
  }

  get projectFormControl() { return this.projectForm.controls; }

  setUserData(){
    if(this.userData.role_id==1){
      this.projectForm.get('user_id').setValue(null);
    } else {
      this.projectForm.get('user_id').setValue(this.userData.id);
    }
  }

  onProjectFormSubmit(){
    this.isProjectFormSubmitted=true;
    if (this.projectForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.projectForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_PROJECT,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getProjectList(this.paginateConfig.currentPage);
      },error=>{
        this.btnSpinner=false;
      });
    }

  }

  getProjectList(page: number){
    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage};
    this.apiserviceService.get(APIConstant.GET_PROJECT,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;
      this.paginateConfig.totalItems=res.data.totalItems;
      this.projects=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;
      this.paginateConfig.totalItems=0;
      this.projects=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteProject(projectObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_PROJECT.replace("{projectID}",projectObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getProjectList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });
    // }, function () {
    //   return;
    // });
  }

  viewProject(projectObj,modelContent){
    this.formEditable=false;
    this.projectForm.disable();
    this.projectForm.patchValue(projectObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetProject();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetProject();
      }
    );
  }

  editProject(projectObj,modelContent){
    this.formEditable=true;
    this.projectForm.enable();
    this.projectForm.patchValue(projectObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetProject();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetProject();
      }
    );
  }

  addProject(modelContent) {
    this.setUserData();
    this.formEditable=true;
    this.projectForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetProject();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetProject();
      }
    );
  }
  resetProject(){
    this.isProjectFormSubmitted=false;
    this.projectForm.reset();
    this.projectForm.setErrors(null);
    this.projectForm.markAsPristine();
    this.projectForm.markAsUntouched();
    this.projectForm.updateValueAndValidity();
  }

  getUsers(){
    const apiURL = APIConstant.USER_LIST;
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.userList=res.data.dataGet;
    },error=>{
      this.userList=[];
    });
  }

}
