import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { PasswordValidator } from 'src/app/customvalidator/password.validator';

import { SessionStorageService } from "src/app/services/session-storage.service";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { SharedService } from "src/app/services/shared.service";
import { ApiserviceService } from "src/app/services/apiservice.service";


import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import * as CryptoJS from 'crypto-js'
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  validationMessage = validationMessage;
  closeResult: string;

  loginForm: FormGroup;
  fbologinForm: FormGroup;

  forgetPassword: FormGroup;
  registerForm: FormGroup;
  isLoginFormSubmitted = false;
  isfboLoginFormSubmitted = false;
  isForgetPasswordSubmitted = false;
  isRegisterFormSubmitted = false;
  btnSpinner: boolean = false;
  btnPasswordSpinner: boolean = false;
  btnRegSpinner: boolean = false;
  myRecaptcha = new FormControl(false);
  isShow: boolean = false;
  loginTitle = 'FBO Login';
  usernamePlaceholder = "Licenese Number";
  loginView = 'FBO';
  constructor(
    private commonService: CommonService,
    private sharedService: SharedService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private sessionStorageService: SessionStorageService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    public router:Router
  ) {
    let token = this.commonService.getToken();
    if (token != null && token != '' && token != undefined) {
      this.commonService.redirectToPage(navigationConstants.DASHBOARD);
    }
  }

  ngOnInit(): void {
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);

    this.loginForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
      captcha: ["", Validators.required],
      captcha1: ["", Validators.required],
      captcha2: ["", Validators.required],
    });
    this.fbologinForm = this.formBuilder.group({
      username: ["", Validators.required],
      password: ["", Validators.required],
      captcha: ["", Validators.required],
      captcha1: ["", Validators.required],
      captcha2: ["", Validators.required],
    });

    this.forgetPassword = this.formBuilder.group({
      email: ["", [Validators.required, Validators.pattern(regExConstant.email)]]
    });
    this.registerForm = this.formBuilder.group({
      name: ["", Validators.required],
      email: ["", [Validators.required, Validators.pattern(regExConstant.email)]],
      password: ["", Validators.required],
      c_password: ["", Validators.required]
    }, { validator: PasswordValidator("password", "c_password") });

    this.fbologinForm.get('captcha1').setValue(Math.floor(Math.random() * 10));
    this.fbologinForm.get('captcha2').setValue(Math.floor(Math.random() * 10));

    // this.confirmDialogService.confirmThis("Are you sure to delete?", function () {
    //   alert("Yes clicked");
    // }, function () {
    //   alert("No clicked");
    // });


  }
  loginType(type) {
    // alert(this.isShow);
    this.isShow = true;
    if (type == 1) {
      // alert(this.isShow);
      this.loginTitle = "FBO Login";
      this.usernamePlaceholder = "Licenese Number";
      this.loginView = 'FBO';
      this.resetFboLogin();
      this.resetLogin();
      this.fbologinForm.get('captcha1').setValue(Math.floor(Math.random() * 10));
      this.fbologinForm.get('captcha2').setValue(Math.floor(Math.random() * 10));
    } else if (type == 2) {
      this.loginTitle = "NON FBO Login";
      this.usernamePlaceholder = "Username";
      this.loginView = 'NON-FBO';
      this.resetFboLogin();
      this.resetLogin();
      this.loginForm.get('captcha1').setValue(Math.floor(Math.random() * 10));
      this.loginForm.get('captcha2').setValue(Math.floor(Math.random() * 10));

    } else {
      this.loginTitle = "Authorities Login";
      this.usernamePlaceholder = "Username";
      this.loginView = 'OFFICIAL';
      this.resetFboLogin();
      this.resetLogin();
      this.loginForm.get('captcha1').setValue(Math.floor(Math.random() * 10));
      this.loginForm.get('captcha2').setValue(Math.floor(Math.random() * 10));

    }
  }
  get loginFormControl() { return this.loginForm.controls; }
  get fbologinFormControl() { return this.fbologinForm.controls; }
  get forgetPasswordControl() { return this.forgetPassword.controls; }
  get registerFormControl() { return this.registerForm.controls; }

  openRegisterModal(modelContent) {
    this.modalService.open(modelContent, {
      size: "md",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
      this.resetRegistration();
    }, reason => {
      this.closeResult = `Dismissed`;
      this.resetRegistration();
    }
    );
  }
  openForgetPasswordModal(modelContent) {
    this.modalService.open(modelContent, {
      size: "md",
      backdrop: 'static',
      windowClass: "modalClass-700",
      keyboard: false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
      this.closeResult = `Closed with: ${result}`;
      this.resetForgetPassword();
    }, reason => {
      this.closeResult = `Dismissed`;
      this.resetForgetPassword();
    }
    );
  }

  onLoginFormSubmit(type) {
    
    this.isLoginFormSubmitted = true;
    
    if (this.loginForm.invalid) {
      return;
    } else if ((this.loginFormControl.captcha1.value + this.loginFormControl.captcha2.value) != this.loginFormControl.captcha.value) {
      this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.login.captchaRequire);
    } else {
      this.spinner.show('loginLoader');
      this.btnSpinner = true;
      let loginData = {
        username: this.loginFormControl.username.value,
        password: this.loginFormControl.password.value,
        type: type
      };
      // const loginData1 = { username: this.loginFormControl.username.value, password: SHA256_password };
      this.apiserviceService.post(APIConstant.LOGIN, loginData, {}).subscribe(response => {});
                      // this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);

                      this.router.navigate([navigationConstants.DASHBOARD_USER]);

      this.apiserviceService.post(APIConstant.LOGIN, loginData, {}).subscribe(response => {

        this.sessionStorageService.setData(
          customStorage.sessionKeys.token,
          response.data.token
        );
        this.sessionStorageService.setData(
          customStorage.sessionKeys.user,
          JSON.stringify(response.data.user)
        );
        this.sessionStorageService.setData(
          customStorage.sessionKeys.permissions,
          JSON.stringify(response.data.permissions)
        );
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.dashboardPermissions,
        //   JSON.stringify(response.data.dashboardPermission)
        // );


        this.sharedService.sharePermissionData(response.data.permissions);

        this.commonService.applicationConfiguration();
        this.sharedService.globalConfigStatus.subscribe(dataShared => {
          if (dataShared == true) {
            this.btnSpinner = false;
            this.spinner.hide('loginLoader');

            switch (response.data.user.role_id) {
              case 1:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD);
                break;
              case 2:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_MEMBER);
                break;
              case 3:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_DEO);
                break;
              case 5:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_ADVISOR);
                break;
              case 6:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_DIRECTOR);
                break;
              case 7:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_ASST_DIRECTOR);
                break;
              case 8:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_TO);
                break;
              case 9:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_CHAIR);
                break;

              case 10:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
                break;
              case 11:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_CEO);
                break;
              case 12:
                this.commonService.redirectToPage(navigationConstants.DASHBOARD_JD);
                break;
              default:
                // this.commonService.redirectToPage(navigationConstants.DASHBOARD_HO);
                this.commonService.redirectToPage(navigationConstants.DASHBOARD);
                break;
            }
          }
        });
        // alert(response.data.user.role_id);
        // alert(response.data.user.role_id);

        // this.commonService.redirectToPage(navigationConstants.DASHBOARD);
      }, error => {
        this.spinner.hide('loginLoader');
        this.btnSpinner = false;
      });
    }

  }
  onFboLoginFormSubmit() {
    // alert(4);

    this.isLoginFormSubmitted = true;
    if (this.fbologinForm.invalid) {
      return;
    } else if ((this.fbologinFormControl.captcha1.value + this.fbologinFormControl.captcha2.value) != this.fbologinFormControl.captcha.value) {
      this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.login.captchaRequire);
    } else {
      this.spinner.show('loginLoader');
      this.btnSpinner = true;
      let loginData = {
        loginId: this.fbologinFormControl.username.value,
        userPwd:this.fbologinFormControl.password.value,

      };
      this.spinner.hide('loginLoader');

      // let encrypted = CryptoJS.HmacSHA256(this.fbologinFormControl.password.value, "$$CHALLENGE");
      // let SHA256_password = CryptoJS.enc.Base64.stringify(encrypted);

      // const fbo_login_data = { loginId: this.fbologinFormControl.username.value, userPwd: SHA256_password };
      this.router.navigate([navigationConstants.DASHBOARD_USER]);
      this.apiserviceService.post(APIConstant.FBO_LOGIN, loginData, {}).subscribe(response => {
        this.spinner.hide('loginLoader');
        this.router.navigate([navigationConstants.DASHBOARD_USER]);

        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.token,
        //   response.data.token
        // );
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.user,
        //   JSON.stringify(response.data.user)
        // );
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.permissions,
        //   JSON.stringify(response.data.permissions)
        // );
        // this.sharedService.sharePermissionData(response.data.permissions);
        // this.commonService.applicationConfiguration();
        // this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
      }, error => {

        this.spinner.hide('loginLoader');
        this.btnSpinner = false;
      });
      // this.spinner.hide('loginLoader');
      // },error=>{


      // });
    }

  }

  onRegisterFormSubmit() {
    this.isRegisterFormSubmitted = true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.btnRegSpinner = true;
      this.apiserviceService.post(APIConstant.REGISTER, this.registerForm.value, {}).subscribe(response => {
        this.btnRegSpinner = false;
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.token,
        //   response.data.token
        //   );
        setTimeout(() => {
          this.modalService.dismissAll();
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.btnRegSpinner = false;
      });
    }
  }

  onForgetPasswordSubmit() {
    this.isForgetPasswordSubmitted = true;
    if (this.forgetPassword.invalid) {
      return;
    } else {
      this.btnPasswordSpinner = true;
      let forgetPasswordData = this.forgetPassword.getRawValue();
      this.apiserviceService.post(APIConstant.FORGET_PASSWORD, forgetPasswordData, {}).subscribe(response => {
        this.btnPasswordSpinner = false;
        this.modalService.dismissAll();
        this.resetForgetPassword();
      }, error => {
        this.btnPasswordSpinner = false;
      });
    }
  }
  resetLogin() {
    this.isLoginFormSubmitted = false;
    this.loginForm.reset();
    this.loginForm.setErrors(null);
    this.loginForm.markAsPristine();
    this.loginForm.markAsUntouched();
    this.loginForm.updateValueAndValidity();
  }
  resetFboLogin() {
    this.isfboLoginFormSubmitted = false;
    this.fbologinForm.reset();
    this.fbologinForm.setErrors(null);
    this.fbologinForm.markAsPristine();
    this.fbologinForm.markAsUntouched();
    this.fbologinForm.updateValueAndValidity();
  }
  resetForgetPassword() {
    this.isForgetPasswordSubmitted = false;
    this.forgetPassword.reset();
    this.forgetPassword.setErrors(null);
    this.forgetPassword.markAsPristine();
    this.forgetPassword.markAsUntouched();
    this.forgetPassword.updateValueAndValidity();
  }
  resetRegistration() {
    this.isRegisterFormSubmitted = false;
    this.registerForm.reset();
    this.registerForm.setErrors(null);
    this.registerForm.markAsPristine();
    this.registerForm.markAsUntouched();
    this.registerForm.updateValueAndValidity();
  }
  onScriptLoad() {
    console.log('Google reCAPTCHA loaded and is ready for use!')
  }

  onScriptError() {
    console.log('Something went long when loading the Google reCAPTCHA')
  }

}
