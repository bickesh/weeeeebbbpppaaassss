

//DASHBOARD DEO
  import { Component, OnInit } from '@angular/core';
  import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
  import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
  import { NgxSpinnerService } from "ngx-spinner";
  import { NgxChartsModule } from '@swimlane/ngx-charts';

  import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
  import { CommonService } from "src/app/services/common.service";
  import { ApiserviceService } from "src/app/services/apiservice.service";
  import { SessionStorageService } from 'src/app/services/session-storage.service';
  import { environment } from "src/environments/environment";

  import { APIConstant } from "src/app/constants/apiConstants";
  import { regExConstant } from 'src/app/constants/regExConstant';
  import { validationMessage } from "src/app/constants/validationMessage";
  import { navigationConstants } from "src/app/constants/navigationConstant";
  import { customStorage } from "src/app/constants/storageKeys";
  import * as internal from 'events';


@Component({
  selector: 'app-dashboard-three',
  templateUrl: './dashboard-three.component.html',
  styleUrls: ['./dashboard-three.component.css']
})
  export class DashboardThreeComponent implements OnInit {

    breadcrumbObj={
      title:'Dashboard DEO',
      classBreadcrumb:'fa-dashboard',
      urls:[
        {urlName:'Home',linkURL:'/dashboard-3'}
      ]
    }
    validationMessage=validationMessage;
    closeResult: string;
    paginateConfig:any;
    permissionUser:any;

    statusTypeVar:any;
    dataTypeVar:any;
    totalCoast:any;

    adminDashboardList = [];
    DashboardListData = [];
    roleOfficerData = [];
    adminDashboardApplicationHistory=[];
    pendingApplication=0;
    approveApplication=0;
    rejectApplication=0;
    userData:any;
    applicationListLabel="Application Details";
    applicationColumnLabel="Current State";
    applicationProcessStatus = [];

    officerName:any='';
    officerRemarks:any='';
    queryRemarks:any='';
    user_id:any='';
    application_id:any='';
    DashboardDataPaginationLoad:any='';
    searchTxt='';
    constructor(
      private commonService: CommonService,
      private apiserviceService: ApiserviceService,
      private sessionStorageService: SessionStorageService,
      private confirmDialogService: ConfirmDialogService,
      private formBuilder: FormBuilder,
      private spinner: NgxSpinnerService,
      private modalService: NgbModal
    ) {
      this.permissionUser=this.commonService.getPermission('dashboard-3',null);
      this.setPermission();

     this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
// this.paginateConfig={ id: 'server', itemsPerPage: 1, currentPage:1, totalItems:0};

this.userData=JSON.parse(this.sessionStorageService.getData('user'));

      // console.log(this.userData.role_id);
    }

    setPermission(){
      // console.log(this.permissionUser);
      if(!this.permissionUser.isview){
        // this.commonService.redirectToPage(navigationConstants.UNAUTHORISE);
      }
    }
    ngOnInit(): void {
      this.getDashboardCount();
      this.sessionStorageService.setData(
        customStorage.sessionKeys.dashboardData,
        JSON.stringify("all")
      );
      this.getDashboardDetail(this.paginateConfig.currentPage,'all');
      this.getRoleOfficerDetail(5);

    }
    getDashboardCount(){
      const params =
      {
        'status_id':this.userData.dashboardPermission.status_id,
        'role_id':this.userData.role_id,
        'assgined_to':this.userData.id
      };
      this.spinner.show('dashboardCounterLoader');
      this.apiserviceService.post(APIConstant.DASHBOARD_DASHBOARD_COUNT,params,{}).subscribe((res)=>{
        if(res.data){
          this.DashboardListData = res.data.dataGet.dashboardStatus;
        } else {
          this.approveApplication=0;
          this.rejectApplication=0;
          this.pendingApplication=0;
        }
        this.spinner.hide('dashboardCounterLoader');
      },error=>{
        this.spinner.hide('dashboardCounterLoader');
      });
    }
    getDashboardDetail(page: number,type='all',searchText=""){
      this.DashboardDataPaginationLoad=JSON.parse(this.sessionStorageService.getData('dashboardData'));
      let params = {};

      if(this.DashboardDataPaginationLoad!='all'){
         params = {'page':page,'pageLength':this.paginateConfig.itemsPerPage,'current_assign_role_id':this.DashboardDataPaginationLoad,'current_assign_user_id':this.userData.id, 'status_id':this.userData.dashboardPermission.status_id,"searchText":searchText};
      }else{
        params = {'page':page,'pageLength':this.paginateConfig.itemsPerPage,'current_assign_user_id':this.userData.id, 'status_id':this.userData.dashboardPermission.status_id,"searchText":searchText};
      }
      this.spinner.show('dashboardDetailLoader');
      this.apiserviceService.post(APIConstant.GET_DASHBOARD_LIST_DETAIL,params,{}).subscribe((res)=>{
        if(res.data){
          this.adminDashboardList = res.data.dataGet  .totalDataDetail;
          this.paginateConfig.currentPage=res.data.currentPage;
          this.paginateConfig.totalItems=res.data.totalItems;

        } else {
          this.adminDashboardList = null;
        }
        this.spinner.hide('dashboardDetailLoader');
      },error=>{
        this.spinner.hide('dashboardDetailLoader');
        this.adminDashboardList = null;
      });
    }
    detailApplication(applicationTypeData){
      // console.log(applicationTypeData);
      let applicationType = applicationTypeData.current_assign_role_id
      if(applicationType==null){
        applicationType=0;
      }
      this.applicationListLabel=applicationTypeData.name +' Application Detail';
      this.applicationColumnLabel="Action By";
      this.sessionStorageService.setData(
          customStorage.sessionKeys.dashboardData,
          JSON.stringify(applicationType)
        );
      this.getDashboardDetail(1,applicationType);

    }

    viewApplicationHistory(userObj,modelContent){
      this.spinner.show('dashboardHistoryLoader');
      const params = {'applicationId':userObj.application_no};
      this.apiserviceService.post(APIConstant.GET_DASHBOARD_APPLICATION_HISTORY,params,{}).subscribe((res)=>{
        this.spinner.hide('dashboardHistoryLoader');
        if(res.data){
          this.adminDashboardApplicationHistory = res.data.dataGet.applicationHistory;
           this.modalService.open(modelContent,{
            size:"lg",
            backdrop : 'static',
            windowClass : "modalClass-700",
            keyboard : false,
            ariaLabelledBy: "modal-basic-title"
          }).result.then(result => {
              this.closeResult = `Closed with: ${result}`;
            },reason => {
              this.closeResult = `Dismissed `;

            }
          );

        } else {
          this.spinner.hide('dashboardHistoryLoader');
          this.adminDashboardApplicationHistory = [];
        }

      },error=>{
        this.spinner.show('dashboardHistoryLoader');
        this.adminDashboardApplicationHistory = [];
      });

    }
    viewData(userObj,modelContent){
      console.log(userObj);
      this.application_id = userObj.application_no;
      this.user_id = userObj.user_id;
      this.modalService.open(modelContent,{
        size:"lg",
        backdrop : 'static',
        windowClass : "modalClass-700",
        keyboard : false,
        ariaLabelledBy: "modal-basic-title"
      }).result.then(result => {
          this.closeResult = `Closed with: ${result}`;
        },reason => {
          this.closeResult = `Dismissed `;

        }
      );


    }
    replaceAll(string, search, replace) {
      return string.split(search).join(replace);
    }

    getRoleOfficerDetail(role_id){
      let params = {'role_id':role_id};
      this.spinner.show('dashboardDetailLoader');
      this.apiserviceService.post(APIConstant.ROLE_OFFICER_DATA,params,{}).subscribe((res)=>{
        if(res.data){
          this.roleOfficerData = res.data.dataGet  .totalDataDetail;

        } else {
          this.roleOfficerData = null;
        }
        this.spinner.hide('dashboardDetailLoader');
      },error=>{
        this.spinner.hide('dashboardDetailLoader');
        this.roleOfficerData = null;
      });
    }

    applicationProcess(){
      // alert(this.userData.id); return;
      let params = {
        'role_id':5,
        'officer_user_id':this.officerName,
        'remarks':this.officerRemarks ,
        'application_id':this.application_id,
        'user_id':this.userData.id,
        'status':6
      };
      this.spinner.show('forwardApplicationLoader');
      this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
        if(res){
          this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
          this.modalService.dismissAll();
          this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        } else {
          this.applicationProcessStatus = null;
        }
        this.spinner.hide('forwardApplicationLoader');
      },error=>{
        this.spinner.hide('forwardApplicationLoader');
        this.applicationProcessStatus = null;
      });
    }
    queryProcess(){
      let params = {
        'role_id':10,
        'officer_user_id':this.user_id,
        'remarks':this.queryRemarks ,
        'application_id':this.application_id,
        'user_id':this.userData.id,
        'status':7
      };
      this.spinner.show('forwardApplicationLoader');
      this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
        if(res){
          this.applicationProcessStatus = res.data.dataGet.totalDataDetail;
          this.modalService.dismissAll();
          this.getDashboardDetail(this.paginateConfig.currentPage,'all');
        } else {
          this.applicationProcessStatus = null;
        }
        this.spinner.hide('forwardApplicationLoader');
      },error=>{
        this.spinner.hide('forwardApplicationLoader');
        this.applicationProcessStatus = null;
      });
    }
    searchApplication(){
      // alert(this.searchTxt);
      this.getDashboardDetail(1,'all',this.searchTxt);
    }
    resetApplication(){
      this.searchTxt = '';
      this.getDashboardDetail(1,'all',this.searchTxt);
    }
  }
