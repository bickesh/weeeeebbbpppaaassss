
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import * as internal from 'events';

@Component({
  selector: 'app-payment-invoice',
  templateUrl: './payment-invoice.component.html',
  styleUrls: ['./payment-invoice.component.css']
})
export class PaymentInvoiceComponent implements OnInit {

  breadcrumbObj = {
    title: 'User Dashboard',
    classBreadcrumb: 'fa-dashboard',
    urls: [
      { urlName: 'Home', linkURL: '/dashboard-10' }
    ]
  }
  validationMessage = validationMessage;
  closeResult: string;
  paginateConfig: any;
  permissionUser: any;

  statusTypeVar: any;
  dataTypeVar: any;
  totalCoast: any;
  dataDashboardList = [];
  adminDashboardApplicationHistory = [];
  pendingApplication = 0;
  approveApplication = 0;
  rejectApplication = 0;
  userData: any;
  applicationListLabel = "Application Details";
  applicationColumnLabel = "Current State";
  officerRoleId: any = 0;
  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private sessionStorageService: SessionStorageService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.permissionUser = this.commonService.getPermission('dashboard-10', null);
    this.setPermission();

    this.paginateConfig = { id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage: 1, totalItems: 0 };
    if (this.paginateConfig.itemsPerPage == null) {
      this.paginateConfig = { id: 'server', itemsPerPage: 10, currentPage: 1, totalItems: 0 };
    }

    this.userData = JSON.parse(this.sessionStorageService.getData('user'));
    this.officerRoleId = this.userData.role_id
    // console.log(this.userData.role_id);
  }

  setPermission() {
    // console.log(this.permissionUser);
    if (!this.permissionUser.isview) {
      this.commonService.redirectToPage(navigationConstants.UNAUTHORISE);
    }
  }
  ngOnInit(): void {

    this.getDashboardDetail(this.paginateConfig.currentPage);
  }

  getDashboardDetail(page: number) {
    let params = { 'page': page, 'pageLength': this.paginateConfig.itemsPerPage, 'user_id': this.userData.id };
    this.spinner.show('dashboardDetailLoader');
    this.apiserviceService.post(APIConstant.GET_DASHBOARD_ADMIN_DASHBOARD_DETAIL_FOR_INVOICE, params, {}).subscribe((res) => {
      if (res.data) {
        this.dataDashboardList = res.data.dataGet.totalDataDetail;
        // alert(res.data.currentPage);
        this.paginateConfig.currentPage = res.data.currentPage;
        this.paginateConfig.totalItems = res.data.totalItems;
        this.paginateConfig.startIndex = res.startIndex;

      } else {
        this.dataDashboardList = null;
      }
      this.spinner.hide('dashboardDetailLoader');
    }, error => {
      this.spinner.hide('dashboardDetailLoader');
      this.dataDashboardList = null;
    });
  }
  detailApplication(applicationType) {
    this.getDashboardDetail(this.paginateConfig.currentPage);
    if (applicationType == 1) {
      this.applicationListLabel = 'Pending Application Detail';
      this.applicationColumnLabel = "Pending On";

    } else if (applicationType == 2) {
      this.applicationListLabel = 'Approved Application Detail';
      this.applicationColumnLabel = "Approved By";
    } else if (applicationType == 3) {
      this.applicationListLabel = 'Reject Application Detail';
      this.applicationColumnLabel = "Rejected By";
    }
  }

  viewApplicationHistory(userObj, modelContent) {
    this.spinner.show('dashboardHistoryLoader');
    const params = { 'applicationId': userObj.application_no };
    this.apiserviceService.post(APIConstant.GET_DASHBOARD_APPLICATION_HISTORY, params, {}).subscribe((res) => {
      this.spinner.hide('dashboardHistoryLoader');
      if (res.data) {
        this.adminDashboardApplicationHistory = res.data.dataGet.applicationHistory;
        this.modalService.open(modelContent, {
          size: "lg",
          backdrop: 'static',
          windowClass: "modalClass-700",
          keyboard: false,
          ariaLabelledBy: "modal-basic-title"
        }).result.then(result => {
          this.closeResult = `Closed with: ${result}`;
        }, reason => {
          this.closeResult = `Dismissed `;

        }
        );

      } else {
        this.spinner.hide('dashboardHistoryLoader');
        this.adminDashboardApplicationHistory = [];
      }

    }, error => {
      this.spinner.show('dashboardHistoryLoader');
      this.adminDashboardApplicationHistory = [];
    });

  }
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }
  modifyApplication(userObj, type) {
    if (type == 'edit') {
      this.sessionStorageService.setData(
        customStorage.sessionKeys.currentApplication,
        JSON.stringify(userObj.application_no)
      );
      this.commonService.redirectToPage(navigationConstants.APPLICATION_FORM + '/' + userObj.application_no);
    }

  }

  viewInvoice(item: any) {
    this.sessionStorageService.setData("appDetail", JSON.stringify(item));
    this.commonService.redirectToPage(navigationConstants.INVOICE);
  }



}

