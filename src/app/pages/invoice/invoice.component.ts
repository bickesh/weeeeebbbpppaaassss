import { OnInit } from '@angular/core';
import { of } from 'rxjs';
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { IngredientsServicesService } from "src/app/services/ingredients-services.service";
import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from '@angular/router';
import { Component, ViewChild, ElementRef } from '@angular/core';
// import * as jsPDF from 'jspdf';
//import { jsPDF } from 'jspdf';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {


  breadcrumbObj = {
    title: 'Invoice',
    classBreadcrumb: 'fa-dashboard',
    urls: [
      { urlName: 'Home', linkURL: '/dashboard' }
    ]
  }

  userData: any;
  invoiceData: any;
  totalAmountPaisa: number;
  totalAmountInr: number;
  applicationAmountInr: number;
  totalGst: number;
  cGst: number;
  sGst: number;
  taxAmountInWord: string;
  totalAmountInWord: string;
  gstNo: string;
  iGstFlag = true;
  constructor(
    public sessionStorageService: SessionStorageService,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private apiserviceService: ApiserviceService,
    private commonService: CommonService,
  ) {

    this.userData = JSON.parse(this.sessionStorageService.getData('user'));

  }


  ngOnInit(): void {
    this.invoiceData = JSON.parse(this.sessionStorageService.getData('appDetail'));

    this.totalAmountPaisa = this.invoiceData.amount;
    this.totalAmountInr = this.totalAmountPaisa / 100;
    this.applicationAmountInr = (this.totalAmountInr / 1.18);
    this.totalGst = this.totalAmountInr - this.applicationAmountInr;
    this.cGst = this.totalGst / 2;
    this.sGst = this.totalGst / 2;
    this.taxAmountInWord = this.convertNumberToWords(this.totalGst);
    this.totalAmountInWord = this.convertNumberToWords(this.totalAmountInr);

    if (this.invoiceData.gst_no != null && this.invoiceData.gst_no != "") {
      this.gstNo = this.invoiceData.gst_no;
      let checkIGstNo = this.gstNo.substring(0, 2);
      if (checkIGstNo == "07") {
        this.iGstFlag = false;
      }
      else {
        this.iGstFlag = true;
      }
    }
    else {
      this.gstNo = "N/A";
      this.iGstFlag = false;
    }
  }

  convertNumberToWords(num) {
    var ones = ["", "One ", "Two ", "Three ", "Four ", "Five ", "Six ", "Seven ", "Eight ", "Nine ", "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen "];
    var tens = ["", "", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"];
    if ((num = num.toString()).length > 9) { return "Overflow: Maximum 9 digits supported"; }

    let n: any = ("000000000" + num).substr(-9).match(/^(\d{2})(\d{2})(\d{2})(\d{1})(\d{2})$/);
    if (!n) return;
    var str = "";
    str += n[1] != 0 ? (ones[Number(n[1])] || tens[n[1][0]] + " " + ones[n[1][1]]) + "Crore " : "";
    str += n[2] != 0 ? (ones[Number(n[2])] || tens[n[2][0]] + " " + ones[n[2][1]]) + "Lakh " : "";
    str += n[3] != 0 ? (ones[Number(n[3])] || tens[n[3][0]] + " " + ones[n[3][1]]) + "Thousand " : "";
    str += n[4] != 0 ? (ones[Number(n[4])] || tens[n[4][0]] + " " + ones[n[4][1]]) + "Hundred " : "";
    str += n[5] != 0 ? (str != "" ? "and " : "") + (ones[Number(n[5])] || tens[n[5][0]] + " " + ones[n[5][1]]) : "";
    return str;
  }


  printInvoice() {
    let printContents, popupWin;
    printContents = document.getElementById('content').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          <style>
          body {
            margin: 0px;
            padding: 0px;
            font-family: calibri;
          }

          .invoice {
            font-size: 14px;
            font-family: calibri;
            border: 1px solid #b9b7b7;
            pading-left: 50px;
            pading-right: 50px;
          }

          .invoice td {
            border-bottom: 1px solid #b9b7b7;
            border-right: 1px solid #b9b7b7;
          }

          /* @page
          {
              size: auto;   /* auto is the initial value */
              margin: 0mm;  /* this affects the margin in the printer settings */
          } */
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }
}
