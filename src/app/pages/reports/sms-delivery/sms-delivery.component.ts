import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-sms-delivery',
  templateUrl: './sms-delivery.component.html',
  styleUrls: ['./sms-delivery.component.css']
})
export class SmsDeliveryComponent implements OnInit {
  breadcrumbObj={
    title:'SMS-Delivery report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'SMS-Delivery report',linkURL:'/report/sms-delivery'}
    ]
  }
  Hours=[
    {id:'0',text:'0'},
    {id:'1',text:'1'},
    {id:'2',text:'2'},
    {id:'3',text:'3'},
    {id:'4',text:'4'},
    {id:'5',text:'5'},
    {id:'6',text:'6'},
    {id:'7',text:'7'},
    {id:'8',text:'8'},
    {id:'9',text:'9'},
    {id:'10',text:'10'},
    {id:'11',text:'11'},
    {id:'12',text:'12'},
    {id:'13',text:'13'},
    {id:'14',text:'14'},
    {id:'15',text:'15'},
    {id:'16',text:'16'},
    {id:'17',text:'17'},
    {id:'18',text:'18'},
    {id:'19',text:'19'},
    {id:'20',text:'20'},
    {id:'21',text:'21'},
    {id:'22',text:'22'},
    {id:'23',text:'23'},
  ];
  Minutes=[
    {id:'0',text:'0'},
    {id:'1',text:'1'},
    {id:'2',text:'2'},
    {id:'3',text:'3'},
    {id:'4',text:'4'},
    {id:'5',text:'5'},
    {id:'6',text:'6'},
    {id:'7',text:'7'},
    {id:'8',text:'8'},
    {id:'9',text:'9'},
    {id:'10',text:'10'},
    {id:'11',text:'11'},
    {id:'12',text:'12'},
    {id:'13',text:'13'},
    {id:'14',text:'14'},
    {id:'15',text:'15'},
    {id:'16',text:'16'},
    {id:'17',text:'17'},
    {id:'18',text:'18'},
    {id:'19',text:'19'},
    {id:'20',text:'20'},
    {id:'21',text:'21'},
    {id:'22',text:'22'},
    {id:'23',text:'23'},
    {id:'24',text:'24'},
    {id:'25',text:'25'},
    {id:'26',text:'26'},
    {id:'27',text:'27'},
    {id:'28',text:'28'},
    {id:'29',text:'29'},
    {id:'30',text:'30'},
    {id:'31',text:'31'},
    {id:'32',text:'32'},
    {id:'33',text:'33'},
    {id:'34',text:'34'},
    {id:'35',text:'35'},
    {id:'36',text:'36'},
    {id:'37',text:'37'},
    {id:'38',text:'38'},
    {id:'39',text:'39'},
    {id:'40',text:'40'},
    {id:'41',text:'41'},
    {id:'42',text:'42'},
    {id:'43',text:'43'},
    {id:'44',text:'44'},
    {id:'45',text:'45'},
    {id:'46',text:'46'},
    {id:'47',text:'47'},
    {id:'48',text:'48'},
    {id:'49',text:'49'},
    {id:'50',text:'50'},
    {id:'51',text:'51'},
    {id:'52',text:'52'},
    {id:'53',text:'53'},
    {id:'54',text:'54'},
    {id:'55',text:'55'},
    {id:'56',text:'56'},
    {id:'57',text:'57'},
    {id:'58',text:'58'},
    {id:'59',text:'59'},
  ];
  smsTypes=[
    {id:'Normal',text:'Normal'},
    {id:'OTP',text:'OTP'},
    {id:'All',text:'All'},
  ];
  languages=[
    {id:'All',text:'All'},
    {id:'EN',text:'English'},
    {id:'HI',text:'Hindi'},
    {id:'AS',text:'Assemese'},
    {id:'BU',text:'Bengali'},
    {id:'CHH',text:'Chhattisgarhi'},
    {id:'PU',text:'Punjabi'},
    {id:'GU',text:'Gujarati'},
    {id:'CH',text:'Chhattisgarhi'},
    {id:'KA',text:'Kannada'},
    {id:'MAL',text:'Malyalam'},
    {id:'MAR',text:'Marathi'},
    {id:'OD',text:'Odia'},
    {id:'TE',text:'Telugu'}
  ];
  smsTags=[
    {id:'All',text:'All'},
    {id:'reg-success',text:'beneficiary-self reg-success'},
    {id:'session-scheduled',text:'beneficiary-session-scheduled'},
    {id:'dose-wise',text:'beneficiary-dose-wise-vaccinated'},
    {id:'all-dose',text:'beneficiary-all-dose-vaccinated'},
    {id:'could-not',text:'beneficiary-could-not-be-vaccinated'},
    {id:'user-created',text:'user -management-user-created'},
    {id:'update-password',text:'user-profile-update-password'},
    {id:'update-mobile',text:'user-profile-update-mobile'},
    {id:'session-canceled',text:'beneficiary-session-canceled'},
    {id:'login-created',text:'user -management-login-created-with-role'},
    {id:'vaccinator-session',text:'vaccinator-session-planned'},
    {id:'admin-session',text:'dist-admin-session-planned'},
    {id:'reset-password',text:'user-profile-reset-password'},
  ];

  validationMessage=validationMessage;  
  paginateConfig:any;  
  btnSrchSpinner:boolean=false;
  
  fromSearch:any=new Date();
  fromSearchHr:any=0;
  fromSearchMin:any=0;
  toSearch:any=new Date();
  toSearchHr:any=0;
  toSearchMin:any=0;
  smsCat:any='Normal';
  smsLang:any='All';
  smsTagged:any='All';
  dashRepData:any[];
  closeResult: string;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.getSMSDeliveryData ();    
  }

  getSMSDeliveryData (){
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'smsCat':this.smsCat,
      'smsLang':this.smsLang,
      'smsTagged':this.smsTagged,
    };

    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_SMS_STATUS_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dashRepData=res.data.dataGet;
      } else {
        this.dashRepData=null;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.dashRepData=null;
      this.spinner.hide('dataTableLoader');
    });
    
  }
  
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }


}
