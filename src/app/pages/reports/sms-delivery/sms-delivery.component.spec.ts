import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmsDeliveryComponent } from './sms-delivery.component';

describe('SmsDeliveryComponent', () => {
  let component: SmsDeliveryComponent;
  let fixture: ComponentFixture<SmsDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmsDeliveryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmsDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
