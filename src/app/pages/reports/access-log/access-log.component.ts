import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-access-log',
  templateUrl: './access-log.component.html',
  styleUrls: ['./access-log.component.css']
})
export class AccessLogComponent implements OnInit {
  breadcrumbObj={
    title:'Access-Log report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Access-Log report',linkURL:'/report/access-log'}
    ]
  }

  smsTypes=[
    {id:'Normal',text:'Normal'},
    {id:'OTP',text:'OTP'},
    {id:'All',text:'All'},
  ];
  languages=[
    {id:'All',text:'All'},
    {id:'EN',text:'English'},
    {id:'HI',text:'Hindi'},
    {id:'AS',text:'Assemese'},
    {id:'BU',text:'Bengali'},
    {id:'CHH',text:'Chhattisgarhi'},
    {id:'PU',text:'Punjabi'},
    {id:'GU',text:'Gujarati'},
    {id:'CH',text:'Chhattisgarhi'},
    {id:'KA',text:'Kannada'},
    {id:'MAL',text:'Malyalam'},
    {id:'MAR',text:'Marathi'},
    {id:'OD',text:'Odia'},
    {id:'TE',text:'Telugu'}
  ];
  smsTags=[
    {id:'All',text:'All'},
    {id:'reg-success',text:'beneficiary-self reg-success'},
    {id:'session-scheduled',text:'beneficiary-session-scheduled'},
    {id:'dose-wise',text:'beneficiary-dose-wise-vaccinated'},
    {id:'all-dose',text:'beneficiary-all-dose-vaccinated'},
    {id:'could-not',text:'beneficiary-could-not-be-vaccinated'},
    {id:'user-created',text:'user -management-user-created'},
    {id:'update-password',text:'user-profile-update-password'},
    {id:'update-mobile',text:'user-profile-update-mobile'},
    {id:'session-canceled',text:'beneficiary-session-canceled'},
    {id:'login-created',text:'user -management-login-created-with-role'},
    {id:'vaccinator-session',text:'vaccinator-session-planned'},
    {id:'admin-session',text:'dist-admin-session-planned'},
    {id:'reset-password',text:'user-profile-reset-password'},
  ];

  validationMessage=validationMessage;  
  paginateConfig:any;  
  btnSrchSpinner:boolean=false;
  
  fromSearch:any=new Date();
  fromSearchHr:any=0;
  fromSearchMin:any=0;
  toSearch:any=new Date();
  toSearchHr:any=0;
  toSearchMin:any=0;
  smsCat:any='Normal';
  smsLang:any='All';
  smsTagged:any='All';
  dashRepData:any[];
  closeResult: string;
  statusTypeVar:any;
  dataTypeVar:any;
  dateWiseReportData:any=[
    {"showdate":"2021-01-19","dateTotal":525212,"clickedTotal":525212},
    {"showdate":"2021-01-20","dateTotal":572293,"clickedTotal":572293}
  ];

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.getSMSDeliveryData ();    
  }

  getSMSDeliveryData (){
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'smsCat':this.smsCat,
      'smsLang':this.smsLang,
      'smsTagged':this.smsTagged,
    };

    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_SMS_STATUS_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dashRepData=res.data.dataGet;
      } else {
        this.dashRepData=null;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.dashRepData=null;
      this.spinner.hide('dataTableLoader');
    });
    
  }
  
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }

  dateWiseData(modelContent,statusType,dataType){
    this.spinner.show('modalLoader');
    switch (statusType) {
      case 'Request':
        this.statusTypeVar='Request';
        break;
      case 'Sent':
        this.statusTypeVar='Sent To NIC';
        break;
      case 'Status':
          this.statusTypeVar='Status From NIC';
          break;
      default:
        this.statusTypeVar='';
        break;
    }
    switch (dataType) {
      case 'Correct':
        this.dataTypeVar='Correct';
        break;
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Drop':
        this.dataTypeVar='Drop';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Total':
        this.dataTypeVar='Total';
        break;
    }
    this.modalService.open(modelContent,{
      size:"md",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
      },reason => {
        this.closeResult = `Dismissed`;
      }
    );
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    let params={
      'dataType':dataType,
      'statusType':statusType,
      'fromSearch':newLocalFromDate,
      'toSearch':newLocalToDate,
    };
    this.apiserviceService.get(APIConstant.GET_DASHBOARD_MODEL_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        // this.dateWiseReportData=res.data.dataGet;
      } else {
        // this.dateWiseReportData=[];
      }
      this.spinner.hide('modalLoader');
    },error=>{ 
      // this.dateWiseReportData=[];
      this.spinner.hide('modalLoader');
    });      
}

}
