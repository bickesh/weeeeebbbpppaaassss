import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-date-wise',
  templateUrl: './date-wise.component.html',
  styleUrls: ['./date-wise.component.css']
})
export class DateWiseComponent implements OnInit {
  breadcrumbObj={
    title:'Date-wise report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Date-wise report',linkURL:'/report/date-wise-data'}
    ]
  }
  reportTypes=[
    {keys:'Request',values:'Request'},
    {keys:'Sent',values:'Sent To NIC'},
    {keys:'Delivered',values:'Delivered'},
    {keys:'Fail',values:'Failed'},
    {keys:'Waiting',values:'Waiting For Response'}
  ];
  dataTypes=[
    {keys:'Normal',values:'Normal'},
    {keys:'OTP',values:'OTP'},
    {keys:'total',values:'Total'}
  ];

  searchForm: FormGroup;
  validationMessage=validationMessage;  
  paginateConfig:any;
  reportData:any;
  totalRecord:any=0;
  
  btnSrchSpinner:boolean=false;
  
  mobsearch:any='';

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      fromdate: ["",[]],
      todate: ["",[]],
      reportType: ["",[]],
      dataType: ["",[]],
    });
    let reportFromDate=this.sessionStorageService.getData(customStorage.sessionKeys.reportFromDate);
    let reportToDate=this.sessionStorageService.getData(customStorage.sessionKeys.reportToDate);
    let reportType=this.sessionStorageService.getData(customStorage.sessionKeys.reportType);
    let reportData=this.sessionStorageService.getData(customStorage.sessionKeys.reportData);
    if(reportFromDate){
      this.searchForm.get('fromdate').setValue(reportFromDate);
    } else {

    }
    if(reportToDate){
      this.searchForm.get('todate').setValue(reportToDate);
    } else {

    }
    if(reportType){
      this.searchForm.get('reportType').setValue(reportType);
    }
    if(reportData){
      this.searchForm.get('dataType').setValue(reportData);
    }
    this.getData();
    
  }

  getData(){
    this.btnSrchSpinner=true;
    let searhcData=this.searchForm.getRawValue();

    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(searhcData.fromdate));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-');
    }
    newLocalFromDate=newLocalFromDate.replace(',','');
    
    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(searhcData.todate));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');    

    let params={
      'reportType':searhcData.reportType,
      'dataType':searhcData.dataType,
      'fromdate':newLocalFromDate,
      'todate':newLocalToDate,
    };
    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_DATE_WISE_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.reportData=res.data.dataGet;
        this.btnSrchSpinner=false;
        this.totalRecord=res.data.totalItems;
      } else {
        this.reportData=[];
        this.btnSrchSpinner=false;
        this.totalRecord=0;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.reportData=[];
      this.btnSrchSpinner=false;
      this.totalRecord=0;
      this.spinner.hide('dataTableLoader');
    });
    
  }
  pageChanged(event){
    this.paginateConfig.currentPage = event;
  }
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }

}
