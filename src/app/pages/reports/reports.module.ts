import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { SharedModule } from 'src/app/modules/shared.module';
import { ReportsRoutingModule } from './reports-routing.module';

import { MobileMessageComponent } from './mobile-message/mobile-message.component';
import { DateWiseComponent } from './date-wise/date-wise.component';
import { TimeWiseComponent } from './time-wise/time-wise.component';
import { SmsDeliveryComponent } from './sms-delivery/sms-delivery.component';
import { AccessLogComponent } from './access-log/access-log.component';
import { CowinDataComponent } from './cowin-data/cowin-data.component';
import { CowinDataDetailsComponent } from './cowin-data-details/cowin-data-details.component';

// for dateTimepicker 
import { OwlDateTimeModule, OwlMomentDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from '@danielmoncada/angular-datetime-picker';
export const MY_MOMENT_FORMATS = {
  parseInput: 'DD/MM/YYYY HH:mm:ss',
  fullPickerInput: 'DD/MM/YYYY HH:mm:ss',
  datePickerInput: 'DD/MM/YYYY',
  timePickerInput: 'HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};

@NgModule({
  declarations: [
    MobileMessageComponent,
    DateWiseComponent,
    TimeWiseComponent,
    SmsDeliveryComponent,
    AccessLogComponent,
    CowinDataComponent,
    CowinDataDetailsComponent
  ],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule,
    OwlDateTimeModule, OwlNativeDateTimeModule, OwlMomentDateTimeModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS }
  ],
})
export class ReportsModule { }
