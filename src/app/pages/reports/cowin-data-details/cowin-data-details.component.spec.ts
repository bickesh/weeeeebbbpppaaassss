import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CowinDataDetailsComponent } from './cowin-data-details.component';

describe('CowinDataDetailsComponent', () => {
  let component: CowinDataDetailsComponent;
  let fixture: ComponentFixture<CowinDataDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CowinDataDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CowinDataDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
