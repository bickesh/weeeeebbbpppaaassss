import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-cowin-data-details',
  templateUrl: './cowin-data-details.component.html',
  styleUrls: ['./cowin-data-details.component.css']
})
export class CowinDataDetailsComponent implements OnInit {
  breadcrumbObj={
    title:'Cowin-data Details report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Cowin-data details report',linkURL:'/report/cowin-data-details'}
    ]
  }
  validationMessage=validationMessage;  
  paginateConfig:any;  
  btnSrchSpinner:boolean=false;
  
  fromSearch:any=new Date();
  fromSearchHr:any=0;
  fromSearchMin:any=0;
  fromSearchSec:any=0;
  toSearch:any=new Date();
  toSearchHr:any=0;
  toSearchMin:any=0;
  toSearchSec:any=0;


  dateWiseReportData:any=[];
  dateWiseReportHistory:any=[];
  closeResult: string;

  smsCat:any;
  statusTypeVar:any;
  dataTypeVar:any;
  statusTypeCat:any;
  dataTypeCat:any;


  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal) { 
      this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
    }

  ngOnInit(): void {
    this.smsCat=this.sessionStorageService.getData(customStorage.sessionKeys.smsCat);
    this.statusTypeCat=this.sessionStorageService.getData(customStorage.sessionKeys.statusTypeCat);
    this.dataTypeCat=this.sessionStorageService.getData(customStorage.sessionKeys.dataTypeCat);
    switch (this.statusTypeCat) {
      case 'Request':
        this.statusTypeVar='Request';
        break;
      case 'Sent':
        this.statusTypeVar='Sent To NIC';
        break;
      case 'Status':
          this.statusTypeVar='Status From NIC';
          break;
      default:
        this.statusTypeVar='';
        break;
    }
    switch (this.dataTypeCat) {
      case 'Correct':
        this.dataTypeVar='Correct';
        break;
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Drop':
        this.dataTypeVar='Drop';
        break;      
      case 'Not':
        this.dataTypeVar='Not';
        break;      
      case 'Accepted':
        this.dataTypeVar='Accepted';
        break;      
      case 'Rejected':
        this.dataTypeVar='Rejected';
        break;      
      case 'Delivered':
        this.dataTypeVar='Delivered';
        break;      
      case 'Failed':
        this.dataTypeVar='Failed';
        break;      
      case 'Partial':
        this.dataTypeVar='Partial';
        break;      
      case 'Waiting':
        this.dataTypeVar='Waiting';
        break;
      case 'Total':
        this.dataTypeVar='Total';
        break;
    }

    if(this.sessionStorageService.getData(customStorage.sessionKeys.reportFromDate)){
      this.fromSearch=this.sessionStorageService.getData(customStorage.sessionKeys.reportFromDate)
    }
    if(this.sessionStorageService.getData(customStorage.sessionKeys.reportToDate)){
      this.toSearch=this.sessionStorageService.getData(customStorage.sessionKeys.reportToDate)
    }
    this.getSMSDeliveryData(this.paginateConfig.currentPage);    
  }
  getSMSDeliveryData(page: number){
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    this.fromSearchHr=theDateFrom.getHours();
    this.fromSearchMin=theDateFrom.getMinutes();
    this.fromSearchSec=theDateFrom.getSeconds();
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    this.toSearchHr=theDateTo.getHours();
    this.toSearchMin=theDateTo.getMinutes();
    this.toSearchSec=theDateTo.getSeconds();
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');

    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'fromSearchSec':this.fromSearchSec,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'toSearchSec':this.toSearchSec,
      'smsCat':this.smsCat,
      'statusType':this.statusTypeCat,
      'dataType':this.dataTypeCat,
      'page':page,
      'pageLength':this.paginateConfig.itemsPerPage
    };
    this.paginateConfig.currentPage = page;
    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_COWIN_MODEL_DETAILS_RPT,params).subscribe((res)=>{      
      if(res.data.dataGet){
        this.dateWiseReportData=res.data.dataGet;
        this.paginateConfig.totalItems=res.data.totalItems;
      } else {
        this.dateWiseReportData=[];
        this.paginateConfig.totalItems=0;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{ 
      this.dateWiseReportData=[];
      this.paginateConfig.totalItems=0;
      this.spinner.hide('dataTableLoader');
    });
  }
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }
  historyStatus(modelContent,dateData){

    this.spinner.show('modalLoader');

    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
      },reason => {
        this.closeResult = `Dismissed`;
      }
    );
    
    let params={};
    if(dateData.resID){
      params['resID']=dateData.resID;
    } else {
      params['reqID']=dateData.reqID;
    }

    this.apiserviceService.get(APIConstant.GET_COWIN_MODEL_DETAILS_HISTORY,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dateWiseReportHistory=res.data.dataGet;
        console.log(this.dateWiseReportHistory[0].req_date);
        
      } else {
        this.dateWiseReportHistory=[];
      }
      this.spinner.hide('modalLoader');
    },error=>{ 
      this.dateWiseReportHistory=[];
      this.spinner.hide('modalLoader');
    });      
}


}
