import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MobileMessageComponent } from './mobile-message/mobile-message.component';
import { DateWiseComponent } from './date-wise/date-wise.component';
import { TimeWiseComponent } from './time-wise/time-wise.component';
import { SmsDeliveryComponent } from './sms-delivery/sms-delivery.component';
import { AccessLogComponent } from './access-log/access-log.component';
import { CowinDataComponent } from './cowin-data/cowin-data.component';
import { CowinDataDetailsComponent } from './cowin-data-details/cowin-data-details.component';

const routes: Routes = [
  {path:'',redirectTo:'mobile-message',pathMatch:'full' },
  {path:'mobile-message' , component:MobileMessageComponent},
  {path:'date-wise-data' , component:DateWiseComponent},
  {path:'time-wise-data' , component:TimeWiseComponent},
  {path:'sms-delivery' , component:SmsDeliveryComponent},
  {path:'access-log' , component:AccessLogComponent},
  {path:'cowin-data' , component:CowinDataComponent},
  {path:'cowin-data-details' , component:CowinDataDetailsComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
