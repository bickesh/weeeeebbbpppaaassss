import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CowinDataComponent } from './cowin-data.component';

describe('CowinDataComponent', () => {
  let component: CowinDataComponent;
  let fixture: ComponentFixture<CowinDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CowinDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CowinDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
