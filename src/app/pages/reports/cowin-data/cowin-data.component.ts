import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-cowin-data',
  templateUrl: './cowin-data.component.html',
  styleUrls: ['./cowin-data.component.css']
})
export class CowinDataComponent implements OnInit {
  breadcrumbObj={
    title:'Cowin-data report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Cowin-data report',linkURL:'/report/cowin-data'}
    ]
  }

  smsTypes=[
    {id:'Normal',text:'Normal'},
    {id:'OTP',text:'OTP'},
    {id:'All',text:'All'},
  ];
  languages=[
    {id:'All',text:'All'},
    {id:'EN',text:'English'},
    {id:'HI',text:'Hindi'},
    {id:'AS',text:'Assemese'},
    {id:'BU',text:'Bengali'},
    {id:'CHH',text:'Chhattisgarhi'},
    {id:'PU',text:'Punjabi'},
    {id:'GU',text:'Gujarati'},
    {id:'CH',text:'Chhattisgarhi'},
    {id:'KA',text:'Kannada'},
    {id:'MAL',text:'Malyalam'},
    {id:'MAR',text:'Marathi'},
    {id:'OD',text:'Odia'},
    {id:'TE',text:'Telugu'}
  ];
  smsTags=[
    {id:'All',text:'All'},
    {id:'reg-success',text:'beneficiary-self reg-success'},
    {id:'session-scheduled',text:'beneficiary-session-scheduled'},
    {id:'dose-wise',text:'beneficiary-dose-wise-vaccinated'},
    {id:'all-dose',text:'beneficiary-all-dose-vaccinated'},
    {id:'could-not',text:'beneficiary-could-not-be-vaccinated'},
    {id:'user-created',text:'user -management-user-created'},
    {id:'update-password',text:'user-profile-update-password'},
    {id:'update-mobile',text:'user-profile-update-mobile'},
    {id:'session-canceled',text:'beneficiary-session-canceled'},
    {id:'login-created',text:'user -management-login-created-with-role'},
    {id:'vaccinator-session',text:'vaccinator-session-planned'},
    {id:'admin-session',text:'dist-admin-session-planned'},
    {id:'reset-password',text:'user-profile-reset-password'},
  ];

  validationMessage=validationMessage;  
  paginateConfig:any;  
  btnSrchSpinner:boolean=false;
  
  fromSearch:any=new Date();
  fromSearchHr:any=0;
  fromSearchMin:any=0;
  fromSearchSec:any=0;
  toSearch:any=new Date();
  toSearchHr:any=0;
  toSearchMin:any=0;
  toSearchSec:any=0;
  smsCat:any='Normal';
  smsLang:any='All';
  smsTagged:any='All';
  smsState:any;
  smsDistrict:any;
  dashRepData:any;
  closeResult: string;
  statusTypeVar:any;
  dataTypeVar:any;
  dateWiseReportData:any=[
    // {"showdate":"2021-01-19","dateTotal":525212,"clickedTotal":525212},
  ];
  statusTypeCat:any;
  dataTypeCat:any;


  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.getSMSDeliveryData ();    
  }

  getSMSDeliveryData (){
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    this.fromSearchHr=theDateFrom.getHours();
    this.fromSearchMin=theDateFrom.getMinutes();
    this.fromSearchSec=theDateFrom.getSeconds();
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    this.toSearchHr=theDateTo.getHours();
    this.toSearchMin=theDateTo.getMinutes();
    this.toSearchSec=theDateTo.getSeconds();
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'fromSearchSec':this.fromSearchSec,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'toSearchSec':this.toSearchSec,
      'smsCat':this.smsCat
    };

    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_COWIN_STATUS_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dashRepData=res.data.dataGet;        
      } else {
        this.dashRepData=null;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.dashRepData=null;
      this.spinner.hide('dataTableLoader');
    });
    
  }
  
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }

  dateWiseData(modelContent,statusType,dataType){
    this.statusTypeCat=statusType;
    this.dataTypeCat=dataType;

    this.spinner.show('modalLoader');
    switch (statusType) {
      case 'Request':
        this.statusTypeVar='Request';
        break;
      case 'Sent':
        this.statusTypeVar='Sent To NIC';
        break;
      case 'Status':
          this.statusTypeVar='Status From NIC';
          break;
      default:
        this.statusTypeVar='';
        break;
    }
    switch (dataType) {
      case 'Correct':
        this.dataTypeVar='Correct';
        break;
      case 'Wrong':
        this.dataTypeVar='Wrong';
        break;      
      case 'Drop':
        this.dataTypeVar='Drop';
        break;      
      case 'Not':
        this.dataTypeVar='Not';
        break;      
      case 'Accepted':
        this.dataTypeVar='Accepted';
        break;      
      case 'Rejected':
        this.dataTypeVar='Rejected';
        break;      
      case 'Delivered':
        this.dataTypeVar='Delivered';
        break;      
      case 'Failed':
        this.dataTypeVar='Failed';
        break;      
      case 'Partial':
        this.dataTypeVar='Partial';
        break;      
      case 'Waiting':
        this.dataTypeVar='Waiting';
        break;
      case 'Total':
        this.dataTypeVar='Total';
        break;
    }
    this.modalService.open(modelContent,{
      size:"md",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
      },reason => {
        this.closeResult = `Dismissed`;
      }
    );

    
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    this.fromSearchHr=theDateFrom.getHours();
    this.fromSearchMin=theDateFrom.getMinutes();
    this.fromSearchSec=theDateFrom.getSeconds();
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    this.toSearchHr=theDateTo.getHours();
    this.toSearchMin=theDateTo.getMinutes();
    this.toSearchSec=theDateTo.getSeconds();
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'fromSearchSec':this.fromSearchSec,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'toSearchSec':this.toSearchSec,
      'smsCat':this.smsCat,
      'statusType':this.statusTypeCat,
      'dataType':this.dataTypeCat,
    };

    this.apiserviceService.get(APIConstant.GET_COWIN_MODEL_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dateWiseReportData=res.data.dataGet;
      } else {
        this.dateWiseReportData=[];
      }
      this.spinner.hide('modalLoader');
    },error=>{ 
      this.dateWiseReportData=[];
      this.spinner.hide('modalLoader');
    });      
}
openReport(dateData){
  this.sessionStorageService.setData(
    customStorage.sessionKeys.reportFromDate,
    dateData.request_from_time
  );
  this.sessionStorageService.setData(
    customStorage.sessionKeys.reportToDate,
    dateData.request_to_time
  );
  
  this.sessionStorageService.setData(
    customStorage.sessionKeys.smsCat,
    this.smsCat
  );
  this.sessionStorageService.setData(
    customStorage.sessionKeys.statusTypeCat,
    this.statusTypeCat
  );
  this.sessionStorageService.setData(
    customStorage.sessionKeys.dataTypeCat,
    this.dataTypeCat
  );

  this.modalService.dismissAll();
  this.commonService.redirectToPage(navigationConstants.COWIN_DATA_DETAILS);
}

}
