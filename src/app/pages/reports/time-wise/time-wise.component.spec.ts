import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeWiseComponent } from './time-wise.component';

describe('TimeWiseComponent', () => {
  let component: TimeWiseComponent;
  let fixture: ComponentFixture<TimeWiseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeWiseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeWiseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
