import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-time-wise',
  templateUrl: './time-wise.component.html',
  styleUrls: ['./time-wise.component.css']
})
export class TimeWiseComponent implements OnInit {
  breadcrumbObj={
    title:'Time-wise report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Time-wise report',linkURL:'/report/time-wise-data'}
    ]
  }
  Hours=[
    {id:'0',text:'0'},
    {id:'1',text:'1'},
    {id:'2',text:'2'},
    {id:'3',text:'3'},
    {id:'4',text:'4'},
    {id:'5',text:'5'},
    {id:'6',text:'6'},
    {id:'7',text:'7'},
    {id:'8',text:'8'},
    {id:'9',text:'9'},
    {id:'10',text:'10'},
    {id:'11',text:'11'},
    {id:'12',text:'12'},
    {id:'13',text:'13'},
    {id:'14',text:'14'},
    {id:'15',text:'15'},
    {id:'16',text:'16'},
    {id:'17',text:'17'},
    {id:'18',text:'18'},
    {id:'19',text:'19'},
    {id:'20',text:'20'},
    {id:'21',text:'21'},
    {id:'22',text:'22'},
    {id:'23',text:'23'},
  ];
  Minutes=[
    {id:'0',text:'0'},
    {id:'1',text:'1'},
    {id:'2',text:'2'},
    {id:'3',text:'3'},
    {id:'4',text:'4'},
    {id:'5',text:'5'},
    {id:'6',text:'6'},
    {id:'7',text:'7'},
    {id:'8',text:'8'},
    {id:'9',text:'9'},
    {id:'10',text:'10'},
    {id:'11',text:'11'},
    {id:'12',text:'12'},
    {id:'13',text:'13'},
    {id:'14',text:'14'},
    {id:'15',text:'15'},
    {id:'16',text:'16'},
    {id:'17',text:'17'},
    {id:'18',text:'18'},
    {id:'19',text:'19'},
    {id:'20',text:'20'},
    {id:'21',text:'21'},
    {id:'22',text:'22'},
    {id:'23',text:'23'},
    {id:'24',text:'24'},
    {id:'25',text:'25'},
    {id:'26',text:'26'},
    {id:'27',text:'27'},
    {id:'28',text:'28'},
    {id:'29',text:'29'},
    {id:'30',text:'30'},
    {id:'31',text:'31'},
    {id:'32',text:'32'},
    {id:'33',text:'33'},
    {id:'34',text:'34'},
    {id:'35',text:'35'},
    {id:'36',text:'36'},
    {id:'37',text:'37'},
    {id:'38',text:'38'},
    {id:'39',text:'39'},
    {id:'40',text:'40'},
    {id:'41',text:'41'},
    {id:'42',text:'42'},
    {id:'43',text:'43'},
    {id:'44',text:'44'},
    {id:'45',text:'45'},
    {id:'46',text:'46'},
    {id:'47',text:'47'},
    {id:'48',text:'48'},
    {id:'49',text:'49'},
    {id:'50',text:'50'},
    {id:'51',text:'51'},
    {id:'52',text:'52'},
    {id:'53',text:'53'},
    {id:'54',text:'54'},
    {id:'55',text:'55'},
    {id:'56',text:'56'},
    {id:'57',text:'57'},
    {id:'58',text:'58'},
    {id:'59',text:'59'},
  ];

  validationMessage=validationMessage;  
  paginateConfig:any;  
  btnSrchSpinner:boolean=false;
  
  fromSearch:any=new Date();
  fromSearchHr:any=0;
  fromSearchMin:any=0;
  toSearch:any=new Date();
  toSearchHr:any=0;
  toSearchMin:any=0;
  dashRepData:any[];
  totalCoast:any;
  failedMsgData:any;
  closeResult: string;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.getDashboard ();    
  }

  getDashboard (){
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
    };

    this.spinner.show('dataTableLoader');
    this.apiserviceService.get(APIConstant.GET_TIMEWISE_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.dashRepData=res.data.dataGet;
        this.totalCoast=(res.data.dataGet.totalSentToNIC*(0.031)).toFixed(0);        
      } else {
        this.dashRepData=[];
        this.totalCoast=0;
      }
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.dashRepData=[];
      this.totalCoast=0;
      this.spinner.hide('dataTableLoader');
    });
    
  }
  
  replaceAll(string, search, replace) {
    return string.split(search).join(replace);
  }

  timeWiseFailedData(modelContent,dataType){
    this.spinner.show('modalLoader');

    this.modalService.open(modelContent,{
      size:"md",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
      },reason => {
        this.closeResult = `Dismissed`;
      }
    );
    let newLocalFromDate='';
    var theDateFrom = new Date(Date.parse(this.fromSearch));
    const localFromDate = theDateFrom.toLocaleString().split(" ");
    if(localFromDate[0]!='Invalid'){
      newLocalFromDate=this.replaceAll(localFromDate[0],'/','-'); 
    }
    newLocalFromDate=newLocalFromDate.replace(',','');

    let newLocalToDate='';
    var theDateTo = new Date(Date.parse(this.toSearch));
    const localToDate = theDateTo.toLocaleString().split(" ");
    if(localToDate[0]!='Invalid'){
      newLocalToDate=this.replaceAll(localToDate[0],'/','-'); 
    }
    newLocalToDate=newLocalToDate.replace(',','');
    let params={
      'fromSearch':newLocalFromDate,
      'fromSearchHr':this.fromSearchHr,
      'fromSearchMin':this.fromSearchMin,
      'toSearch':newLocalToDate,
      'toSearchHr':this.toSearchHr,
      'toSearchMin':this.toSearchMin,
      'dataType':dataType,
    };

    this.spinner.show('modalLoader');
    this.apiserviceService.get(APIConstant.GET_TIMEWISE_FAILED_RPT,params).subscribe((res)=>{
      if(res.data.dataGet){
        this.failedMsgData=res.data.dataGet;
      } else {
        this.failedMsgData=null;
      }
      this.spinner.hide('modalLoader');
    },error=>{
      this.failedMsgData=null;
      this.spinner.hide('modalLoader');
    });
    
 
  }

}
