import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-mobile-message',
  templateUrl: './mobile-message.component.html',
  styleUrls: ['./mobile-message.component.css']
})
export class MobileMessageComponent implements OnInit {
  breadcrumbObj={
    title:'Mobile Message report',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Mobile Message report',linkURL:'/report/resource-productivity'}
    ]
  }
  mobsearch:any='';

  private baseUrl = environment.uploadUrl;
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  companies:any;

  companyForm: FormGroup;
  companyLogo:any;
  viewLogo:any;
  formEditable:boolean=true;
  isCompanyFormSubmitted = false;
  btnSpinner:boolean=false;
  btnSrchSpinner:boolean=false;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.btnSrchSpinner=true;
    let params={'mobile':this.mobsearch};
    this.apiserviceService.get('v1/mobilerpt',params).subscribe((res)=>{
      if(res.data.dataGet){
        this.companies=res.data.dataGet;
        this.btnSrchSpinner=false;
      } else {
        this.companies=[];
        this.btnSrchSpinner=false;
      }
    },error=>{
      this.companies=[];
      this.btnSrchSpinner=false;
    });
    
  }
  pageChanged(event){
    this.paginateConfig.currentPage = event;
  }

}
