import { OnInit } from '@angular/core';
import { of } from 'rxjs';
import { NgWizardConfig, NgWizardService, StepChangedArgs, StepValidationArgs, STEP_STATE, THEME } from 'ng-wizard';
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from "@angular/forms";
import { requiredFileType } from "src/app/customvalidator/cftv.validator";
import { removeSpaces } from "src/app/customvalidator/removeSpaces.validator";
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { SessionStorageService } from "src/app/services/session-storage.service";
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { IngredientsServicesService } from "src/app/services/ingredients-services.service";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { environment } from "src/environments/environment";
import { HttpClient } from "@angular/common/http";
import { threadId } from 'worker_threads';
import { ActivatedRoute } from '@angular/router';

import { Component, ViewChild, ElementRef } from '@angular/core';
// import * as jsPDF from 'jspdf';
// import { jsPDF } from 'jspdf';
//import { PaymentComponent } from '../payment/payment.component';
import { WindowRefService } from "src/app/services/window-ref.service";
import { serialize } from 'v8';
declare var Razorpay: any;

@Component({
  selector: 'app-application-form',
  templateUrl: './application-form.component.html',
  styleUrls: ['./application-form.component.css']
})

export class ApplicationFormComponent {
  @ViewChild('content', { static: true }) el: ElementRef<any>;

  validationMessage = validationMessage;
  current_application_id = '101';
  ingredientsForm: FormGroup;
  applicationFormStep1: FormGroup;
  applicationFormStep2: FormGroup;
  applicationFormStep3: FormGroup;
  applicationFormStep4: FormGroup;
  applicationFormStep5: FormGroup;
  applicationFormStep6: FormGroup;
  applicationFormStep7: FormGroup;
  applicationFormStep8: FormGroup;
  isIngredientsFormSubmitted = false;
  isApplicationFormStep1Submitted = false;
  isApplicationFormStep2Submitted = false;
  isApplicationFormStep3Submitted = false;
  isApplicationFormStep4Submitted = false;
  isApplicationFormStep5Submitted = false;
  isApplicationFormStep6Submitted = false;
  isApplicationFormStep7Submitted = false;
  isApplicationFormStep8Submitted = false;
  fileDocumentPreview: any;
  nabl_certificate_file_url: any;
  ilac_certificate_file_url: any;
  manufacturing_pro_file_url: any;
  consumption_purpose_file_url: any;
  agreement_copy_file_url: any;
  safetyInfo_file_url: any;
  additional_specific_3a_txt4_file_url: any;
  evidenceDemo_file_url: any;
  risk_assessment_review_file_url: any;
  safety_studies_conducted_file_url: any;
  ingredient_evidence_file_url: any;
  addSpecific_file_url: any;

  addSpecific_3c_1_file_url: any;
  addSpecific_3c_2_file_url: any;
  addSpecific_3c_3_file_url: any;
  addSpecific_3c_4_file_url: any;

  risk_assessment_file_url: any;
  toxicity_studies_file_url: any;
  allorgenicity_file_url: any;
  geographical_area_file_url: any;
  consumption_quantity_file_url: any;

  receipt_copy_file_url: any;
  receipt_copy_file1_url: any;
  receipt_file1_url: any;
  receipt_file3_url: any;
  receipt_copy_file2_url: any;
  receipt_file4_url: any;
  safety_recognised_url: any;
  additional_specific_3d_template_file_url: any;
  additional_specific_4d_other_file_url: any;

  btnSpinner: boolean = false;
  oldFileDocumentPreview: any;
  fileDocumentPreviewName: any[];
  fileDocumentPreview1: any;
  fileDocumentPreviewName1: any[];
  fileDocumentPreview2: any;
  fileDocumentPreviewName2: any[];
  fileDocumentPreview2manufacturing_pro: any;
  fileDocumentPreviewName2manufacturing_pro: any[];
  fileDocumentPreview3: any;
  fileDocumentPreviewName3: any[];
  fileDocumentPreview4: any;
  fileDocumentPreviewName4: any[];
  fileDocumentPreview5: any;
  fileDocumentPreviewName5: any[];
  fileDocumentPreview6: any;
  fileDocumentPreviewName6: any[];
  fileDocumentPreview7: any;
  fileDocumentPreviewName7: any[];
  fileDocumentPreview8: any;
  fileDocumentPreviewName8: any[];
  fileDocumentPreview9: any;
  fileDocumentPreviewName9: any[];
  fileDocumentPreview10: any;
  fileDocumentPreviewName10: any[];
  fileDocumentPreview11: any;
  fileDocumentPreviewName11: any[];
  fileDocumentPreview12: any;
  fileDocumentPreviewName12: any[];
  fileDocumentPreview13: any;
  fileDocumentPreviewName13: any[];
  fileDocumentPreview14: any;
  fileDocumentPreviewName14: any[];
  fileDocumentPreview15: any;
  fileDocumentPreviewName15: any[];
  fileDocumentPreview16: any;
  fileDocumentPreviewName16: any[];
  fileDocumentPreview17: any;
  fileDocumentPreviewName17: any[];
  fileDocumentPreview18: any;
  fileDocumentPreviewName18: any[];
  fileDocumentPreview19: any;
  fileDocumentPreviewName19: any[];

  fileDocumentPreviewSafety: any;
  fileDocumentPreviewNameSafety: any[];

  fileDocumentPreviewSafetyInfo: any;
  fileDocumentPreviewNameSafetyInfo: any[];

  fileDocumentPreviewEvidenceDemo: any;
  fileDocumentPreviewNameEvidenceDemo: any[];
  fileDocumentPreviewAddSpecific: any;
  fileDocumentPreviewNameAddSpecific: any[];
  fileDocumentPreviewAddSpecific_3c_1: any;
  fileDocumentPreviewNameAddSpecific_3c_1: any[];
  fileDocumentPreviewAddSpecific_3c_2: any;
  fileDocumentPreviewNameAddSpecific_3c_2: any[];
  fileDocumentPreviewAddSpecific_3c_3: any;
  fileDocumentPreviewNameAddSpecific_3c_3: any[];
  fileDocumentPreviewAddSpecific_3c_4: any;
  fileDocumentPreviewNameAddSpecific_3c_4: any[];

  fileDocumentPreviewadditional_specific_4d_other_file: any;
  fileDocumentPreviewNameadditional_specific_4d_other_file: any[];

  fileDocumentPreviewadditional_specific_3d_template_file: any;
  fileDocumentPreviewNameadditional_specific_3d_template_file: any[];

  currentStep: number = 0;
  isDraftClicked: number = 0;
  myFiles: any[];
  projectForm: any[];
  userData: any;
  applicationData: any[];
  ingredient_ddl: any = '';
  addd = {};
  standarizeData: any = 'Yes';
  viewDataPDF: any;
  viewApplicationForOther: any;
  viewAuthorisedPersonOther: any;
  viewFoodNameOther: any;
  viewRegulatoryStatus: any;
  viewGenus: any;
  words: any;
  wordsAnyOther: any;
  SafetyInfoToolTip2 = 'Arrange all risk documents chronological  start from the latest';
  ApplicationForFoodType: any;

  closeModal: string;

  public uploadUrl = environment.uploadUrl + "public/";
  // public uploadUrl1 = environment.uploadUrl + "public/";
  private apiUrl = environment.apiUrl;
  IngredientsStandarizeResult = {};

  stepZeroData: any;
  paymentStatus: any;
  applicationConfig: any;
  breadcrumbObj: any;

  // breadcrumbObj = {
  //   title: 'Application Form/Check Ingredients',
  //   classBreadcrumb: 'fa-dashboard',
  //   urls: [
  //     { urlName: 'Home', linkURL: '/dashboard-10' }
  //   ]
  // }
  stepStates = {
    normal: STEP_STATE.normal,
    disabled: STEP_STATE.disabled,
    error: STEP_STATE.error,
    hidden: STEP_STATE.hidden
  };

  // savePdf(){
  //   // let content=this.content.nativeElement;
  //   let pdf = new jsPDF('p','pt','a3');
  //   pdf.setFontSize(10);
  //   pdf.text("content", 15, 15);
  //   // pdf.text("hello rajesh", 10,10);
  //   pdf.html(this.el.nativeElement,{
  //     callback: (pdf)=>{
  //       pdf.save("applicationform.pdf");
  //     }
  //   });

  // }
  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
      toolbarExtraButtons: [
        {
          text: 'Save Draft', class: 'btn btn-secondary draft-btn', event: () => {


            this.spinner.show('registrationPopup');
            this.btnSpinner = true;
            // this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM,this.applicationFormStep1,{}).subscribe(data=>{
            //   this.btnSpinner=false;
            //   this.spinner.hide('registrationPopup');
            //     setTimeout(() => {
            //      // this.commonService.redirectToPage(navigationConstants.LOGIN);
            //     }, 2000);
            // },error=>{
            //   this.spinner.hide('registrationPopup');
            //   this.btnSpinner=false;
            // });
            //this.saveDraft();
            //document.getElementById("ng-wizard-btn-next").click();

            // var result = document.getElementsByClassName("ng-wizard-btn-next").click();
            // var el =  document.getElementsByClassName('ng-wizard-btn-next');
            // result.click();
            //((document.getElementById("health_id_otp") as HTMLElement).removeAttribute('shiv'));
            //console.log('CURRENT STEP'+this.currentStep+'"' );

            this.isDraftClicked = 1;
            if (this.currentStep == 0) {
              this.exitStepZero();
            }
            if (this.currentStep == 1) {
              this.exitStepOne();
            }
            if (this.currentStep == 2) {
              this.exitStepTwo();
            }
            if (this.currentStep == 3) {
              this.exitStepFour(); // Nothing happend in this.exitStepThree();
            }
            return false;

          }
        },
        {
          text: 'Submit', class: 'btn btn-info submit-btn', event: () => {

            //this.exitStepOne();
            // this.exitStepTwo();
            //this.exitStepThree();
            //  this.exitStepFour();
            // this.exitStepFive();
            // this.exitStepSix();
            if (this.paymentStatus) {
              this.isDraftClicked = 0;
              this.exitStepFour();
            } else {
              this.exitStepEight();
            }



            this.spinner.show('registrationPopup');
            this.btnSpinner = true;
            // this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM,[this.applicationFormStep1,this.applicationFormControlStep2, this.applicationFormControlStep3, this.applicationFormControlStep4, this.applicationFormControlStep5, this.applicationFormControlStep6, this.applicationFormControlStep7],{}).subscribe(data=>{
            this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, [this.applicationFormStep1, this.applicationFormControlStep2, this.applicationFormControlStep4], {}).subscribe(data => {
              this.btnSpinner = false;
              this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
              this.spinner.hide('registrationPopup');

            }, error => {
              this.spinner.hide('registrationPopup');
              this.btnSpinner = false;
            });
            //console.log([this.applicationFormStep1,this.applicationFormControlStep2, this.applicationFormControlStep3, this.applicationFormControlStep4, this.applicationFormControlStep5, this.applicationFormControlStep6, this.applicationFormControlStep7]);
          }
        }

      ],
    }
  };


  ingredientsData: any = [];
  additivesData: any = [];
  constructor(
    private ngWizardService: NgWizardService,
    private router: Router,
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    public formBuilder: FormBuilder,
    public sessionStorageService: SessionStorageService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private http: HttpClient,
    private ingredientsService: IngredientsServicesService,
    private activatedRoute: ActivatedRoute,
    private winRef: WindowRefService,// FOR RAZORPAY
    private ElByClassName: ElementRef,
  ) {
    this.ingredientsService.getIngredients().subscribe(data => {
      //console.log(data);
      this.ingredientsData = data;
    });
    this.ingredientsService.getAdditives().subscribe(data => {
      //console.log(data);
      this.additivesData = data;
    });



    this.userData = JSON.parse(this.sessionStorageService.getData('user'));
    this.breadcrumbObj = {
      title: 'Application Details',
      classBreadcrumb: 'fa-dashboard',
      urls: [
        // { urlName: 'Home', linkURL: '/dashboard-' + this.userData.role_id }-------------------
      ]
    }

    //////////////////PAYMENT/////////////////
    //this.permissionUser=this.commonService.getPermission('dashboard-6',null);
    //this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
    // this.paginateConfig={ id: 'server', itemsPerPage: 1, currentPage:1, totalItems:0};
    //this.userData=JSON.parse(this.sessionStorageService.getData('user'));
    //this.officerRoleId =this.userData.role_id
    //console.log(this.userData.role_id);

  }

  triggerModal(content) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
    });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit(): void {
    debugger;
    this.paymentStatus = false;
    this.current_application_id = this.activatedRoute.snapshot.params.param;

    if (this.current_application_id == undefined) {
      this.sessionStorageService.setData(customStorage.sessionKeys.currentApplication, JSON.stringify('')
      );
    }

    if (this.current_application_id) {
      this.sessionStorageService.setData(customStorage.sessionKeys.currentApplication, JSON.stringify(this.current_application_id)
      );
    }
    console.log(this.current_application_id)

    this.getData();
    this.getPaymentData();
    //this.getDataPDF();
    this.ingredientsForm = this.formBuilder.group({
      itemrows: this.formBuilder.array([this.initItemRows()]),
      itemrows1: this.formBuilder.array([this.initItemRows1()]),
      ingredient_name: ["", Validators.required],
      ingredient_quantity: ["", Validators.required],
      ingredient_standarizeData: ["", Validators.required]
      // additive_name: ["", Validators.required],
      // additive_quantity: ["", Validators.required]
    });


    this.applicationFormStep1 = this.formBuilder.group({
      applicationfor: ["", [Validators.required]],
      // specify: ["", []],
      applicantName: ["", [Validators.required, removeSpaces]],
      nameOfAuthorizedPerson: ["", [Validators.required, removeSpaces]],
      // auth_person_other: ["", []],
      mobileNo: ["", [Validators.required, removeSpaces, Validators.pattern(regExConstant.mobileNumber)]],
      emailId: ["", [Validators.required, removeSpaces, Validators.pattern(regExConstant.email)]],
      organisationName: ["", [Validators.required, removeSpaces]],
      organisationAddress: ["", [Validators.required, removeSpaces]],
      licenseNo: ["", [removeSpaces]],
      natureOfBusiness: ["", [Validators.required, removeSpaces]],
      nameOfFood: ["", [Validators.required, removeSpaces]],
      typeOfFood: ["", [Validators.required, removeSpaces]],
      // food_name: ["", []],
      //propritary_name: ["", Validators.required],
      justificationForName: ["", [Validators.required, removeSpaces]],
      proposedProductCategory: [null, [Validators.required, removeSpaces]],
      sourceOfFoodIngradient: ["", [Validators.required, removeSpaces]],
      // genus_species: ["", []],
      functionalUse: ["", [Validators.required, removeSpaces]],
      intendedUse: ["", [Validators.required, removeSpaces]],
      certificateOfAnalysisThirdPartyImg: ["", [Validators.required, requiredFileType('pdf')]],
      //ilac_certificate: ["", Validators.required],
      manufacturingProcessImg: ["", [Validators.required, requiredFileType('pdf')]],
      gstNo: ["", []]
    });

    //********************** CONDITIONAL VALIDATIONS applicationFormStep1 ***********/
    //1. Application for other
    const application_for_con = this.applicationFormStep1.get('applicationfor');
    const specify_con = this.applicationFormStep1.get('specify');
    application_for_con.valueChanges.subscribe(value => {
      if (value === 'Any other non-specified food, please specify') {
        specify_con.setValidators([Validators.required, removeSpaces])
      }
      else {
        specify_con.setValidators([]);
      }
      specify_con.updateValueAndValidity();
    });

    //2. auth person other
    const nameOfAuthorizedPerson = this.applicationFormStep1.get('nameOfAuthorizedPerson');
    const auth_person_other = this.applicationFormStep1.get('auth_person_other');
    nameOfAuthorizedPerson.valueChanges.subscribe(value => {
      if (value === 'Other') {
        auth_person_other.setValidators([Validators.required, removeSpaces])
      }
      else {
        auth_person_other.setValidators([]);
      }
      auth_person_other.updateValueAndValidity();
    });

    //3. food type if any
    const typeOfFood = this.applicationFormStep1.get('typeOfFood');
    const food_name = this.applicationFormStep1.get('food_name');
    typeOfFood.valueChanges.subscribe(value => {
      if (value === 'Proprietary Name, if any') {
        //food_name.setValidators([Validators.required])
      }
      else {
        food_name.setValidators([]);
      }
      food_name.updateValueAndValidity();
    });

    //4.genus_species
    const sourceOfFoodIngradient = this.applicationFormStep1.get('sourceOfFoodIngradient');
    const genus_species = this.applicationFormStep1.get('genus_species');
    sourceOfFoodIngradient.valueChanges.subscribe(value => {
      if (value != 'Chemical') {
        genus_species.setValidators([Validators.required, removeSpaces])
      }
      else {
        genus_species.setValidators([]);
      }
      genus_species.updateValueAndValidity();
    });
    //********************** /CONDITIONAL VALIDATIONS applicationFormStep1 ***********/

    this.applicationFormStep2 = this.formBuilder.group({
      regulatoryStatus: ["", [Validators.required, removeSpaces]],
      regulatoryStatusImg: ["", []],
      country_name: ["", []],
      consumption_purpose: ["", []],
      consumption_purpose_file: ["", [Validators.required, requiredFileType('pdf')]],
      agreement_copy: ["", []],
      copyofAgreement: ["", []],
      safetyInformation: ["", [Validators.required, removeSpaces]],
      safetyInformationImgPath: ["", [Validators.required, requiredFileType('pdf')]],
      evidenceDemo_file: ["", []],
      risk_assessment_review: ["", []],
      safety_studies_conducted: ["", []],
      ingredient_evidence: ["", []],
      additional_info: ["", []],
      target_group: ["", []],
      evidenceDemo_ddl: ["", []]
    });

    /****************CONDITIONAL VALIDATIONS FOR STEP *******************/
    //Regulatory status if other
    const regulatoryStatus = this.applicationFormStep2.get('regulatoryStatus');
    const regulatoryStatusImg = this.applicationFormStep2.get('regulatoryStatusImg');
    regulatoryStatus.valueChanges.subscribe(value => {
      if (value === 'Any Other') {
        regulatoryStatusImg.setValidators([Validators.required, removeSpaces])
      }
      else {
        regulatoryStatusImg.setValidators([]);
      }
      regulatoryStatusImg.updateValueAndValidity();
    });

    this.applicationFormStep3 = this.formBuilder.group({
      technology_details: ["", Validators.required],
    });
    this.applicationFormStep4 = this.formBuilder.group({
      additionalType: ["", [removeSpaces]],
      additionalHistoryOfConsumptionImgPath: ["", [requiredFileType('pdf')]],
      additionalTheTargetGroup: ["", []],
      additionalDetailedCompositionOfProduct: ["", [removeSpaces]],
      additionalDetailsOfNewTechnology: ["", [removeSpaces]],
      additionalSafetyInformation: ["", [removeSpaces]],
      additionalSafetyInformationImgPath: ["", [requiredFileType('pdf')]],
      additionalHistoryOfConsumption: ["", [removeSpaces]],
      newAdditiveChemicalNameAndInsNo: ["", [removeSpaces]],
      newAdditivePurity: ["", [removeSpaces]],
      newAdditiveAcceptableDailyIntake: ["", [removeSpaces]],
      newAdditiveProposedLevelOfUseInFoodCategory: ["", [removeSpaces]],
      newAdditiveInCaseOfColouring: ["", [removeSpaces]],
      newProcessingSpecificationImgPath: ["", [requiredFileType('pdf')]],
      newProcessingEnzymeActivityImgPath: ["", [requiredFileType('pdf')]],
      newProcessingPurityImgPath: ["", [requiredFileType('pdf')]],
      newProcessingResidualLimit: ["", [requiredFileType('pdf')]],

      anyOtherComment: ["", []],
      anyOtherUploadImgPath: ["", []],
      additional_specific_3d_template_file: ["", [requiredFileType('pdf')]],

      risk_assessment_file: ["", []],
      toxicity_studies_file: ["", []],
      allorgenicity: ["", []],
      geographical_area: ["", []],
      consumption_quantity: ["", []],
      chemical_name: ["", []],
      ins_number: ["", []],
      priority: ["", []],
    });
    this.applicationFormStep5 = this.formBuilder.group({
      skuified: ["", Validators.required],
      other_risk_asses: ["", Validators.required],
      proposed_level: ["", Validators.required],
      colour_no: ["", Validators.required],
      applicable: ["", Validators.required],
      specification: ["", Validators.required],
      enzyme_activity: ["", Validators.required],
      resideul_limit: ["", Validators.required],
      priority_new: ["", Validators.required],
      ccc_name: ["", Validators.required],
      reference_no: ["", Validators.required],
      receipt_copy: ["", Validators.required],
    });
    this.applicationFormStep6 = this.formBuilder.group({
      origion_country: ["", Validators.required],
      foreign_org_name: ["", Validators.required],
      reference_no_new: ["", Validators.required],
      receipt_copy1: ["", Validators.required],
      icc_name_address1: ["", Validators.required],
      reference_no_new2: ["", Validators.required],
      receipt1: ["", Validators.required],
      icc_name_address2: ["", Validators.required],
      reference_no_new3: ["", Validators.required],
      receipt2: ["", Validators.required],
    });
    this.applicationFormStep7 = this.formBuilder.group({
      origion_country1: ["", Validators.required],
      foreign_org_name1: ["", Validators.required],
      reference_number: ["", Validators.required],
      receipt3: ["", Validators.required],
      origion_country2: ["", Validators.required],
      iccc_name_address: ["", Validators.required],
      reference_no_new1: ["", Validators.required],
      receipt4: ["", Validators.required],
      material_transfer: ["", Validators.required],
      organism: ["", Validators.required],
      institutional_bio: ["", Validators.required],
      safety_recognised: ["", Validators.required],
      declaration: ["", Validators.required],
    });

  }



  getData() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    //alert(this.current_application_id );
    if (this.current_application_id != '' && this.current_application_id != null) {

      let params = {
        "user_id": this.userData.id,
        "application_form_id": this.current_application_id

      };
      this.spinner.show('dataTableLoader');
      this.apiserviceService.get(APIConstant.GET_APPLICATION_DETAIL, params).subscribe((res) => {

        if (res.data.dataGet) {
          this.setApplicantData(res.data.dataGet);
        } else {
          this.applicationData = null;

        }
        this.spinner.hide('dataTableLoader');
      }, error => {
        this.applicationData = null;

      });
    }

  }
  getPaymentData() {
    let params = {


    };
    this.apiserviceService.get(APIConstant.GET_CONFIGURATION, params).subscribe((res) => {

      if (res.data.dataGet) {
        this.applicationConfig = res.data.dataGet
        // this.setApplicantData(res.data.dataGet);
      } else {
        this.applicationConfig = null;

      }
      this.spinner.hide('dataTableLoader');
    }, error => {
      this.applicationData = null;

    });
  }


  getDataPDF() {

    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    //this.current_application_id = "28";
    // console.log("PDF");
    //  console.log(JSON.parse(this.sessionStorageService.getData('current_application')));

    if (this.userData.id) {

      let params = {
        "user_id": this.userData.id,
        "application_form_id": this.current_application_id,
        // "application-data":[this.applicationFormStep1,this.applicationFormControlStep2, this.applicationFormControlStep4]
      };
      this.spinner.show('dataTableLoader');

      this.apiserviceService.get(APIConstant.GET_APPLICATION_PDF, params).subscribe((res) => {
        // console.log(res.data.dataGet);
        if (res.data.dataGet) {
          this.viewDataPDF = res.data.dataGet;
          //console.log(this.viewDataPDF);
          // this.setApplicantData(res.data.dataGet);
        } else {
          this.applicationData = null;

        }
        this.spinner.hide('dataTableLoader');
      }, error => {
        this.applicationData = null;

      });
    }

  }


  setApplicantData(appDataGet) {
    // alert(3);
    // if(appDataGet.application_form_id){
    //   this.applicationFormStep1.get('application_form_id').setValue(appDataGet.application_form_id);
    // }
    if (appDataGet.payment_status == 1) {
      this.paymentStatus = true;
    }
    if (appDataGet.stepZeroData) {
      this.stepZeroData = JSON.parse(appDataGet.stepZeroData);
      if (this.stepZeroData.itemrows.length) { // For ingredient
        //console.log('Doing it1');
        this.deleterow(0);
        this.stepZeroData.itemrows.forEach(item => {
          this.formArr.push(this.formBuilder.group({
            ingredient_name: [item.ingredient_name, Validators.required],
            ingredient_quantity: [item.ingredient_quantity, Validators.required],
            ingredient_standarizeData: [item.ingredient_standarizeData, Validators.required]
          })
          );
        });
      }

      if (this.stepZeroData.itemrows1.length) { // For Additives
        //console.log('Doing it2');
        this.deleterow1(0);
        this.stepZeroData.itemrows1.forEach(item => {
          this.formArr1.push(this.formBuilder.group({
            additive_name: [item.additive_name, Validators.required],
            additive_quantity: [item.additive_quantity, Validators.required]
          })
          );
        });
      }

    }
    if (appDataGet.applicationfor) {
      this.applicationFormStep1.get('applicationfor').setValue(appDataGet.applicationfor);
    }
    if (appDataGet.specify) {
      this.applicationFormStep1.get('specify').setValue(appDataGet.specify);
    }
    if (appDataGet.emailId) {
      this.applicationFormStep1.get('emailId').setValue(appDataGet.emailId);
    }
    if (appDataGet.applicantName) {
      this.applicationFormStep1.get('applicantName').setValue(appDataGet.applicantName);
    }
    if (appDataGet.nameOfAuthorizedPerson) {
      this.applicationFormStep1.get('nameOfAuthorizedPerson').setValue(appDataGet.nameOfAuthorizedPerson);
    }
    if (appDataGet.auth_person_other) {
      this.applicationFormStep1.get('auth_person_other').setValue(appDataGet.auth_person_other);
    }
    if (appDataGet.genus_species) {
      this.applicationFormStep1.get('genus_species').setValue(appDataGet.genus_species);
    }

    // NABL CERTIFICATE
    if (appDataGet.nabl_certificate_file == '' || appDataGet.nabl_certificate_file == null) {
      this.applicationFormStep1.get('nabl_certificate').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      //this.applicationFormStep1.get('nabl_certificate').clearValidators();
      this.applicationFormStep1.get('certificateOfAnalysisThirdPartyImg').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep1.get('certificateOfAnalysisThirdPartyImg').updateValueAndValidity();
    }
    if (appDataGet.nabl_certificate_file) {
      this.nabl_certificate_file_url = this.uploadUrl + appDataGet.certificateOfAnalysisThirdPartyImg;
    }

    //Manufacturing Pro
    if (appDataGet.manufacturingProcessImg == '' || appDataGet.manufacturingProcessImg == null) {
      this.applicationFormStep1.get('manufacturingProcessImg').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      //this.applicationFormStep1.get('manufacturing_pro').clearValidators();
      this.applicationFormStep1.get('manufacturingProcessImg').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep1.get('manufacturingProcessImg').updateValueAndValidity();
    }
    if (appDataGet.manufacturingProcessImg) {
      this.manufacturing_pro_file_url = this.uploadUrl + appDataGet.manufacturingProcessImg;
    }

    if (appDataGet.mobileNo) {
      this.applicationFormStep1.get('mobileNo').setValue(appDataGet.mobileNo);
    }
    if (appDataGet.organisationName) {
      this.applicationFormStep1.get('organisationName').setValue(appDataGet.organisationName);
    }
    if (appDataGet.organisationAddress) {
      this.applicationFormStep1.get('organisationAddress').setValue(appDataGet.organisationAddress);
    }
    if (appDataGet.licenseNo) {
      this.applicationFormStep1.get('licenseNo').setValue(appDataGet.licenseNo);
    }
    if (appDataGet.natureOfBusiness) {
      this.applicationFormStep1.get('natureOfBusiness').setValue(appDataGet.natureOfBusiness);
    }
    if (appDataGet.nameOfFood) {
      this.applicationFormStep1.get('nameOfFood').setValue(appDataGet.nameOfFood);
    }

    if (appDataGet.typeOfFood) {
      this.applicationFormStep1.get('typeOfFood').setValue(appDataGet.typeOfFood);
    }

    if (appDataGet.food_name) {
      this.applicationFormStep1.get('food_name').setValue(appDataGet.food_name);
    }
    // if(appDataGet.propritary_name){
    //   this.applicationFormStep1.get('propritary_name').setValue(appDataGet.propritary_name);
    // }
    if (appDataGet.justificationForName) {
      this.applicationFormStep1.get('justificationForName').setValue(appDataGet.justificationForName);
    }
    if (appDataGet.proposedProductCategory) {
      this.applicationFormStep1.get('proposedProductCategory').setValue(appDataGet.proposedProductCategory);
    }
    if (appDataGet.sourceOfFoodIngradient) {
      this.applicationFormStep1.get('sourceOfFoodIngradient').setValue(appDataGet.sourceOfFoodIngradient);
    }
    if (appDataGet.functionalUse) {
      this.applicationFormStep1.get('functionalUse').setValue(appDataGet.functionalUse);
    }
    if (appDataGet.intendedUse) {
      this.applicationFormStep1.get('intendedUse').setValue(appDataGet.intendedUse);
    }
    // if(appDataGet.manufacturing_pro){
    //   this.applicationFormStep1.get('manufacturing_pro').setValue(appDataGet.manufacturing_pro);
    // }

    if (appDataGet.country_name) {
      this.applicationFormStep2.get('country_name').setValue(appDataGet.country_name);
    }

    if (appDataGet.regulatoryStatus) {
      this.applicationFormStep2.get('regulatoryStatus').setValue(appDataGet.regulatoryStatus);
    }

    if (appDataGet.regulatoryStatusImg) {
      this.applicationFormStep2.get('regulatoryStatusImg').setValue(appDataGet.regulatoryStatusImg);
    }

    //Consumption(Regulatory status)
    if (appDataGet.consumption_purpose_file == '' || appDataGet.consumption_purpose_file == null) {
      this.applicationFormStep2.get('consumption_purpose_file').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep2.get('consumption_purpose_file').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep2.get('consumption_purpose_file').updateValueAndValidity();
    }
    if (appDataGet.consumption_purpose_file) {
      this.consumption_purpose_file_url = this.uploadUrl + appDataGet.consumption_purpose_file;
    }


    if (appDataGet.agreement_copy_file) {
      this.agreement_copy_file_url = this.uploadUrl + appDataGet.agreement_copy_file;
    }

    //Safety Info file
    if (appDataGet.safetyInformationImgPath == '' || appDataGet.safetyInformationImgPath == null) {
      this.applicationFormStep2.get('safetyInformationImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep2.get('safetyInformationImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep2.get('safetyInformationImgPath').updateValueAndValidity();
    }
    if (appDataGet.safetyInformationImgPath) {
      this.safetyInfo_file_url = this.uploadUrl + appDataGet.safetyInformationImgPath;
    }
    //Safety information (additional_specific_3a_txt4_file)
    if (appDataGet.additionalSafetyInformationImgPath == '' || appDataGet.additionalSafetyInformationImgPath == null) {
      this.applicationFormStep4.get('additionalSafetyInformationImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('additionalSafetyInformationImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('additionalSafetyInformationImgPath').updateValueAndValidity();
    }
    if (appDataGet.additionalSafetyInformationImgPath) {
      this.additional_specific_3a_txt4_file_url = this.uploadUrl + appDataGet.additionalSafetyInformationImgPath;
    }

    //History of consumption (additional_specific_3a_file)
    if (appDataGet.additionalHistoryOfConsumptionImgPath == '' || appDataGet.additionalHistoryOfConsumptionImgPath == null) {
      this.applicationFormStep4.get('additionalHistoryOfConsumptionImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('additionalHistoryOfConsumptionImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('additionalHistoryOfConsumptionImgPath').updateValueAndValidity();
    }
    if (appDataGet.additionalHistoryOfConsumptionImgPath) {
      this.addSpecific_file_url = this.uploadUrl + appDataGet.additionalHistoryOfConsumptionImgPath;
    }

    //Specification (additional_specific_3c_1_file)
    if (appDataGet.newProcessingSpecificationImgPath == '' || appDataGet.newProcessingSpecificationImgPath == null) {
      this.applicationFormStep4.get('newProcessingSpecificationImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('newProcessingSpecificationImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('newProcessingSpecificationImgPath').updateValueAndValidity();
    }
    if (appDataGet.newProcessingSpecificationImgPath) {
      this.addSpecific_3c_1_file_url = this.uploadUrl + appDataGet.newProcessingSpecificationImgPath;
    }

    //Anzyme Activity (additional_specific_3c_2_file)
    if (appDataGet.newProcessingEnzymeActivityImgPath == '' || appDataGet.newProcessingEnzymeActivityImgPath == null) {
      this.applicationFormStep4.get('newProcessingEnzymeActivityImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('newProcessingEnzymeActivityImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('newProcessingEnzymeActivityImgPath').updateValueAndValidity();
    }
    if (appDataGet.newProcessingEnzymeActivityImgPath) {
      this.addSpecific_3c_2_file_url = this.uploadUrl + appDataGet.newProcessingEnzymeActivityImgPath;
    }

    //Purity (additional_specific_3c_3_file)
    if (appDataGet.newProcessingPurityImgPath == '' || appDataGet.newProcessingPurityImgPath == null) {
      this.applicationFormStep4.get('newProcessingPurityImgPath').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('newProcessingPurityImgPath').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('newProcessingPurityImgPath').updateValueAndValidity();
    }
    if (appDataGet.newProcessingPurityImgPath) {
      this.addSpecific_3c_3_file_url = this.uploadUrl + appDataGet.newProcessingPurityImgPath;
    }

    //Residula limit (additional_specific_3c_4_file)
    if (appDataGet.newProcessingResidualLimit == '' || appDataGet.newProcessingResidualLimit == null) {
      this.applicationFormStep4.get('newProcessingResidualLimit').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('newProcessingResidualLimit').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('newProcessingResidualLimit').updateValueAndValidity();
    }
    if (appDataGet.newProcessingResidualLimit) {
      this.addSpecific_3c_4_file_url = this.uploadUrl + appDataGet.newProcessingResidualLimit;
    }

    //Template upload
    if (appDataGet.additional_specific_3d_template_file == '' || appDataGet.additional_specific_3d_template_file == null) {
      this.applicationFormStep4.get('additional_specific_3d_template_file').validator = <any>Validators.compose([Validators.required, requiredFileType('pdf')]);
    } else {
      this.applicationFormStep4.get('additional_specific_3d_template_file').setValidators([requiredFileType('pdf')]);
      this.applicationFormStep4.get('additional_specific_3d_template_file').updateValueAndValidity();
    }
    if (appDataGet.additional_specific_3d_template_file) {
      this.additional_specific_3d_template_file_url = this.uploadUrl + appDataGet.additional_specific_3d_template_file;
    }

    // if(appDataGet.evidenceDemo_file){
    //   this.evidenceDemo_file_url = this.uploadUrl+appDataGet.evidenceDemo_file;
    // }

    if (appDataGet.anyOtherUploadImgPath) {
      this.additional_specific_4d_other_file_url = this.uploadUrl + appDataGet.anyOtherUploadImgPath;
    }

    if (appDataGet.copyofAgreement) {
      this.applicationFormStep2.get('copyofAgreement').setValue(appDataGet.copyofAgreement);
    }

    if (appDataGet.additionalType) {
      this.applicationFormStep4.get('additionalType').setValue(appDataGet.additionalType);
    }

    if (appDataGet.safetyInformation) {
      this.applicationFormStep2.get('safetyInformation').setValue(appDataGet.safetyInformation);
    }

    // if(appDataGet.risk_assessment_review_file){
    //   this.risk_assessment_review_file_url = this.uploadUrl+appDataGet.risk_assessment_review_file;
    // }
    // if(appDataGet.safety_studies_conducted_file){
    //   this.safety_studies_conducted_file_url = this.uploadUrl+appDataGet.safety_studies_conducted_file;
    // }
    // if(appDataGet.ingredient_evidence_file){
    //   this.ingredient_evidence_file_url = this.uploadUrl+appDataGet.ingredient_evidence_file;
    // }
    if (appDataGet.consumption_purpose) {
      this.applicationFormStep2.get('consumption_purpose').setValue(appDataGet.consumption_purpose);
    }
    if (appDataGet.additional_info) {
      this.applicationFormStep2.get('additional_info').setValue(appDataGet.additional_info);
    }
    if (appDataGet.target_group) {
      this.applicationFormStep2.get('target_group').setValue(appDataGet.target_group);
    }

    if (appDataGet.technology_details) {
      this.applicationFormStep3.get('technology_details').setValue(appDataGet.technology_details);
    }

    if (appDataGet.chemical_name) {
      this.applicationFormStep4.get('chemical_name').setValue(appDataGet.chemical_name);
    }
    if (appDataGet.additionalTheTargetGroup) {
      this.applicationFormStep4.get('additionalTheTargetGroup').setValue(appDataGet.additionalTheTargetGroup);
    }
    if (appDataGet.additionalDetailedCompositionOfProduct) {
      this.applicationFormStep4.get('additionalDetailedCompositionOfProduct').setValue(appDataGet.additionalDetailedCompositionOfProduct);
    }
    if (appDataGet.additionalDetailsOfNewTechnology) {
      this.applicationFormStep4.get('additionalDetailsOfNewTechnology').setValue(appDataGet.additionalDetailsOfNewTechnology);
    }
    if (appDataGet.additionalSafetyInformation) {
      this.applicationFormStep4.get('additionalSafetyInformation').setValue(appDataGet.additionalSafetyInformation);
    }
    if (appDataGet.additionalHistoryOfConsumption) {
      this.applicationFormStep4.get('additionalHistoryOfConsumption').setValue(appDataGet.additionalHistoryOfConsumption);
    }
    if (appDataGet.newAdditiveChemicalNameAndInsNo) {
      this.applicationFormStep4.get('newAdditiveChemicalNameAndInsNo').setValue(appDataGet.newAdditiveChemicalNameAndInsNo);
    }
    if (appDataGet.newAdditivePurity) {
      this.applicationFormStep4.get('newAdditivePurity').setValue(appDataGet.newAdditivePurity);
    }
    if (appDataGet.newAdditiveAcceptableDailyIntake) {
      this.applicationFormStep4.get('newAdditiveAcceptableDailyIntake').setValue(appDataGet.newAdditiveAcceptableDailyIntake);
    }
    if (appDataGet.newAdditiveProposedLevelOfUseInFoodCategory) {
      this.applicationFormStep4.get('newAdditiveProposedLevelOfUseInFoodCategory').setValue(appDataGet.newAdditiveProposedLevelOfUseInFoodCategory);
    }
    if (appDataGet.newAdditiveInCaseOfColouring) {
      this.applicationFormStep4.get('newAdditiveInCaseOfColouring').setValue(appDataGet.newAdditiveInCaseOfColouring);
    }

    if (appDataGet.anyOtherComment) {
      this.applicationFormStep4.get('anyOtherComment').setValue(appDataGet.anyOtherComment);
    }

    if (appDataGet.ins_number) {
      this.applicationFormStep4.get('ins_number').setValue(appDataGet.ins_number);
    }
    // if(appDataGet.risk_assessment_file){
    //   this.risk_assessment_file_url = this.uploadUrl+appDataGet.risk_assessment_file;
    // }

    // if(appDataGet.toxicity_studies_file){
    //   this.toxicity_studies_file_url = this.uploadUrl+appDataGet.toxicity_studies_file;
    // }
    // if(appDataGet.allorgenicity_file){
    //   this.allorgenicity_file_url = this.uploadUrl+appDataGet.allorgenicity_file;
    // }
    // if(appDataGet.geographical_area_file){
    //   this.geographical_area_file_url = this.uploadUrl+appDataGet.geographical_area_file;
    // }
    // if(appDataGet.consumption_quantity_file){
    //   this.consumption_quantity_file_url = this.uploadUrl+appDataGet.consumption_quantity_file;
    // }
    if (appDataGet.priority) {
      this.applicationFormStep4.get('priority').setValue(appDataGet.priority);
    }

    if (appDataGet.skuified) {
      this.applicationFormStep5.get('skuified').setValue(appDataGet.skuified);
    }

    if (appDataGet.other_risk_asses) {
      this.applicationFormStep5.get('other_risk_asses').setValue(appDataGet.other_risk_asses);
    }

    if (appDataGet.proposed_level) {
      this.applicationFormStep5.get('proposed_level').setValue(appDataGet.proposed_level);
    }

    if (appDataGet.colour_no) {
      this.applicationFormStep5.get('colour_no').setValue(appDataGet.colour_no);
    }

    if (appDataGet.applicable) {
      this.applicationFormStep5.get('applicable').setValue(appDataGet.applicable);
    }

    if (appDataGet.specification) {
      this.applicationFormStep5.get('specification').setValue(appDataGet.specification);
    }

    if (appDataGet.enzyme_activity) {
      this.applicationFormStep5.get('enzyme_activity').setValue(appDataGet.enzyme_activity);
    }

    if (appDataGet.resideul_limit) {
      this.applicationFormStep5.get('resideul_limit').setValue(appDataGet.resideul_limit);
    }

    if (appDataGet.priority_new) {
      this.applicationFormStep5.get('priority_new').setValue(appDataGet.priority_new);
    }

    if (appDataGet.ccc_name) {
      this.applicationFormStep5.get('ccc_name').setValue(appDataGet.ccc_name);
    }

    if (appDataGet.reference_no) {
      this.applicationFormStep5.get('reference_no').setValue(appDataGet.reference_no);
    }

    if (appDataGet.origion_country) {
      this.applicationFormStep6.get('origion_country').setValue(appDataGet.origion_country);
    }

    if (appDataGet.foreign_org_name) {
      this.applicationFormStep6.get('foreign_org_name').setValue(appDataGet.foreign_org_name);
    }

    if (appDataGet.reference_no_new) {
      this.applicationFormStep6.get('reference_no_new').setValue(appDataGet.reference_no_new);
    }

    if (appDataGet.icc_name_address1) {
      this.applicationFormStep6.get('icc_name_address1').setValue(appDataGet.icc_name_address1);
    }

    if (appDataGet.reference_no_new2) {
      this.applicationFormStep6.get('reference_no_new2').setValue(appDataGet.reference_no_new2);
    }

    if (appDataGet.icc_name_address2) {
      this.applicationFormStep6.get('icc_name_address2').setValue(appDataGet.icc_name_address2);
    }

    if (appDataGet.reference_no_new3) {
      this.applicationFormStep6.get('reference_no_new3').setValue(appDataGet.reference_no_new3);
    }

    if (appDataGet.origion_country1) {
      this.applicationFormStep7.get('origion_country1').setValue(appDataGet.origion_country1);
    }

    if (appDataGet.foreign_org_name1) {
      this.applicationFormStep7.get('foreign_org_name1').setValue(appDataGet.foreign_org_name1);
    }

    if (appDataGet.reference_number) {
      this.applicationFormStep7.get('reference_number').setValue(appDataGet.reference_number);
    }

    if (appDataGet.origion_country2) {
      this.applicationFormStep7.get('origion_country2').setValue(appDataGet.origion_country2);
    }

    if (appDataGet.iccc_name_address) {
      this.applicationFormStep7.get('iccc_name_address').setValue(appDataGet.iccc_name_address);
    }

    if (appDataGet.reference_no_new1) {
      this.applicationFormStep7.get('reference_no_new1').setValue(appDataGet.reference_no_new1);
    }

    if (appDataGet.material_transfer) {
      this.applicationFormStep7.get('material_transfer').setValue(appDataGet.material_transfer);
    }

    if (appDataGet.organism) {
      this.applicationFormStep7.get('organism').setValue(appDataGet.organism);
    }

    if (appDataGet.institutional_bio) {
      this.applicationFormStep7.get('institutional_bio').setValue(appDataGet.institutional_bio);
    }

    if (appDataGet.declaration) {
      this.applicationFormStep7.get('declaration').setValue(appDataGet.declaration);
    }

    // if(appDataGet.receipt_copy_file){
    //   this.receipt_copy_file_url = this.uploadUrl+appDataGet.receipt_copy_file;
    // }

    // if(appDataGet.receipt_copy_file1){
    //   this.receipt_copy_file1_url = this.uploadUrl+appDataGet.receipt_copy_file1;
    // }

    // if(appDataGet.receipt_file1){
    //   this.receipt_file1_url = this.uploadUrl+appDataGet.receipt_file1;
    // }

    // if(appDataGet.receipt_file3){
    //   this.receipt_file3_url = this.uploadUrl+appDataGet.receipt_file3;
    // }

    // if(appDataGet.receipt_copy_file2){
    //   this.receipt_copy_file2_url = this.uploadUrl+appDataGet.receipt_copy_file2;
    // }

    // if(appDataGet.receipt_file4){
    //   this.receipt_file4_url = this.uploadUrl+appDataGet.receipt_file4;
    // }

    // if(appDataGet.safety_recognised){
    //   this.safety_recognised_url = this.uploadUrl+appDataGet.safety_recognised;
    // }
  }



  get formArr() {
    return this.ingredientsForm.get('itemrows') as FormArray;
  }
  get formArr1() {
    return this.ingredientsForm.get('itemrows1') as FormArray;
  }

  onSubmit() {
    //console.log(this.ingredientsForm);
  }
  initItemRows() {
    return this.formBuilder.group({
      ingredient_name: ["", Validators.required],
      ingredient_quantity: ["", Validators.required],
      ingredient_standarizeData: ["", Validators.required]
    })
  }
  addrow() {
    this.formArr.push(this.initItemRows());
  }
  deleterow(index: number) {
    this.formArr.removeAt(index);
  }

  initItemRows1() {
    return this.formBuilder.group({
      additive_name: ["", Validators.required],
      additive_quantity: ["", Validators.required],
    })
  }

  addrow1() {
    this.formArr1.push(this.initItemRows1());
  }
  deleterow1(index: number) {
    this.formArr1.removeAt(index);
  }

  onFilePreview(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName = event.target.files[0];
      this.fileDocumentPreview = this.uploadUrl + "images/zip.png";
      this.myFiles.push(event.target.files[0]);
      this.applicationFormStep1.get('photo').setValue(event.target.files);
    }
  }
  onFilePreview1(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName1 = event.target.files[0];
      this.fileDocumentPreview1 = this.uploadUrl + "images/pdf-icon.png";
      //console.log( event.target.files[0] );
      // this.applicationFormStep1.get('certificateOfAnalysisThirdPartyImg').setValue(event.target.files[0]);
      this.applicationFormStep1.get('certificateOfAnalysisThirdPartyImg').setValue('pdfCert');
    }
  }
  onFilePreview2(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName2 = event.target.files[0];
      this.fileDocumentPreviewName2manufacturing_pro = event.target.files[0];
      this.fileDocumentPreview2manufacturing_pro = this.uploadUrl + "images/pdf-icon.png";
      // this.applicationFormStep1.get('manufacturingProcessImg').setValue(event.target.files[0]);
      this.applicationFormStep1.get('manufacturingProcessImg').setValue('pdfMP');
    }
  }
  onFilePreview3(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName3 = event.target.files[0];
      this.fileDocumentPreview3 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('consumption_purpose_file').setValue(event.target.files[0]);
    }
  }
  onFilePreview4(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName4 = event.target.files[0];
      this.fileDocumentPreview4 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('agreement_copy').setValue(event.target.files[0]);
    }
  }
  onFilePreview5(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName5 = event.target.files[0];
      this.fileDocumentPreview5 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('risk_assessment_review').setValue(event.target.files[0]);
    }
  }
  onFilePreview6(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName6 = event.target.files[0];
      this.fileDocumentPreview6 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('safety_studies_conducted').setValue(event.target.files[0]);
    }
  }
  onFilePreview7(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName7 = event.target.files[0];
      this.fileDocumentPreview7 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('ingredient_evidence').setValue(event.target.files[0]);
    }
  }
  onFilePreview8(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName8 = event.target.files[0];
      this.fileDocumentPreview8 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('risk_assessment_file').setValue(event.target.files[0]);
    }
  }
  onFilePreview9(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName9 = event.target.files[0];
      this.fileDocumentPreview9 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('toxicity_studies_file').setValue(event.target.files[0]);
    }
  }
  onFilePreview10(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName10 = event.target.files[0];
      this.fileDocumentPreview10 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('allorgenicity').setValue(event.target.files[0]);
    }
  }
  onFilePreview11(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName11 = event.target.files[0];
      this.fileDocumentPreview11 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('geographical_area').setValue(event.target.files[0]);
    }
  }
  onFilePreview12(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName12 = event.target.files[0];
      this.fileDocumentPreview12 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('consumption_quantity').setValue(event.target.files[0]);
    }
  }
  onFilePreview13(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName13 = event.target.files[0];
      this.fileDocumentPreview13 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep5.get('receipt_copy').setValue(event.target.files[0]);
    }
  }
  onFilePreview14(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName14 = event.target.files[0];
      this.fileDocumentPreview14 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep6.get('receipt_copy1').setValue(event.target.files[0]);
    }
  }
  onFilePreview15(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName15 = event.target.files[0];
      this.fileDocumentPreview15 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep6.get('receipt1').setValue(event.target.files[0]);
    }
  }
  onFilePreview16(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName16 = event.target.files[0];
      this.fileDocumentPreview16 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep6.get('receipt2').setValue(event.target.files[0]);
    }
  }
  onFilePreview17(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName17 = event.target.files[0];
      this.fileDocumentPreview17 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep7.get('receipt3').setValue(event.target.files[0]);
    }
  }
  onFilePreview18(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName18 = event.target.files[0];
      this.fileDocumentPreview18 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep7.get('receipt4').setValue(event.target.files[0]);
    }
  }
  onFilePreview19(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewName19 = event.target.files[0];
      this.fileDocumentPreview19 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep7.get('safety_recognised').setValue(event.target.files[0]);
    }
  }

  onFilePreviewSafety(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameSafety = event.target.files[0];
      this.fileDocumentPreviewSafety = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('safetyInformationImgPath').setValue(event.target.files[0]);
    }
  }

  onFilePreviewSafetyInfo(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameSafetyInfo = event.target.files[0];
      this.fileDocumentPreviewSafetyInfo = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('additionalSafetyInformationImgPath').setValue(event.target.files[0]);
    }
  }
  onFilePreviewEvidenceDemo(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameEvidenceDemo = event.target.files[0];
      this.fileDocumentPreviewEvidenceDemo = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep2.get('evidenceDemo_file').setValue(event.target.files[0]);
    }
  }
  onFilePreviewAddSpecific(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameAddSpecific = event.target.files[0];
      this.fileDocumentPreviewAddSpecific = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('additionalHistoryOfConsumptionImgPath').setValue(event.target.files[0]);
    }
  }
  onFilePreviewAddSpecific_3c_1(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameAddSpecific_3c_1 = event.target.files[0];
      this.fileDocumentPreviewAddSpecific_3c_1 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('newProcessingSpecificationImgPath').setValue(event.target.files[0]);
    }
  }
  onFilePreviewAddSpecific_3c_2(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameAddSpecific_3c_2 = event.target.files[0];
      this.fileDocumentPreviewAddSpecific_3c_2 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('newProcessingEnzymeActivityImgPath').setValue(event.target.files[0]);
    }
  }
  onFilePreviewAddSpecific_3c_3(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameAddSpecific_3c_3 = event.target.files[0];
      this.fileDocumentPreviewAddSpecific_3c_3 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('newProcessingPurityImgPath').setValue(event.target.files[0]);
    }
  }
  onFilePreviewAddSpecific_3c_4(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameAddSpecific_3c_4 = event.target.files[0];
      this.fileDocumentPreviewAddSpecific_3c_4 = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('newProcessingResidualLimit').setValue(event.target.files[0]);
    }
  }

  onFilePreviewadditional_specific_4d_other_file(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameadditional_specific_4d_other_file = event.target.files[0];
      this.fileDocumentPreviewadditional_specific_4d_other_file = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('anyOtherUploadImgPath').setValue(event.target.files[0]);
    }
  }


  onFilePreviewadditional_specific_3d_template_file(event) {
    if (event.target.files && event.target.files[0]) {
      var filesAmount = event.target.files.length;
      const file = event.target.files[0];
      this.fileDocumentPreviewNameadditional_specific_3d_template_file = event.target.files[0];
      this.fileDocumentPreviewadditional_specific_3d_template_file = this.uploadUrl + "images/pdf-icon.png";
      this.applicationFormStep4.get('additional_specific_3d_template_file').setValue(event.target.files[0]);
    }
  }

  exitStepZero() {
    var checkNo = "";
    var qty_error = false;
    this.stepZeroData = this.ingredientsForm.getRawValue();
    //console.log( JSON.stringify(this.stepZeroData));

    if (this.stepZeroData.itemrows.length) {
      this.stepZeroData.itemrows.forEach(item => {
        if (item.ingredient_name != '' && !parseInt(item.ingredient_quantity)) {
          alert('Please enter Ingredients quantity');
          qty_error = true;
        }

        if (item.ingredient_standarizeData == 'No') {
          this.standarizeData = 'No';
          checkNo += 'no';
        } else {
          this.standarizeData = 'Yes';
          checkNo += 'yes';
        }
      });
    }

    //Additives
    if (this.stepZeroData.itemrows1.length) {
      this.stepZeroData.itemrows1.forEach(item => {
        if (item.additive_name != '' && !parseInt(item.additive_quantity)) {
          alert('Please enter Additives quantity');
          qty_error = true;
        }
      });
    }

    if (qty_error) {
      return false;
    }

    if (checkNo.indexOf('no') >= 0) {
      this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
      let submitDataNewStepOne = new FormData();
      // let submitDataNewStepOne2 = {
      //   refId : 

      // }
      submitDataNewStepOne.append('stepZeroData', JSON.stringify(this.stepZeroData));
      debugger;
      console.log(JSON.stringify(this.stepZeroData));
      console.log(this.stepZeroData);
      submitDataNewStepOne.append('form_step', 'step_0');
      submitDataNewStepOne.append('isDraftClicked', JSON.stringify(this.isDraftClicked));
      // submitDataNewStepOne.append('current_user_id', this.userData.id);----------------
      submitDataNewStepOne.append('application_form_id', this.current_application_id);
      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      // this.apiserviceService.post(APIConstant.POST_INGREDIENT_DETAILS, submitDataNewStepOne, {}).subscribe(result => {});////
      // this.apiserviceService.post(APIConstant.POST_ADDITIVE_DETAILS, submitDataNewStepOne, {}).subscribe(result => {});//////

      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepOne, {}).subscribe(result => {
        this.btnSpinner = false;
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.currentApplication,
        //   JSON.stringify(result.data.dataGet.id)
        // );
        //  console.log();
        if (this.isDraftClicked) {
          setTimeout(() => {
            this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
          }, 1000);
        }

        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });
      return true;
    }
    else {
      if (this.ingredientsForm.untouched) {
        this.isIngredientsFormSubmitted = false;
      }
      else {
        this.isIngredientsFormSubmitted = true;
      }
      // console.log(this.ingredientsForm);
      alert('To apply atleast one item should be non specified/non standarized. You may seek Licence directly from FOSCOS (https://foscos.fssai.gov.in)');
      return false;
    }
    /*
        if (this.standarizeData == 'No') {
          return true;
        } else {
          if (this.ingredientsForm.untouched) {
            this.isIngredientsFormSubmitted = false;
          }
          else {
            this.isIngredientsFormSubmitted = true;
          }
          // console.log(this.ingredientsForm);
          alert("Atleast one item should be Not Standarize")
          return false;
        }
    */


  };
  exitStepOne() {
    var hitButtonClass = (event.target as Element).className;
    if (hitButtonClass.includes('ng-wizard-btn-prev')) {
      return true;
    }

    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep1.valid) {
      // console.log("done with step 1");
      //console.log(this.applicationFormStep1);

      let submitDataNewStepOne = new FormData();
      submitDataNewStepOne.append('stepZeroData', JSON.stringify(this.stepZeroData) );
      submitDataNewStepOne.append('applicationfor', this.applicationFormControlStep1.applicationfor.value);
      // submitDataNewStepOne.append('specify', this.applicationFormControlStep1.specify.value);
      submitDataNewStepOne.append('applicantName', this.applicationFormControlStep1.applicantName.value);
      submitDataNewStepOne.append('nameOfAuthorizedPerson', this.applicationFormControlStep1.nameOfAuthorizedPerson.value);
      // submitDataNewStepOne.append('auth_person_other', this.applicationFormControlStep1.auth_person_other.value);
      submitDataNewStepOne.append('mobileNo', this.applicationFormControlStep1.mobileNo.value);
      submitDataNewStepOne.append('emailId', this.applicationFormControlStep1.emailId.value);
      submitDataNewStepOne.append('organisationName', this.applicationFormControlStep1.organisationName.value);
      submitDataNewStepOne.append('organisationAddress', this.applicationFormControlStep1.organisationAddress.value);
      submitDataNewStepOne.append('licenseNo', this.applicationFormControlStep1.licenseNo.value);
      submitDataNewStepOne.append('natureOfBusiness', this.applicationFormControlStep1.natureOfBusiness.value);
      submitDataNewStepOne.append('nameOfFood', this.applicationFormControlStep1.nameOfFood.value);
      submitDataNewStepOne.append('typeOfFood', this.applicationFormControlStep1.typeOfFood.value);
      // submitDataNewStepOne.append('food_name', this.applicationFormControlStep1.food_name.value);
      //submitDataNewStepOne.append('propritary_name', this.applicationFormControlStep1.propritary_name.value);
      submitDataNewStepOne.append('justificationForName', this.applicationFormControlStep1.justificationForName.value);
      submitDataNewStepOne.append('proposedProductCategory', this.applicationFormControlStep1.proposedProductCategory.value);
      submitDataNewStepOne.append('sourceOfFoodIngradient', this.applicationFormControlStep1.sourceOfFoodIngradient.value);
      // submitDataNewStepOne.append('genus_species', this.applicationFormControlStep1.genus_species.value);
      submitDataNewStepOne.append('functionalUse', this.applicationFormControlStep1.functionalUse.value);
      submitDataNewStepOne.append('intendedUse', this.applicationFormControlStep1.intendedUse.value);
      // submitDataNewStepOne.append('nabl_certificate', this.applicationFormControlStep1.nabl_certificate.value);
      //submitDataNewStepOne.append('ilac_certificate', this.applicationFormControlStep1.ilac_certificate.value);
      submitDataNewStepOne.append('manufacturingProcessImg', this.applicationFormControlStep1.manufacturingProcessImg.value);
      submitDataNewStepOne.append('form_step', 'step_1');
      submitDataNewStepOne.append('isDraftClicked', JSON.stringify(this.isDraftClicked));
      // submitDataNewStepOne.append('current_user_id', this.userData.id);----------------
      // submitDataNewStepOne.append('refId', this.current_application_id);
      submitDataNewStepOne.append('gstNo', this.applicationFormControlStep1.gstNo.value);

      if (this.applicationFormControlStep1.certificateOfAnalysisThirdPartyImg.value) {
        submitDataNewStepOne.append('certificateOfAnalysisThirdPartyImg', this.applicationFormControlStep1.certificateOfAnalysisThirdPartyImg.value);
      } else {
        submitDataNewStepOne.append('certificateOfAnalysisThirdPartyImg', '');
      }

      // if(this.applicationFormControlStep1.ilac_certificate.value){
      //   submitDataNewStepOne.append('ilac_certificate', this.applicationFormControlStep1.ilac_certificate.value);
      // } else {
      //   submitDataNewStepOne.append('ilac_certificate','');
      // }

      if (this.applicationFormControlStep1.manufacturingProcessImg.value) {
        submitDataNewStepOne.append('manufacturingProcessImg', this.applicationFormControlStep1.manufacturingProcessImg.value);
      } else {
        submitDataNewStepOne.append('manufacturingProcessImg', '');
      }


      const officialData = { 'userType': 'user' };

      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      var object = {};
      submitDataNewStepOne.forEach(function(value, key){
    object[key] = value;

});
var json = JSON.stringify(object);
console.log(json)
debugger;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, json, {}).subscribe(result => {
        this.btnSpinner = false;
        // this.sessionStorageService.setData(
        //   customStorage.sessionKeys.currentApplication,
        //   JSON.stringify(result.data.dataGet.id)
        // );
        //console.log('this.isDraftClicked' + this.isDraftClicked );
        if (this.isDraftClicked) {
          setTimeout(() => {
            this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
          }, 1000);
        }
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });


      return true;
    } else {
      //   console.log("not yet completed step 1");
      //   console.log(this.applicationFormStep1);
      //    // Get all Form Controls keys and loop them
      //   Object.keys(this.applicationFormStep1.controls).forEach(key => {
      //   // Get errors of every form control
      //   console.log('error on ' + key);
      //   console.log(this.applicationFormStep1.get(key).errors);
      // });

      if (this.applicationFormStep1.untouched) {
        this.isApplicationFormStep1Submitted = false;
      }
      else {
        this.isApplicationFormStep1Submitted = true;
      }
      return false;
    }
  };
  exitStepTwo() {
    var hitButtonClass = (event.target as Element).className;
    if (hitButtonClass.includes('ng-wizard-btn-prev')) {
      return true;
    }

    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep2.valid) {
      //console.log(this.applicationFormStep2);

      let submitDataNewStepTwo = new FormData();
      submitDataNewStepTwo.append('regulatoryStatus', this.applicationFormControlStep2.regulatoryStatus.value);
      submitDataNewStepTwo.append('regulatoryStatusImg', this.applicationFormControlStep2.regulatoryStatusImg.value);
      submitDataNewStepTwo.append('country_name', this.applicationFormControlStep2.country_name.value);
      submitDataNewStepTwo.append('consumption_purpose', this.applicationFormControlStep2.consumption_purpose.value);
      // submitDataNewStepTwo.append('consumption_purpose_file', this.applicationFormControlStep2.consumption_purpose_file.value);
      //submitDataNewStepTwo.append('agreement_copy', this.applicationFormControlStep2.agreement_copy.value);
      // submitDataNewStepTwo.append('risk_assessment_review', this.applicationFormControlStep2.risk_assessment_review.value);
      // submitDataNewStepTwo.append('safety_studies_conducted', this.applicationFormControlStep2.safety_studies_conducted.value);
      //submitDataNewStepTwo.append('ingredient_evidence', this.applicationFormControlStep2.ingredient_evidence.value);
      submitDataNewStepTwo.append('additional_info', this.applicationFormControlStep2.additional_info.value);
      submitDataNewStepTwo.append('target_group', this.applicationFormControlStep2.target_group.value);
      submitDataNewStepTwo.append('form_step', 'step_2');
      submitDataNewStepTwo.append('isDraftClicked', JSON.stringify(this.isDraftClicked));
      // submitDataNewStepTwo.append('current_user_id', this.userData.id);-----------------------
      // submitDataNewStepTwo.append('refId', this.current_application_id);
      submitDataNewStepTwo.append('copyofAgreement', this.applicationFormControlStep2.copyofAgreement.value);
      submitDataNewStepTwo.append('safetyInformation', this.applicationFormControlStep2.safetyInformation.value);
      submitDataNewStepTwo.append('evidenceDemo_ddl', this.applicationFormControlStep2.evidenceDemo_ddl.value);

      if (this.applicationFormControlStep2.consumption_purpose_file.value) {
        submitDataNewStepTwo.append('consumption_purpose_file', this.applicationFormControlStep2.consumption_purpose_file.value);
      } else {
        submitDataNewStepTwo.append('consumption_purpose_file', '');
      }
      if (this.applicationFormControlStep2.agreement_copy.value) {
        submitDataNewStepTwo.append('agreement_copy', this.applicationFormControlStep2.agreement_copy.value);
      } else {
        submitDataNewStepTwo.append('agreement_copy', '');
      }

      // if(this.applicationFormControlStep2.risk_assessment_review.value){
      //   submitDataNewStepTwo.append('risk_assessment_review', this.applicationFormControlStep2.risk_assessment_review.value);
      // } else {
      //   submitDataNewStepTwo.append('risk_assessment_review','');
      // }

      // if(this.applicationFormControlStep2.safety_studies_conducted.value){
      //   submitDataNewStepTwo.append('safety_studies_conducted', this.applicationFormControlStep2.safety_studies_conducted.value);
      // } else {
      //   submitDataNewStepTwo.append('safety_studies_conducted','');
      // }

      // if(this.applicationFormControlStep2.ingredient_evidence.value){
      //   submitDataNewStepTwo.append('ingredient_evidence', this.applicationFormControlStep2.ingredient_evidence.value);
      // } else {
      //   submitDataNewStepTwo.append('ingredient_evidence','');
      // }

      if (this.applicationFormControlStep2.safetyInformationImgPath.value) {
        submitDataNewStepTwo.append('safetyInformationImgPath', this.applicationFormControlStep2.safetyInformationImgPath.value);
      } else {
        submitDataNewStepTwo.append('safetyInformationImgPath', '');
      }

      if (this.applicationFormControlStep2.evidenceDemo_file.value) {
        submitDataNewStepTwo.append('evidenceDemo_file', this.applicationFormControlStep2.evidenceDemo_file.value);
      } else {
        submitDataNewStepTwo.append('evidenceDemo_file', '');
      }



      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepTwo, {}).subscribe(data => {
        this.btnSpinner = false;
        this.spinner.hide('registrationPopup');
        //console.log( data );
        if (data.payment_status == 1) {
          this.paymentStatus = true;
          this.isDraftClicked = 0;
        }

        if (this.isDraftClicked) {
          setTimeout(() => {
            this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
          }, 1000);
        }

        setTimeout(() => {
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });

      return true;
    } else {

      //    console.log("not yet completed step 2");
      //    // Get all Form Controls keys and loop them
      //   Object.keys(this.applicationFormStep2.controls).forEach(key => {
      //   // Get errors of every form control
      //   console.log('error on ' + key);
      //   console.log(this.applicationFormStep2.get(key).errors);
      // });

      if (this.applicationFormStep2.untouched) {
        this.isApplicationFormStep2Submitted = false;
      }
      else {
        this.isApplicationFormStep2Submitted = true;
      }
      return false;
    }
  };
  exitStepThree() {
    var hitButtonClass = (event.target as Element).className;
    if (hitButtonClass.includes('ng-wizard-btn-prev')) {
      return true;
    }
    //console.log(this.ingredientsForm.getRawValue());
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep3.valid) {
      console.log(this.applicationFormStep3);

      let submitDataNewStepThree = new FormData();
      submitDataNewStepThree.append('technology_details', this.applicationFormControlStep3.technology_details.value);
      submitDataNewStepThree.append('form_step', 'step_3');
      submitDataNewStepThree.append('isDraftClicked', JSON.stringify(this.isDraftClicked));
      // submitDataNewStepThree.append('current_user_id', this.userData.id);----------------------------
      submitDataNewStepThree.append('application_form_id', this.current_application_id);
      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepThree, {}).subscribe(data => {
        this.btnSpinner = false;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });


      return true;
    } else {
      //     console.log("not yet completed step 3");
      //     // Get all Form Controls keys and loop them
      //    Object.keys(this.applicationFormStep2.controls).forEach(key => {
      //    // Get errors of every form control
      //    console.log('error on ' + key);
      //    console.log(this.applicationFormStep2.get(key).errors);
      //  });

      if (this.applicationFormStep3.untouched) {
        this.isApplicationFormStep3Submitted = false;
      }
      else {
        this.isApplicationFormStep3Submitted = true;
      }
      return true;
    }
  };
  exitStepFour() {

    var hitButtonClass = (event.target as Element).className;
    if (hitButtonClass.includes('ng-wizard-btn-prev')) {
      return true;
    }
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep4.valid) {
      //console.log(this.applicationFormStep4);

      let submitDataNewStepFour = new FormData();
      //submitDataNewStepFour.append('risk_assessment_file', this.applicationFormControlStep4.risk_assessment_file.value);
      //submitDataNewStepFour.append('toxicity_studies_file', this.applicationFormControlStep4.toxicity_studies_file.value);
      //submitDataNewStepFour.append('allorgenicity', this.applicationFormControlStep4.allorgenicity.value);
      //submitDataNewStepFour.append('geographical_area', this.applicationFormControlStep4.geographical_area.value);
      // submitDataNewStepFour.append('consumption_quantity', this.applicationFormControlStep4.consumption_quantity.value);
      submitDataNewStepFour.append('additionalTheTargetGroup', this.applicationFormControlStep4.additionalTheTargetGroup.value);
      submitDataNewStepFour.append('additionalDetailedCompositionOfProduct', this.applicationFormControlStep4.additionalDetailedCompositionOfProduct.value);
      submitDataNewStepFour.append('additionalDetailsOfNewTechnology', this.applicationFormControlStep4.additionalDetailsOfNewTechnology.value);
      submitDataNewStepFour.append('additionalSafetyInformation', this.applicationFormControlStep4.additionalSafetyInformation.value);
      submitDataNewStepFour.append('additionalHistoryOfConsumption', this.applicationFormControlStep4.additionalHistoryOfConsumption.value);

      submitDataNewStepFour.append('newAdditiveChemicalNameAndInsNo', this.applicationFormControlStep4.newAdditiveChemicalNameAndInsNo.value);
      submitDataNewStepFour.append('newAdditivePurity', this.applicationFormControlStep4.newAdditivePurity.value);
      submitDataNewStepFour.append('newAdditiveAcceptableDailyIntake', this.applicationFormControlStep4.newAdditiveAcceptableDailyIntake.value);
      submitDataNewStepFour.append('newAdditiveProposedLevelOfUseInFoodCategory', this.applicationFormControlStep4.newAdditiveProposedLevelOfUseInFoodCategory.value);
      submitDataNewStepFour.append('newAdditiveInCaseOfColouring', this.applicationFormControlStep4.newAdditiveInCaseOfColouring.value);
      submitDataNewStepFour.append('chemical_name', this.applicationFormControlStep4.chemical_name.value);
      submitDataNewStepFour.append('ins_number', this.applicationFormControlStep4.ins_number.value);
      submitDataNewStepFour.append('priority', this.applicationFormControlStep4.priority.value);
      submitDataNewStepFour.append('form_step', 'step_4');
      submitDataNewStepFour.append('isDraftClicked', JSON.stringify(this.isDraftClicked));
      // submitDataNewStepFour.append('current_user_id', this.userData.id);-------------------------
      submitDataNewStepFour.append('application_form_id', this.current_application_id);

      //console.log('pay status ' + this.paymentStatus );
      if (this.paymentStatus) {
        submitDataNewStepFour.append('is_final_submit', '1');
      }
      submitDataNewStepFour.append('additionalType', this.applicationFormControlStep4.additionalType.value);
      submitDataNewStepFour.append('additional_specific_4d_other', this.applicationFormControlStep4.additional_specific_4d_other.value);

      if (this.applicationFormControlStep4.risk_assessment_file.value) {
        submitDataNewStepFour.append('risk_assessment_file', this.applicationFormControlStep4.risk_assessment_file.value);
      } else {
        submitDataNewStepFour.append('risk_assessment_file', '');
      }
      if (this.applicationFormControlStep4.additionalHistoryOfConsumptionImgPath.value) {
        submitDataNewStepFour.append('additionalHistoryOfConsumptionImgPath', this.applicationFormControlStep4.additionalHistoryOfConsumptionImgPath.value);
      } else {
        submitDataNewStepFour.append('additionalHistoryOfConsumptionImgPath', '');
      }

      if (this.applicationFormControlStep4.newProcessingSpecificationImgPath.value) {
        submitDataNewStepFour.append('newProcessingSpecificationImgPath', this.applicationFormControlStep4.newProcessingSpecificationImgPath.value);
      } else {
        submitDataNewStepFour.append('newProcessingSpecificationImgPath', '');
      }
      if (this.applicationFormControlStep4.newProcessingEnzymeActivityImgPath.value) {
        submitDataNewStepFour.append('newProcessingEnzymeActivityImgPath', this.applicationFormControlStep4.newProcessingEnzymeActivityImgPath.value);
      } else {
        submitDataNewStepFour.append('newProcessingEnzymeActivityImgPath', '');
      }
      if (this.applicationFormControlStep4.newProcessingPurityImgPath.value) {
        submitDataNewStepFour.append('newProcessingPurityImgPath', this.applicationFormControlStep4.newProcessingPurityImgPath.value);
      } else {
        submitDataNewStepFour.append('newProcessingPurityImgPath', '');
      }
      if (this.applicationFormControlStep4.newProcessingResidualLimit.value) {
        submitDataNewStepFour.append('newProcessingResidualLimit', this.applicationFormControlStep4.newProcessingResidualLimit.value);
      } else {
        submitDataNewStepFour.append('newProcessingResidualLimit', '');
      }

      if (this.applicationFormControlStep4.toxicity_studies_file.value) {
        submitDataNewStepFour.append('toxicity_studies_file', this.applicationFormControlStep4.toxicity_studies_file.value);
      } else {
        submitDataNewStepFour.append('toxicity_studies_file', '');
      }
      if (this.applicationFormControlStep4.allorgenicity.value) {
        submitDataNewStepFour.append('allorgenicity', this.applicationFormControlStep4.allorgenicity.value);
      } else {
        submitDataNewStepFour.append('allorgenicity', '');
      }
      if (this.applicationFormControlStep4.geographical_area.value) {
        submitDataNewStepFour.append('geographical_area', this.applicationFormControlStep4.geographical_area.value);
      } else {
        submitDataNewStepFour.append('geographical_area', '');
      }
      if (this.applicationFormControlStep4.consumption_quantity.value) {
        submitDataNewStepFour.append('consumption_quantity', this.applicationFormControlStep4.consumption_quantity.value);
      } else {
        submitDataNewStepFour.append('consumption_quantity', '');
      }

      if (this.applicationFormControlStep4.additionalSafetyInformationImgPath.value) {
        submitDataNewStepFour.append('additionalSafetyInformationImgPath', this.applicationFormControlStep4.additionalSafetyInformationImgPath.value);
      } else {
        submitDataNewStepFour.append('additionalSafetyInformationImgPath', '');
      }

      if (this.applicationFormControlStep4.anyOtherUploadImgPath.value) {
        submitDataNewStepFour.append('anyOtherUploadImgPath', this.applicationFormControlStep4.anyOtherUploadImgPath.value);
      } else {
        submitDataNewStepFour.append('anyOtherUploadImgPath', '');
      }

      if (this.applicationFormControlStep4.additional_specific_3d_template_file.value) {
        submitDataNewStepFour.append('additional_specific_3d_template_file', this.applicationFormControlStep4.additional_specific_3d_template_file.value);
      } else {
        submitDataNewStepFour.append('additional_specific_3d_template_file', '');
      }


      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepFour, {}).subscribe(data => {
        this.btnSpinner = false;
        this.spinner.hide('registrationPopup');

        if (this.isDraftClicked) {
          setTimeout(() => {
            this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
          }, 1000);
        }

        setTimeout(() => {
          if (this.paymentStatus) {
            this.commonService.redirectToPage('/application-details/' + this.current_application_id);
          }
        }, 500);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });



      return true;
    } else {

      console.log("not yet completed step 4");
      // Get all Form Controls keys and loop them
      //    Object.keys(this.applicationFormStep4.controls).forEach(key => {
      //    // Get errors of every form control
      //    console.log('error on ' + key);
      //    console.log(this.applicationFormStep4.get(key).errors);
      //  });
      if (this.applicationFormStep4.untouched) {
        this.isApplicationFormStep4Submitted = false;
      }
      else {
        this.isApplicationFormStep4Submitted = true;
      }
      return false;
    }
  };
  exitStepFive() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep5.valid) {


      let submitDataNewStepFive = new FormData();
      submitDataNewStepFive.append('skuified', this.applicationFormControlStep5.skuified.value);
      submitDataNewStepFive.append('other_risk_asses', this.applicationFormControlStep5.other_risk_asses.value);
      submitDataNewStepFive.append('proposed_level', this.applicationFormControlStep5.proposed_level.value);
      submitDataNewStepFive.append('colour_no', this.applicationFormControlStep5.colour_no.value);
      submitDataNewStepFive.append('applicable', this.applicationFormControlStep5.applicable.value);
      submitDataNewStepFive.append('specification', this.applicationFormControlStep5.specification.value);
      submitDataNewStepFive.append('enzyme_activity', this.applicationFormControlStep5.enzyme_activity.value);
      submitDataNewStepFive.append('priority_new', this.applicationFormControlStep5.priority_new.value);
      submitDataNewStepFive.append('resideul_limit', this.applicationFormControlStep5.resideul_limit.value);
      submitDataNewStepFive.append('ccc_name', this.applicationFormControlStep5.ccc_name.value);
      submitDataNewStepFive.append('reference_no', this.applicationFormControlStep5.reference_no.value);
      //submitDataNewStepFive.append('receipt_copy', this.applicationFormControlStep5.receipt_copy.value);
      submitDataNewStepFive.append('form_step', 'step_5');
      // submitDataNewStepFive.append('current_user_id', this.userData.id);-----------------------------
      submitDataNewStepFive.append('application_form_id', this.current_application_id);
      if (this.applicationFormControlStep5.receipt_copy.value) {
        submitDataNewStepFive.append('receipt_copy', this.applicationFormControlStep5.receipt_copy.value);
      } else {
        submitDataNewStepFive.append('receipt_copy', '');
      }

      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepFive, {}).subscribe(data => {
        this.btnSpinner = false;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          //this.commonService.redirectToPage(navigationConstants.DASHBOARD);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });




      return true;
    } else {
      if (this.applicationFormStep5.untouched) {
        this.isApplicationFormStep5Submitted = false;
      }
      else {
        this.isApplicationFormStep5Submitted = true;
      }
      return true;
    }
  };
  exitStepSix() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep6.valid) {
      //console.log(this.applicationFormStep6);

      let submitDataNewStepSix = new FormData();
      submitDataNewStepSix.append('origion_country', this.applicationFormControlStep6.origion_country.value);
      submitDataNewStepSix.append('foreign_org_name', this.applicationFormControlStep6.foreign_org_name.value);
      submitDataNewStepSix.append('reference_no_new', this.applicationFormControlStep6.reference_no_new.value);
      //submitDataNewStepSix.append('receipt_copy1', this.applicationFormControlStep6.receipt_copy1.value);
      submitDataNewStepSix.append('icc_name_address1', this.applicationFormControlStep6.icc_name_address1.value);
      submitDataNewStepSix.append('reference_no_new2', this.applicationFormControlStep6.reference_no_new2.value);
      // submitDataNewStepSix.append('receipt1', this.applicationFormControlStep6.receipt1.value);
      submitDataNewStepSix.append('icc_name_address2', this.applicationFormControlStep6.icc_name_address2.value);
      submitDataNewStepSix.append('reference_no_new3', this.applicationFormControlStep6.reference_no_new3.value);
      //submitDataNewStepSix.append('receipt2', this.applicationFormControlStep6.receipt2.value);
      submitDataNewStepSix.append('current_user_id', this.userData.id);
      submitDataNewStepSix.append('application_form_id', this.current_application_id);

      submitDataNewStepSix.append('form_step', 'step_6');

      if (this.applicationFormControlStep6.receipt_copy1.value) {
        submitDataNewStepSix.append('receipt_copy1', this.applicationFormControlStep6.receipt_copy1.value);
      } else {
        submitDataNewStepSix.append('receipt_copy1', '');
      }
      if (this.applicationFormControlStep6.receipt1.value) {
        submitDataNewStepSix.append('receipt1', this.applicationFormControlStep6.receipt1.value);
      } else {
        submitDataNewStepSix.append('receipt1', '');
      }

      if (this.applicationFormControlStep6.receipt2.value) {
        submitDataNewStepSix.append('receipt2', this.applicationFormControlStep6.receipt2.value);
      } else {
        submitDataNewStepSix.append('receipt2', '');
      }

      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepSix, {}).subscribe(data => {
        this.btnSpinner = false;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          // this.commonService.redirectToPage(navigationConstants.LOGIN);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });



      return true;
    } else {
      if (this.applicationFormStep6.untouched) {
        this.isApplicationFormStep6Submitted = false;
      }
      else {
        this.isApplicationFormStep6Submitted = true;
      }
      return true;
    }
  };
  exitStepSeven() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.applicationFormStep7.valid) {
      //console.log(this.applicationFormStep7);

      let submitDataNewStepSeven = new FormData();
      submitDataNewStepSeven.append('origion_country1', this.applicationFormControlStep7.origion_country1.value);
      submitDataNewStepSeven.append('foreign_org_name1', this.applicationFormControlStep7.foreign_org_name1.value);
      submitDataNewStepSeven.append('reference_number', this.applicationFormControlStep7.reference_number.value);
      // submitDataNewStepSeven.append('receipt3', this.applicationFormControlStep7.receipt3.value);
      submitDataNewStepSeven.append('origion_country2', this.applicationFormControlStep7.origion_country2.value);
      submitDataNewStepSeven.append('iccc_name_address', this.applicationFormControlStep7.iccc_name_address.value);
      submitDataNewStepSeven.append('reference_no_new1', this.applicationFormControlStep7.reference_no_new1.value);
      // submitDataNewStepSeven.append('receipt4', this.applicationFormControlStep7.receipt4.value);
      submitDataNewStepSeven.append('material_transfer', this.applicationFormControlStep7.material_transfer.value);
      submitDataNewStepSeven.append('organism', this.applicationFormControlStep7.organism.value);
      submitDataNewStepSeven.append('institutional_bio', this.applicationFormControlStep7.institutional_bio.value);
      //submitDataNewStepSeven.append('safety_recognised', this.applicationFormControlStep7.safety_recognised.value);
      submitDataNewStepSeven.append('declaration', this.applicationFormControlStep7.declaration.value);

      submitDataNewStepSeven.append('form_step', 'step_7');
      submitDataNewStepSeven.append('current_user_id', this.userData.id);
      submitDataNewStepSeven.append('application_form_id', this.current_application_id);
      submitDataNewStepSeven.append('is_final_submit', '1');

      if (this.applicationFormControlStep7.receipt3.value) {
        submitDataNewStepSeven.append('receipt3', this.applicationFormControlStep7.receipt3.value);
      } else {
        submitDataNewStepSeven.append('receipt3', '');
      }
      if (this.applicationFormControlStep7.receipt4.value) {
        submitDataNewStepSeven.append('receipt4', this.applicationFormControlStep7.receipt4.value);
      } else {
        submitDataNewStepSeven.append('receipt4', '');
      }
      if (this.applicationFormControlStep7.safety_recognised.value) {
        submitDataNewStepSeven.append('safety_recognised', this.applicationFormControlStep7.safety_recognised.value);
      } else {
        submitDataNewStepSeven.append('safety_recognised', '');
      }

      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepSeven, {}).subscribe(data => {
        this.btnSpinner = true;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          this.btnSpinner = true;
          this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });


      return true;
    } else {
      if (this.applicationFormStep7.untouched) {
        this.isApplicationFormStep7Submitted = false;
      }
      else {
        this.isApplicationFormStep7Submitted = true;
      }
      return true;
    }
  };
  exitStepEight() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    if (this.paymentStatus) {

      let submitDataNewStepEight = new FormData();
      submitDataNewStepEight.append('form_step', 'step_8');
      submitDataNewStepEight.append('current_user_id', this.userData.id);
      submitDataNewStepEight.append('application_form_id', this.current_application_id);
      submitDataNewStepEight.append('is_final_submit', '1');
      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepEight, {}).subscribe(data => {
        this.btnSpinner = true;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          this.btnSpinner = true;
          this.commonService.redirectToPage('/application-details/' + this.current_application_id);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });


      return true;
    } else {
      if (this.applicationFormStep8.untouched) {
        this.isApplicationFormStep8Submitted = false;
      }
      else {
        this.isApplicationFormStep8Submitted = true;
      }
      return false;
    }
  };

  get ingredientsFormControl() {
    return this.ingredientsForm.controls
  }
  get applicationFormControlStep1() {
    return this.applicationFormStep1.controls
  }
  get applicationFormControlStep2() {
    return this.applicationFormStep2.controls
  }
  get applicationFormControlStep3() {
    return this.applicationFormStep3.controls
  }
  get applicationFormControlStep4() {
    return this.applicationFormStep4.controls
  }
  get applicationFormControlStep5() {
    return this.applicationFormStep5.controls
  }
  get applicationFormControlStep6() {
    return this.applicationFormStep6.controls
  }
  get applicationFormControlStep7() {
    return this.applicationFormStep7.controls
  }

  // onSubmit() {
  //   console.log(this.applicationFormStep1)
  // }
  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }
  showNextStep(event?: Event) {
    this.isDraftClicked = 0;
    this.ngWizardService.next();
  }
  resetWizard(event?: Event) {
    this.ngWizardService.reset();
  }
  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }
  stepChanged(args: StepChangedArgs) {
    //console.log( args.step.index);
    this.currentStep = args.step.index;

    const btnSubmitElement = (<HTMLElement>this.ElByClassName.nativeElement).querySelector(
      '.submit-btn');
    const btnDraftElement = (<HTMLElement>this.ElByClassName.nativeElement).querySelector(
      '.draft-btn');
    btnSubmitElement.setAttribute('disabled', 'disabled');

    if (this.currentStep == 4) {
      btnDraftElement.setAttribute('disabled', 'disabled');
    } else {
      btnDraftElement.removeAttribute('disabled');
    }
    //btnSubmitElement.classList.remove('disabled');

  }
  isValidTypeBoolean: boolean = true;
  isValidFunctionReturnsBoolean(args: StepValidationArgs) {
    return true;
  }
  isValidFunctionReturnsObservable(args: StepValidationArgs) {
    return of(true);
  }
  ingredientddlvalue(data: any) {
    let ingTotalRow = this.formArr.length;
    for (var i = 0; i < ingTotalRow; i++) {

      this.ingredient_ddl = (<HTMLInputElement>document.getElementById('ingredient_ddl' + i)).value;

      // alert(this.ingredient_ddl);
      let ingredient_ddl_value = this.ingredient_ddl;
      if (ingredient_ddl_value) {
        let isPresent = this.ingredientsData.some(function (el: any) {
          return el.name == ingredient_ddl_value;
        });
        if (isPresent == true) {
          // this.standarizeData = 'Yes';
          this.formArr.controls[i].get('ingredient_standarizeData').patchValue('Yes');
        } else {
          // this.standarizeData = 'No';
          this.formArr.controls[i].get('ingredient_standarizeData').patchValue('No');
        }
      } else {
        // this.standarizeData = '';
        this.formArr.controls[i].get('ingredient_standarizeData').patchValue('');
      }
      this.IngredientsStandarizeResult[i] = this.standarizeData;
      //alert(this.standarizeData);
    }
    // console.log(IngredientsStandarizeResult);
    //alert($('input[name="test[]"]').length);
    //console.log(this.ingredientsData);
    // console.log(this.formArr.length+"NG");

  }

  // Specify Appication for if other
  specifyOther(event) {
    if (event.target.value == 'Any other non-specified food, please specify') {
      this.viewApplicationForOther = true;
    } else {
      this.viewApplicationForOther = false;
    }

    if (event.target.value == 'New additives') {
      this.ApplicationForFoodType = 'Additive';
    } else {
      this.ApplicationForFoodType = '';
    }
  }

  // Specify Authorised person if other
  specifyOtherAuthorisedPerson(event) {
    if (event.target.value == 'Other') {
      this.viewAuthorisedPersonOther = true;
    } else {
      this.viewAuthorisedPersonOther = false;
    }
  }

  // Specify food name other
  specifyOtherFoodName(event) {
    if (event.target.value == 'if any') {
      this.viewFoodNameOther = true;
    } else {
      this.viewFoodNameOther = false;
    }
  }

  // Specify Regulatory status other
  specifyOtherRegulatoryStatus(event) {
    if (event.target.value == 'Any Other') {
      this.viewRegulatoryStatus = true;
    } else {
      this.viewRegulatoryStatus = false;
    }
  }

  // Specify genus
  specifyGenus(event) {
    if (event.target.value != 'Chemical') {
      this.viewGenus = true;
    } else {
      this.viewGenus = false;
    }
  }

  // Change safety info tool tip
  // changeSafetyInfoToolTip(event) {
  //   if( event.target.value == 'Risk Assessment'){
  //     this.SafetyInfoToolTip = 'The information shall be based on safety or risk assessment review from published studies and safety studies conducted on the ingredient and food product by the applicant';
  //   }
  //   else if( event.target.value == 'Toxicity Studies'){
  //     this.SafetyInfoToolTip = 'Provide evidence to demonstrate that the proposed product or the ingredient will not adversely affect any specific population groups that is pregnant women, lactating mothers, children, elderly or any other vulnerable group';
  //   }
  //   else{
  //     this.SafetyInfoToolTip = 'Arrange all risk documents chronological  start from the latest';
  //   }
  // }

  // Change safety info 2 tool tip
  changeSafetyInfoToolTip2(event) {
    if (event.target.value == 'Risk Assessment') {
      this.SafetyInfoToolTip2 = '(a) Information on human studies including dietary exposure, nutritional impact and potential impact on the consumer if any';
    }
    else if (event.target.value == 'Toxicity Studies') {
      this.SafetyInfoToolTip2 = '(b) Toxicological studies including results of Ame’s tests to test mutagenecity, chromosomal aberration tests, studies for reproductive toxicity, prenatal developmental toxicity studies.';
    }
    else if (event.target.value == 'Allergenicity') {
      this.SafetyInfoToolTip2 = '(c) Allergenicity (published or unpublished reports of allergenicity or other adverse effects in humans associated with the food consumption; may include reports prepared by World Health Organisation or by other national or international agencies responsible for food safety or public health ).';
    }
    else if (event.target.value == 'Risk Assessment & Toxicity Studies') {
      this.SafetyInfoToolTip2 = '(a) Information on human studies including dietary exposure, nutritional impact and potential impact on the consumer if any.\n (b) Toxicological studies including results of Ame’s tests to test mutagenecity, chromosomal aberration tests, studies for reproductive toxicity, prenatal developmental toxicity studies.';
    }
    else if (event.target.value == 'Risk Assessment & Allergenicity') {
      this.SafetyInfoToolTip2 = '(a) Information on human studies including dietary exposure, nutritional impact and potential impact on the consumer if any.\n (c) Allergenicity (published or unpublished reports of allergenicity or other adverse effects in humans associated with the food consumption; may include reports prepared by World Health Organisation or by other national or international agencies responsible for food safety or public health ).';
    }

    else if (event.target.value == 'Toxicity Studies & Allergenicity') {
      this.SafetyInfoToolTip2 = '(b) Toxicological studies including results of Ame’s tests to test mutagenecity, chromosomal aberration tests, studies for reproductive toxicity, prenatal developmental toxicity studies.\n (c) Allergenicity (published or unpublished reports of allergenicity or other adverse effects in humans associated with the food consumption; may include reports prepared by World Health Organisation or by other national or international agencies responsible for food safety or public health ).';
    }
    else if (event.target.value == 'All of the above') {
      this.SafetyInfoToolTip2 = '(a) Information on human studies including dietary exposure, nutritional impact and potential impact on the consumer if any. \n (b) Toxicological studies including results of Ame’s tests to test mutagenecity, chromosomal aberration tests, studies for reproductive toxicity, prenatal developmental toxicity studies.\n (c) Allergenicity (published or unpublished reports of allergenicity or other adverse effects in humans associated with the food consumption; may include reports prepared by World Health Organisation or by other national or international agencies responsible for food safety or public health ).';
    }
    else {
      this.SafetyInfoToolTip2 = 'Arrange all risk documents chronological  start from the latest';
    }
  }

  //WordLimt
  WordLimit(event) {
    if (event.target.value == "") {
      event.parentNode.find("span.question_word_left").text(200);
      return;
    }
    var words = event.target.value.trim().replace(/\s+/gi, " ").split(" ").length;
    if (words > 200) {
      var trimmed = event.target.value.split(/\s+/, 200).join(" ");
      event.target.value = trimmed + " ";
      return false;
    } else {
      this.words = 200 - words;
    }
  }

  //WordLimt 4. Any other
  WordLimitAnyOther(event) {
    if (event.target.value == "") {
      event.parentNode.find("span.question_word_left").text(200);
      return;
    }
    var words = event.target.value.trim().replace(/\s+/gi, " ").split(" ").length;
    if (words > 200) {
      var trimmed = event.target.value.split(/\s+/, 200).join(" ");
      event.target.value = trimmed + " ";
      return false;
    } else {
      this.wordsAnyOther = 200 - words;
    }
  }

  requiredFileType(type: string) {
    return function (control: FormControl) {
      const file = control.value;
      if (file) {
        var splitArr = file.split('.');
        const lastEle = splitArr[splitArr.length - 1];
        const extension = lastEle.toLowerCase();
        // console.log('extension ' + extension );
        // console.log('type ' + type );
        if (type.toLowerCase() !== extension.toLowerCase()) {
          return {
            requiredFileType: true
          };
        }

        return null;
      }

      return null;
    };
  }
  saveDraft() {
    this.current_application_id = JSON.parse(this.sessionStorageService.getData('current_application'));
    //alert(this.current_application_id );
    if (this.current_application_id != '' && this.current_application_id != null) {
      let submitDataNewStepEight = new FormData();
      submitDataNewStepEight.append('form_step', 'draft');
      submitDataNewStepEight.append('current_user_id', this.userData.id);
      submitDataNewStepEight.append('application_form_id', this.current_application_id);
      submitDataNewStepEight.append('status', '4');
      this.spinner.show('registrationPopup');
      this.btnSpinner = true;
      this.apiserviceService.post(APIConstant.POST_APPLICATION_FORM, submitDataNewStepEight, {}).subscribe(data => {
        this.btnSpinner = true;
        this.spinner.hide('registrationPopup');
        setTimeout(() => {
          this.btnSpinner = true;
          //alert("Saved !!!");
          //this.commonService.redirectToPage(navigationConstants.DASHBOARD_USER);
        }, 2000);
      }, error => {
        this.spinner.hide('registrationPopup');
        this.btnSpinner = false;
      });
    } else {
      this.exitStepZero();
    }

  }
  /**************************PAYMENT************************** */

  options = {
    "key": "rzp_live_WYvRP7xN48DJt8", //"rzp_live_WYvRP7xN48DJt8", "rzp_test_kO54hTLW47teh1"
    //"key": "rzp_test_kO54hTLW47teh1",
    "amount": "5900000",
    "currency": "INR",
    "name": "FSSAI",
    "description": "FSSAI Transaction",
    "image": "http://localhost:4200/assets/images/logo.png",
    "order_id": "", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "handler": this.paymentResponseHander.bind(this),
    "prefill": {
      "name": "",
      "emailId": "",
      "contact": ""
    },
    // "notes": {
    //     "address": "Razorpay Corporate Office"
    // },
    "theme": {
      "color": "#3399cc"
    }
  };

  rzp1;

  pay() {

    //this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }

  createOrder() {
    this.options.prefill.name = this.userData.name;
    this.options.prefill.emailId = this.userData.emailId;
    this.options.prefill.contact = this.userData.contact;
    //var instance = new this.winRef.nativeWindow.Razorpay(this.options)
    //var Razorpay = require('razorpay');
    //var instance = new Razorpay({ key: 'rzp_test_kO54hTLW47teh1', secret: 'HuUNNNB0pXwPyAsEDiucIeSX' });

    var payload = {
      "user_id": this.userData.id,
      "application_id": this.current_application_id,
      "payment_capture": true,
      "amount": this.applicationConfig.payment_amount,
      "currency": "INR",
      "receipt": "Receipt no. " + this.current_application_id,
      "notes": {
        "user_id": this.userData.id,
      }
    };
    this.spinner.show('registrationPopup');
    this.apiserviceService.post(APIConstant.POST_RAZORPAY_ORDER, payload, {}).subscribe((res) => {
      this.spinner.hide('registrationPopup');
      let jsonObject = JSON.parse(res);
      this.options.order_id = jsonObject.id;
      this.pay();
    }, error => {
      console.log('Razorpay order not created');
    });

  }

  paymentResponseHander(response) {
    this.paymentStatus = true;
    const btnSubmitElement = (<HTMLElement>this.ElByClassName.nativeElement).querySelector(
      '.submit-btn');
    btnSubmitElement.removeAttribute('disabled');

    this.apiserviceService.post(APIConstant.RAZORPAY_RESPONSE_STORE, response, {}).subscribe((res) => {
      document.getElementById('paymentBtn').style.display = 'none';
      document.getElementById('successSpan').style.display = 'block';

    }, error => {
    });
  }
}


