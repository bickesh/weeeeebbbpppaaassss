import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTwelveComponent } from './dashboard-twelve.component';

describe('DashboardTwelveComponent', () => {
  let component: DashboardTwelveComponent;
  let fixture: ComponentFixture<DashboardTwelveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashboardTwelveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTwelveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
