import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardTenComponent } from './dashboard-ten.component';

describe('DashboardTenComponent', () => {
  let component: DashboardTenComponent;
  let fixture: ComponentFixture<DashboardTenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardTenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardTenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
