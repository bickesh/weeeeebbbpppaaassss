import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { WindowRefService } from "src/app/services/window-ref.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import * as internal from 'events';

declare var Razorpay: any;

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})


export class PaymentComponent implements OnInit {

  breadcrumbObj={
    title:'Dashboard',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Home',linkURL:'/payment'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  permissionUser:any;

  statusTypeVar:any;
  dataTypeVar:any;
  totalCoast:any;
  roleOfficerDataAd= [];
  adminDashboardList = [];
  DashboardListData = [];
  roleOfficerData = [];
  adminDashboardApplicationHistory=[];
  pendingApplication=0;
  approveApplication=0;
  rejectApplication=0;
  userData:any;
  rejectRemarks='';
  applicationListLabel="Application Details";
  applicationColumnLabel="Current State";
  applicationProcessStatus = [];
  officerNameDec='';
  officerRecomended='';
  approveRemarks='';
  officerName:any='';
  officerRemarks:any='';
  queryRemarks:any='';
  user_id:any='';
  application_id:any='';
  DashboardDataPaginationLoad:any='';
  searchTxt='';
  panelBgColor = 'bg-info';
  officerRoleId:any=0;
  statusLabel:any='';

  form: any = {}; 
  paymentId: string;
  error: string; 

  options = {
    "key": "rzp_test_kO54hTLW47teh1", //"rzp_test_6OjYigAMhNdnj3", // Enter the Key ID generated from the Dashboard
    "amount": "100", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "Acme Corp",
    "description": "Test Transaction",
    "image": "assets/images/logo.png",
    "order_id": "", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    // "handler": function (response){
    //     // alert(response.razorpay_payment_id);
    //     // alert(response.razorpay_order_id);
    //     // alert(response.razorpay_signature);
    //     console.log('FInal output');
    //     console.log(response);
    //     this.RazorPayStoreInDB(response);
        
    // },
    "handler": this.paymentResponseHander.bind(this),
    "prefill": {
        "name": "Gaurav Kumar",
        "email": "gaurav.kumar@example.com",
        "contact": "9999999999"
    },
    // "notes": {
    //     "address": "Razorpay Corporate Office"
    // },
    "theme": {
        "color": "#3399cc"
    }
};
 
rzp1;
  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private winRef: WindowRefService,
    private sessionStorageService: SessionStorageService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.permissionUser=this.commonService.getPermission('dashboard-6',null);
  

  this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
  // this.paginateConfig={ id: 'server', itemsPerPage: 1, currentPage:1, totalItems:0};

  this.userData=JSON.parse(this.sessionStorageService.getData('user'));
  this.officerRoleId =this.userData.role_id
  //console.log(this.userData.role_id);

  }

    ngOnInit(): void { 
    }


 
  pay(){  
    //this.rzp1 = new this.winRef.nativeWindow.Razorpay(this.options);
    this.rzp1 = new Razorpay(this.options);
    this.rzp1.open();
  }
    
  createOrder(){ 
    //var instance = new this.winRef.nativeWindow.Razorpay(this.options)
    //var Razorpay = require('razorpay');
    //var instance = new Razorpay({ key: 'rzp_test_kO54hTLW47teh1', secret: 'HuUNNNB0pXwPyAsEDiucIeSX' });
   
    var payload = {
      "user_id":this.userData.id,
      "application_id":"28",
      "amount": 100,
      "currency": "INR",
      "receipt": "Receipt no. 2",
      "notes": {
        "notes_key_1": "Tea, Earl Grey, Hot",
        "notes_key_2": "Tea, Earl Grey… decaf."
      }
    };
    this.spinner.show('registrationPopup');
    this.apiserviceService.post(APIConstant.POST_RAZORPAY_ORDER, payload, {}).subscribe((res) => {
      this.spinner.hide('registrationPopup');
      let jsonObject = JSON.parse(res); 
      this.options.order_id = jsonObject.id;
      this.pay();
    }, error => { 
      console.log('Razorpay order not created');
    });

   } 

   paymentResponseHander(response) { 
    this.apiserviceService.post(APIConstant.RAZORPAY_RESPONSE_STORE, response, {}).subscribe((res) => {       
    alert('Payment done successfully!');   
    }, error => {  
       });
    }

} 
