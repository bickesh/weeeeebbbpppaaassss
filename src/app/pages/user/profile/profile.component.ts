import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { SessionStorageService } from "src/app/services/session-storage.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  breadcrumbObj = {
    title: 'Manage Profile',
    classBreadcrumb: 'fa-dashboard',
    urls: [
      { urlName: 'User', linkURL: '/user/profile' }
    ]
  }
  private uploadUrl = environment.uploadUrl;
  validationMessage = validationMessage;

  profileForm: FormGroup;
  isProfileFormSubmitted = false;
  userData: any;
  viewPic: any;
  btnSpinner: boolean = false;
  fileData: any;
  userData1:any;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private formBuilder: FormBuilder,
    public sessionStorageService: SessionStorageService,
    private spinner: NgxSpinnerService) {
    this.userData1=JSON.parse(this.sessionStorageService.getData('user'));
     }

  ngOnInit(): void {
    this.profileForm = this.formBuilder.group({
      id: ["", [Validators.required]],
      name: ["", [Validators.required]],
      email: ["", [Validators.required]],
      userimage: ["", []],
    });
    this.getUserDetails();
    this.profileForm.get('email').disable();
  }
  get profileFormControl() { return this.profileForm.controls; }

  onProfileFormSubmit() { 
    this.isProfileFormSubmitted = true;
    if (this.profileForm.invalid) {
      return;
    } else {
      this.btnSpinner = true;
      var userimageData = '';
      if(this.viewPic.indexOf('data:image') !== -1){
        userimageData = this.viewPic;
      } 
      let submitData = {
        'id':this.profileFormControl.id.value,
        'name':this.profileFormControl.name.value,
        'userimage': userimageData // this.viewPic
      }

      // let submitData = new FormData();
      // submitData.append('id', this.profileFormControl.id.value);
      // submitData.append('name', this.profileFormControl.name.value);
      // if (this.profileFormControl.userimage.value) {
      //   submitData.append('userimage', this.profileFormControl.userimage.value);
      // } else {
      //   submitData.append('userimage', '');
      // }
      this.apiserviceService.post(APIConstant.POST_USER_DETAILS, submitData, {}).subscribe(data => {
        this.btnSpinner = false;
        this.userData1.name=this.profileFormControl.name.value;
        this.userData1.userimage=data.pic;

         this.sessionStorageService.setData(
          'user',
          JSON.stringify(this.userData1) 
        );
          //window.location.reload();
      }, error => {
        this.btnSpinner = false;
      });
    }
  }

  getUserDetails() {
    this.apiserviceService.get(APIConstant.USER_DETAILS, {}).subscribe((response) => {
      this.userData = response.data.dataGet;
      this.setUserData(response.data.dataGet);
    }, error => {
    });
  }
  setUserData(userDataGet) {
    if (userDataGet.id) {
      this.profileForm.get('id').setValue(userDataGet.id);
    }
    if (userDataGet.name) {
      this.profileForm.get('name').setValue(userDataGet.name);
    }
    if (userDataGet.email) {
      this.profileForm.get('email').setValue(userDataGet.email);
    }
    if (userDataGet.userimage) {
      this.viewPic = this.uploadUrl + 'public/' + userDataGet.userimage; 
    }
  }

  picChange(event) {
    //   if (event.target.files.length > 0) {
    //     var reader = new FileReader();
    //     reader.readAsDataURL(event.target.files[0]);
    //     reader.onload = _event => {
    //       this.viewPic = reader.result;
    //     };
    //   } else {
    //     this.viewPic = null;
    //   }
    // }
    this.fileData = event.target.files[0];
    if (this.fileData.type == 'image/jpeg' || this.fileData.type == 'image/png' || this.fileData.type == 'image/gif') {
      if (event.target.files.length > 0) {
            var reader = new FileReader();
            reader.readAsDataURL(event.target.files[0]);
            reader.onload = _event => {
              this.viewPic = reader.result;
            };
          } else {
            this.viewPic = null;
          }
    } else {
      alert("File type should be an image (.jpg, .png, .gif).")
      return;
    }
  }
}
