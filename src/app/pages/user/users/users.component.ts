import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators, FormArray} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { PasswordValidator } from 'src/app/customvalidator/password.validator';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Users',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Dashboard',linkURL:'/master/manage-user'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;

  userPermissionForm: FormGroup;
  elementPermissionForm: FormGroup;
  registerForm: FormGroup;
  formEditable:boolean=true;
  isRegisterFormSubmitted = false;
  btnSpinner:boolean=false;
  usersList:any;
  userPermissionData:any=[];
  elementPermissionData:any=[];
  currentuserData:any;
  currentPermissionID:any;
  roles:any;
formLabel:any='';
  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private sessionStorageService: SessionStorageService,
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      id: ["",[]],
      name: ["", Validators.required],
      email: ["", [Validators.required,Validators.pattern(regExConstant.email)]],
      password: ["",[]],
      mobile: ["", [Validators.required,Validators.pattern(regExConstant.mobileNumber)]],
      nature_of_organisation: ["",[]],
      nature_of_business: ["",[]],
      // c_password: ["",[]],
      role_id: ["", Validators.required],
    }
    // , { validator: PasswordValidator("password", "c_password") }
    );

    this.userPermissionForm=this.formBuilder.group({
      userPermissions: this.formBuilder.array([])
    });
    this.elementPermissionForm=this.formBuilder.group({
      elementPermissions: this.formBuilder.array([])
    });
    this.getUsersListList(this.paginateConfig.currentPage);
    this.getRoles();
  }

  get registerFormControl() { return this.registerForm.controls; }

  onRegisterFormSubmit(){

    this.isRegisterFormSubmitted=true;
    if (this.registerForm.invalid) {
      return;
    } else {
      this.spinner.show('dataTableLoader_addUser');
      this.btnSpinner=true;
      let submitData = this.registerForm.getRawValue();
      const officialData = {'userType':'user'};
      this.apiserviceService.postFile(APIConstant.POST_USER,submitData,officialData).subscribe(res=>{
        this.btnSpinner=false;
        this.spinner.hide('dataTableLoader_addUser');
        this.modalService.dismissAll();
        this.getUsersListList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader_addUser');
        this.btnSpinner=false;
      });
    }

    // if(this.registerForm.get('id').value>0){
    //   this.modalService.dismissAll();
    //   this.commonService.notification(this.validationMessage.toasterClass.success, 'data updated successfully');
    // } else {
    // }

  }

  getUsersListList(page: number){
    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage,'userType':'user'};
    this.apiserviceService.get(APIConstant.USER_LIST,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;
      this.paginateConfig.totalItems=res.data.totalItems;
      this.usersList=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;
      this.paginateConfig.totalItems=0;
      this.usersList=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteUser(userObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_USER.replace("{userID}",userObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getUsersListList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });
    // }, function () {
    //   return;
    // });
  }

  viewUser(userObj,modelContent){
    this.formEditable=false;
    this.registerForm.disable();
    this.registerForm.patchValue(userObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetUser();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetUser();
      }
    );
  }

  editUser(userObj,modelContent){
    this.formLabel = "Edit User";
    this.formEditable=true;
    this.registerForm.enable();
    // this.registerForm.get('password').clearValidators();
    // this.registerForm.get('password').updateValueAndValidity();
    // this.registerForm.get('c_password').clearValidators();
    // this.registerForm.get('c_password').updateValueAndValidity();
    // this.registerForm.updateValueAndValidity();

    this.registerForm.patchValue(userObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetUser();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetUser();
      }
    );
  }

  addUser(modelContent) {
    this.formLabel = "Add User";
    this.formEditable=true;
    this.registerForm.enable();
    // this.registerForm.get('password').setValidators([Validators.required]);
    // this.registerForm.get('password').updateValueAndValidity();
    // this.registerForm.get('c_password').setValidators([Validators.required]);
    // this.registerForm.get('c_password').updateValueAndValidity();
    // this.registerForm.updateValueAndValidity();

    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetUser();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetUser();
      }
    );
  }
  resetUser(){
    this.isRegisterFormSubmitted=false;
    this.registerForm.reset();
    this.registerForm.setErrors(null);
    this.registerForm.markAsPristine();
    this.registerForm.markAsUntouched();
    this.registerForm.updateValueAndValidity();
  }
  editPermission(userObj,modelContent){
    this.getuserPermission(userObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetUserPermission();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetUserPermission();
      }
    );
  }
  getuserPermission(userObj){
    this.currentuserData=userObj;
    const apiURL = APIConstant.USER_PERMISSION.replace("{userID}",userObj.id);
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.userPermissionData=res.data.dataGet;
      this.setUserPermissionFormData();
    },error=>{
      this.userPermissionData=[];
    });
  }
  get userPermissionFormArray(): FormArray {
    return this.userPermissionForm.get('userPermissions') as FormArray;
  }
  setUserPermissionFormData(){
    if(this.userPermissionData && this.userPermissionData.length){
      let control = <FormArray>this.userPermissionForm.controls.userPermissions;
      this.userPermissionData.forEach((x,i) => {
        control.push(this.formBuilder.group({
          moduleID: [x.moduleID],
          module_name: [x.module_name],
          submodule_name: [x.submodule_name],
          subModuleID: [x.subModuleID],
          id: [x.id],
          isadd: [x.isadd],
          isedit: [x.isedit],
          isview: [x.isview],
          isdelete: [x.isdelete],
        }));
      });
    }
  }
  resetUserPermission(){
    this.currentuserData=null;
    (this.userPermissionForm.controls['userPermissions'] as FormArray).clear();
    this.userPermissionForm.reset();
    this.userPermissionForm.setErrors(null);
    this.userPermissionForm.markAsPristine();
    this.userPermissionForm.markAsUntouched();
    this.userPermissionForm.updateValueAndValidity();
  }
  elementPermission(rolePermissionObj,modelContent){
    this.getElementPermission(rolePermissionObj);
    this.modalService.open(modelContent,{
      size:"md",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetElementPermission();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetElementPermission();
      }
    );
  }
  getElementPermission(rolePermObj){
    if(rolePermObj.id=='' || rolePermObj.id==null || rolePermObj.id<1 || rolePermObj.id==undefined){
      rolePermObj.id=0;
    }
    if(rolePermObj.moduleID=='' || rolePermObj.moduleID==null || rolePermObj.moduleID<1 || rolePermObj.moduleID==undefined){
      rolePermObj.moduleID=0;
    }
    if(rolePermObj.subModuleID=='' || rolePermObj.subModuleID==null || rolePermObj.subModuleID<1 || rolePermObj.subModuleID==undefined){
      rolePermObj.subModuleID=0;
    }
    this.currentPermissionID=rolePermObj.id;
    const apiURL = APIConstant.GET_ELEMENT_PERMISSION.replace("{permissionID}",rolePermObj.id).replace("{moduleID}",rolePermObj.moduleID).replace("{subModuleID}",rolePermObj.subModuleID);
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.elementPermissionData=res.data.dataGet;
      this.setElementPermissionFormData();
    },error=>{
      this.elementPermissionData=[];
    });
  }
  setElementPermissionFormData(){
    if(this.elementPermissionData && this.elementPermissionData.length){
      let control = <FormArray>this.elementPermissionForm.controls.elementPermissions;
      this.elementPermissionData.forEach((x,i) => {
        control.push(this.formBuilder.group({
          elementID: [x.elementID],
          element_name: [x.element_name],
          id: [x.id],
          isedit: [x.isedit],
          isview: [x.isview],
        }));
      });
    }
  }
  get elementPermissionFormArray(): FormArray {
    return this.elementPermissionForm.get('elementPermissions') as FormArray;
  }
  resetElementPermission(){
    this.currentPermissionID=null;
    (this.elementPermissionForm.controls['elementPermissions'] as FormArray).clear();
    this.elementPermissionForm.reset();
    this.elementPermissionForm.setErrors(null);
    this.elementPermissionForm.markAsPristine();
    this.elementPermissionForm.markAsUntouched();
    this.elementPermissionForm.updateValueAndValidity();
  }
  onUserPermissionFormSubmit(){
    if (this.userPermissionForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData=this.userPermissionForm.getRawValue();
      const apiURL = APIConstant.POST_USER_PERMISSION.replace("{userID}",this.currentuserData.id);
      this.apiserviceService.post(apiURL,submitData,{}).subscribe(response=>{
        this.modalService.dismissAll();
        this.btnSpinner=false;
      },error=>{
        this.btnSpinner=false;
      });
    }

    // console.log(this.userPermissionForm.value);
    // this.modalService.dismissAll();
    // this.commonService.notification(this.validationMessage.toasterClass.success, 'data updated successfully');
  }
  onElementPermissionFormSubmit(){
    if (this.elementPermissionForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData=this.elementPermissionForm.getRawValue();
      const apiURL = APIConstant.POST_ELEMENT_PERMISSION.replace("{permissionID}",this.currentPermissionID);
      this.apiserviceService.post(apiURL,submitData,{}).subscribe(response=>{
        this.modalService.dismissAll();
        this.btnSpinner=false;
      },error=>{
        this.btnSpinner=false;
      });
    }

    // console.log(this.elementPermissionForm.value);
    // this.modalService.dismissAll();
    // this.commonService.notification(this.validationMessage.toasterClass.success, 'data updated successfully');
  }
  onUserPermissionReset(){
    this.btnSpinner=true;
    const apiURL = APIConstant.POST_USER_PERMISSION_RESET.replace("{userID}",this.currentuserData.id);
    this.apiserviceService.post(apiURL,{},{}).subscribe(response=>{
      this.modalService.dismissAll();
      this.btnSpinner=false;
    },error=>{
      this.btnSpinner=false;
    });
  }
  getRoles(){
    const apiURL = APIConstant.GET_ROLE_MASTER;
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.roles=res.data.dataGet;
    },error=>{
      this.roles=[];
    });
  }

}
