import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { PasswordValidator } from 'src/app/customvalidator/password.validator';
import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  breadcrumbObj={
    title:'Change Password',
    classBreadcrumb:'fa-key',
    urls:[
      {urlName:'User',linkURL:'/user/change-password'}
    ]
  }
  validationMessage=validationMessage;


  changePasswordForm: FormGroup;
  isChangePasswordFormSubmitted = false;
  btnSpinner:boolean=false;
  userData:any;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private sessionStorageService: SessionStorageService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService) {      
      this.userData=JSON.parse(this.sessionStorageService.getData(customStorage.sessionKeys.user));
     }

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group({
      id: ["",[Validators.required]],
      oldpassword: ["",[Validators.required]],
      password: ["",[Validators.required]],
      c_password: ["",[Validators.required]],
    }, { validator: PasswordValidator("password", "c_password") });

  }
  get changePasswordFormControl() { return this.changePasswordForm.controls; }

  onChangePasswordFormSubmit(){
    this.isChangePasswordFormSubmitted=true;
    this.changePasswordForm.get('id').patchValue(this.userData.id);
    if (this.changePasswordForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.changePasswordForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.CHANGE_PASSWORD,submitData,{}).subscribe(data=>{
        this.btnSpinner=false;
      },error=>{
        this.btnSpinner=false;
      });
    }
  }

}
