import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users/users.component';
import { ProfileComponent } from './profile/profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { OfficialComponent } from './official/official.component';
const routes: Routes = [
  {path:'',redirectTo:'profile',pathMatch:'full' },
  {path:'profile' , component:ProfileComponent},
  {path:'manage-users' , component:UsersComponent},
  {path:'manage-officials' , component:OfficialComponent},
  {
    path: 'change-password',
    component: ChangePasswordComponent,
    data: {title: 'Change Password'}
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
