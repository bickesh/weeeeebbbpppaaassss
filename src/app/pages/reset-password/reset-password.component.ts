import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from "ngx-spinner";

import { PasswordValidator } from 'src/app/customvalidator/password.validator';

import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";


import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  validationMessage=validationMessage;

  userData:any;
  updatePassword: FormGroup;
  isUpdatePasswordSubmitted = false;
  btnSpinner:boolean=false;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
  ) { 

  }

  ngOnInit(): void {
    this.spinner.show();
    this.updatePassword = this.formBuilder.group({
      password: ["", Validators.required],
      c_password: ["", Validators.required]
    }, { validator: PasswordValidator("password", "c_password") });

    this.getUserDetails();
  }
  get updatePasswordControl() { return this.updatePassword.controls; }
  
  getUserDetails(){
    const tokenNumber = this.route.snapshot.params.tokenNumber;
    let apiUrl=APIConstant.USER_KEY_DETAIL.replace("{tokenNumber}",tokenNumber);
    this.apiserviceService.post(apiUrl,{},{}).subscribe((response)=>{      
      this.userData=response.data.dataGet;
      this.spinner.hide();
    },error=>{
      this.spinner.hide();
      this.commonService.redirectToPage(navigationConstants.LOGIN);
    });
  }

  onUpdatePasswordSubmit(){
    this.isUpdatePasswordSubmitted=true;
    if (this.updatePassword.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let updatePasswordData={
        password:this.updatePasswordControl.password.value,
        c_password:this.updatePasswordControl.c_password.value
      };
      const tokenNumber = this.route.snapshot.params.tokenNumber;
      let apiUrl=APIConstant.UPDATE_PASSWORD.replace("{tokenNumber}",tokenNumber);

      this.apiserviceService.post(apiUrl,updatePasswordData,{}).subscribe(data=>{
        this.btnSpinner=false;
        this.commonService.redirectToPage(navigationConstants.LOGIN);
      },error=>{
        this.btnSpinner=false;
      });
    }
    
  }


}
