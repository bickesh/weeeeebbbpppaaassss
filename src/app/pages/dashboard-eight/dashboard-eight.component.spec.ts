import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardEightComponent } from './dashboard-eight.component';

describe('DashboardEightComponent', () => {
  let component: DashboardEightComponent;
  let fixture: ComponentFixture<DashboardEightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardEightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardEightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
