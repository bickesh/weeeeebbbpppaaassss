import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurationComponent } from './configuration.component';

const routes: Routes = [
  {path:'',redirectTo:'manage_config',pathMatch:'full' },
  {
    path:'config',
    pathMatch:'full',
    component:ConfigurationComponent
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }
