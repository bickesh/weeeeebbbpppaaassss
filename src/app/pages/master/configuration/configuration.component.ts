import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-configuration',
  templateUrl: './configuration.component.html',
  styleUrls: ['./configuration.component.css']
})
export class ConfigurationComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Configuration',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master',linkURL:'/master/configuration'}
    ]
  }
  validationMessage=validationMessage;
  
  configurationForm: FormGroup;
  formEditable:boolean=true;
  isConfigurationFormSubmitted = false;
  btnSpinner:boolean=false;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.configurationForm = this.formBuilder.group({
      token_validity: ["",[Validators.required]],
      max_login_attempt: ["",[Validators.required]],
      log_request_method: ["",[Validators.required]],
      pageLength: ["",[Validators.required]],
    });
    this.getConfigurations();

  }
  get configurationFormControl() { return this.configurationForm.controls; }

  getConfigurations(){
    this.spinner.show('formLoader');
    this.apiserviceService.get(APIConstant.GET_CONFIGURATION,{}).subscribe((res)=>{
      this.spinner.hide('formLoader');
      this.configurationForm.patchValue(res.data.dataGet);
    },error=>{
      this.spinner.hide('formLoader');
    });
  }
  onConfigurationFormSubmit(){
    this.isConfigurationFormSubmitted=true;
    if (this.configurationForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData=this.configurationForm.getRawValue();
      this.apiserviceService.post(APIConstant.POST_CONFIGURATION,submitData,{}).subscribe(response=>{
        this.btnSpinner=false;
        this.commonService.applicationConfiguration();
      },error=>{
        this.btnSpinner=false;        
      });
    }
  }

}
