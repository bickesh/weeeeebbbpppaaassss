import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { SessionStorageService } from 'src/app/services/session-storage.service';
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-elements',
  templateUrl: './elements.component.html',
  styleUrls: ['./elements.component.css']
})
export class ElementsComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Elements',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master',linkURL:'/master/manage-elements'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  
  elementForm: FormGroup;
  searchForm: FormGroup;
  formEditable:boolean=true;
  isElementFormSubmitted = false;
  btnSpinner:boolean=false;
  subModules:any;
  modules:any;
  elements:any;
  timer:any;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) { 
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
  }

  ngOnInit(): void {
    this.elementForm = this.formBuilder.group({
      id:['',[]],
      module_id:['',[Validators.required]],
      submodule_id:['',[Validators.required]],
      element_name:['',[Validators.required]],
      element_code:['',[Validators.required]]
    });
    this.searchForm = this.formBuilder.group({
      module_name:['',[]],
      submodule_name:['',[]],
      element_name:['',[]],
      table_search:['',[]],
    });
    this.getModuleList();
    // this.getSubModuleList();
    this.getElementList(this.paginateConfig.currentPage);
  }

  get elementFormControl() { return this.elementForm.controls; }

  getModuleList(){
    this.apiserviceService.get(APIConstant.GET_MODULE_MASTER,{}).subscribe((res)=>{
      this.modules=res.data.dataGet;
    },error=>{
      this.modules=[];
    });
  }
  onModuleChange(moduleID){
    this.elementForm.get('submodule_id').patchValue(null);
    this.getSubModuleList(moduleID);
  }
  getSubModuleList(moduleID){    
    let params={'module_id':moduleID};
    this.apiserviceService.get(APIConstant.GET_SUBMODULE_MASTER,params).subscribe((res)=>{ 
      this.subModules=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{ 
      this.subModules=[];
      this.spinner.hide('dataTableLoader');
    });
  }
  getElementList(page: number){
    this.spinner.show('dataTableLoader');
    let pagination={'page':page,'pageLength':this.paginateConfig.itemsPerPage};
    let searchData=this.searchForm.getRawValue();
    var params = Object.assign(pagination, searchData);
    this.apiserviceService.get(APIConstant.GET_ELEMENT_MASTER,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;     
      this.paginateConfig.totalItems=res.data.totalItems;     
      this.elements=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;     
      this.paginateConfig.totalItems=0;     
      this.elements=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteElement(elementObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_ELEMENT_MASTER.replace("{elementID}",elementObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getElementList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });      
    // }, function () {  
    //   return;
    // }); 
  }

  viewElement(elementObj,modelContent){
    this.formEditable=false;
    this.elementForm.disable();
    this.getSubModuleList(elementObj.moduleID);
    this.elementForm.patchValue(elementObj);    
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetSubModule();
      }
    );
  }

  editElement(elementObj,modelContent){
    this.formEditable=true;
    this.elementForm.enable();
    this.getSubModuleList(elementObj.moduleID);
    this.elementForm.patchValue(elementObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetSubModule();
      }
    );
  }

  addElement(modelContent) {
    this.formEditable=true;
    this.elementForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetSubModule();
      }
    );
  }
  resetSubModule(){ 
    this.isElementFormSubmitted=false;
    this.subModules=[];
    this.elementForm.reset();
    this.elementForm.setErrors(null);
    this.elementForm.markAsPristine();
    this.elementForm.markAsUntouched();
    this.elementForm.updateValueAndValidity();
  }
  onElementFormSubmit(){
    this.isElementFormSubmitted=true;
    if (this.elementForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.elementForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_ELEMENT_MASTER,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getElementList(this.paginateConfig.currentPage);
      },error=>{
        this.btnSpinner=false;
      });
    }
  }

  inputData(event){
    window.clearTimeout(this.timer); // prevent errant multiple timeouts from being generated
    this.timer = window.setTimeout(() => {
      this.getElementList(1);
    }, 2000);
  }

}
