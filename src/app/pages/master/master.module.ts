import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { MasterRoutingModule } from './master-routing.module';
import { SharedModule } from 'src/app/modules/shared.module';

import { ConfigurationComponent } from './configuration/configuration.component';
import { ModulesComponent } from './modules/modules.component';
import { SubModulesComponent } from './sub-modules/sub-modules.component';
import { RolesComponent } from './roles/roles.component';
import { ElementsComponent } from './elements/elements.component';

@NgModule({
  declarations: [
    ConfigurationComponent,
    ModulesComponent,
    SubModulesComponent,
    RolesComponent,
    ElementsComponent
  ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    SharedModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MasterModule { }
