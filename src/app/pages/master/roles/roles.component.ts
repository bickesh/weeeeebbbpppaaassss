import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators, FormArray} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { SessionStorageService } from "src/app/services/session-storage.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { empty } from 'rxjs';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Roles',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master',linkURL:'/master/manage-roles'}
    ]
  }	
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  
  rolePermissionForm: FormGroup;
  roleElementPermissionForm: FormGroup;
  roleForm: FormGroup;
  formEditable:boolean=true;
  isRoleFormSubmitted = false;
  btnSpinner:boolean=false;
  roles:any;
  rolesPermissionData:any=[];
  rolesElementPermissionData:any=[];
  currentRoleID:any;
  currentPermissionID:any;

  constructor(
    private commonService: CommonService,
    private sessionStorageService: SessionStorageService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
  }

  ngOnInit(): void {
    this.roleForm = this.formBuilder.group({
      id:['',[]],
      role_name:['',[Validators.required]]
    });
    this.rolePermissionForm=this.formBuilder.group({
      rolePermissions: this.formBuilder.array([])
    });
    this.roleElementPermissionForm=this.formBuilder.group({
      elementPermissions: this.formBuilder.array([])
    });
    this.getRoleList(this.paginateConfig.currentPage);
  }

  get roleFormControl() { return this.roleForm.controls; }

  onRoleFormSubmit(){    
    this.isRoleFormSubmitted=true;
    if (this.roleForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.roleForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_ROLE_MASTER,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getRoleList(this.paginateConfig.currentPage);
      },error=>{
        this.btnSpinner=false;
      });
    }

  }

  getRoleList(page: number){
    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage};
    this.apiserviceService.get(APIConstant.GET_ROLE_MASTER,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;     
      this.paginateConfig.totalItems=res.data.totalItems;     
      this.roles=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;     
      this.paginateConfig.totalItems=0;     
      this.roles=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteRole(roleObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_ROLE_MASTER.replace("{roleID}",roleObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getRoleList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });      
    // }, function () {  
    //   return;
    // }); 
  }

  viewRole(roleObj,modelContent){
    this.formEditable=false;
    this.roleForm.disable();
    this.roleForm.patchValue(roleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetRole();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetRole();
      }
    );
  }

  editRole(roleObj,modelContent){
    this.formEditable=true;
    this.roleForm.enable();
    this.roleForm.patchValue(roleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetRole();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetRole();
      }
    );
  }

  addRole(modelContent) {
    this.formEditable=true;
    this.roleForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetRole();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetRole();
      }
    );
  }
  resetRole(){ 
    this.isRoleFormSubmitted=false;
    this.roleForm.reset();
    this.roleForm.setErrors(null);
    this.roleForm.markAsPristine();
    this.roleForm.markAsUntouched();
    this.roleForm.updateValueAndValidity();
  }
  resetRolePermission(){
    this.currentRoleID=null; 
    (this.rolePermissionForm.controls['rolePermissions'] as FormArray).clear();    
    this.rolePermissionForm.reset();
    this.rolePermissionForm.setErrors(null);
    this.rolePermissionForm.markAsPristine();
    this.rolePermissionForm.markAsUntouched();
    this.rolePermissionForm.updateValueAndValidity();
  }
  resetElementPermission(){
    this.currentPermissionID=null;
    (this.roleElementPermissionForm.controls['elementPermissions'] as FormArray).clear(); 
    this.roleElementPermissionForm.reset();
    this.roleElementPermissionForm.setErrors(null);
    this.roleElementPermissionForm.markAsPristine();
    this.roleElementPermissionForm.markAsUntouched();
    this.roleElementPermissionForm.updateValueAndValidity();
  }
  rolePermission(roleObj,modelContent){
    this.getRolePermission(roleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetRolePermission();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetRolePermission();
      }
    );
  }
  elementPermission(rolePermissionObj,modelContent){
    this.getElementPermission(rolePermissionObj);
    this.modalService.open(modelContent,{
      size:"md",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetElementPermission();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetElementPermission();
      }
    );
  }
  getRolePermission(roleObj){
    const apiURL = APIConstant.GET_ROLE_PERMISSION.replace("{roleID}",roleObj.id);
    this.currentRoleID=roleObj.id;
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.rolesPermissionData=res.data.dataGet;
      this.setPermissionFormData();
    },error=>{
      this.rolesPermissionData=[];
    });
  }
  getElementPermission(rolePermObj){    
    if(rolePermObj.id=='' || rolePermObj.id==null || rolePermObj.id<1 || rolePermObj.id==undefined){
      rolePermObj.id=0;
    }
    if(rolePermObj.moduleID=='' || rolePermObj.moduleID==null || rolePermObj.moduleID<1 || rolePermObj.moduleID==undefined){
      rolePermObj.moduleID=0;
    }
    if(rolePermObj.subModuleID=='' || rolePermObj.subModuleID==null || rolePermObj.subModuleID<1 || rolePermObj.subModuleID==undefined){
      rolePermObj.subModuleID=0;
    }
    this.currentPermissionID=rolePermObj.id;
    const apiURL = APIConstant.GET_ELEMENT_PERMISSION.replace("{permissionID}",rolePermObj.id).replace("{moduleID}",rolePermObj.moduleID).replace("{subModuleID}",rolePermObj.subModuleID);
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.rolesElementPermissionData=res.data.dataGet;
      this.setElementPermissionFormData();
    },error=>{
      this.rolesElementPermissionData=[];
    });
  }
  setPermissionFormData(){
    if(this.rolesPermissionData && this.rolesPermissionData.length){
      let control = <FormArray>this.rolePermissionForm.controls.rolePermissions;
      this.rolesPermissionData.forEach((x,i) => {
        control.push(this.formBuilder.group({
          moduleID: [x.moduleID],
          module_name: [x.module_name],
          submodule_name: [x.submodule_name],
          subModuleID: [x.subModuleID],
          id: [x.id],
          isadd: [x.isadd],
          isedit: [x.isedit],
          isview: [x.isview],
          isdelete: [x.isdelete],
        }));
      });
    }
  }
  setElementPermissionFormData(){
    if(this.rolesElementPermissionData && this.rolesElementPermissionData.length){
      let control = <FormArray>this.roleElementPermissionForm.controls.elementPermissions;
      this.rolesElementPermissionData.forEach((x,i) => {
        control.push(this.formBuilder.group({
          elementID: [x.elementID],
          element_name: [x.element_name],
          id: [x.id],
          isedit: [x.isedit],
          isview: [x.isview],
        }));
      });
    }
  }
  get rolePermissionFormArray(): FormArray {
    return this.rolePermissionForm.get('rolePermissions') as FormArray;
  }
  get roleElementPermissionFormArray(): FormArray {
    return this.roleElementPermissionForm.get('elementPermissions') as FormArray;
  }
  onRolePermissionFormSubmit(){
    if (this.rolePermissionForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData=this.rolePermissionForm.getRawValue();
      const apiURL = APIConstant.POST_ROLE_PERMISSION.replace("{roleID}",this.currentRoleID);
      this.apiserviceService.post(apiURL,submitData,{}).subscribe(response=>{
        this.modalService.dismissAll();
        this.btnSpinner=false;
      },error=>{
        this.btnSpinner=false;        
      });
    }
  }
  onElementPermissionFormSubmit(){
    if (this.roleElementPermissionForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData=this.roleElementPermissionForm.getRawValue();
      const apiURL = APIConstant.POST_ELEMENT_PERMISSION.replace("{permissionID}",this.currentPermissionID);
      this.apiserviceService.post(apiURL,submitData,{}).subscribe(response=>{
        this.modalService.dismissAll();
        this.btnSpinner=false;
      },error=>{
        this.btnSpinner=false;        
      });
    }
    
    // console.log(this.roleElementPermissionForm.value);
    // this.modalService.dismissAll();
    // this.commonService.notification(this.validationMessage.toasterClass.success, 'data updated successfully');
  }

}
