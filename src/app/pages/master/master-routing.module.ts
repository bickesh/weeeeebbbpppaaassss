import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfigurationComponent } from './configuration/configuration.component';
import { ModulesComponent } from './modules/modules.component';
import { SubModulesComponent } from './sub-modules/sub-modules.component';
import { RolesComponent } from './roles/roles.component';
import { ElementsComponent } from './elements/elements.component';

const routes: Routes = [
  {path:'',redirectTo:'manage-config',pathMatch:'full' },
  {
    path:'manage-config',
    component:ConfigurationComponent
    // loadChildren: () => import('./configuration/configuration.module').then(m => m.ConfigurationModule) 
  },
  {path:'manage-modules' , component:ModulesComponent},
  {path:'manage-submodules' , component:SubModulesComponent},
  {path:'manage-elements' , component:ElementsComponent},
  {path:'manage-roles' , component:RolesComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }