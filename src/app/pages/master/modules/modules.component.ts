import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";
import { SessionStorageService } from "src/app/services/session-storage.service";
@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.css']
})
export class ModulesComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Modules',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master',linkURL:'/master/manage-modules'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;

  moduleForm: FormGroup;
  formEditable:boolean=true;
  isModuleFormSubmitted = false;
  btnSpinner:boolean=false;
  modules:any;
  moduleTypes:any;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private sessionStorageService: SessionStorageService,
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: this.sessionStorageService.getData(customStorage.sessionKeys.pageLength), currentPage:1, totalItems:0};
  }

  ngOnInit(): void {
    this.moduleForm = this.formBuilder.group({
      id:['',[]],
      moduletype_detailid:['',[Validators.required]],
      module_name:['',[Validators.required]],
      module_code:['',[Validators.required]],
      module_url:['',[Validators.required]],
      module_sequence:['',[Validators.required]],
      module_icon:['',[]]
    });
    this.getModuleTypes();
    this.getModuleList(this.paginateConfig.currentPage);
  }

  get moduleFormControl() { return this.moduleForm.controls; }

  onModuleFormSubmit(){
    this.isModuleFormSubmitted=true;
    if (this.moduleForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.moduleForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_MODULE_MASTER,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getModuleList(this.paginateConfig.currentPage);
      },error=>{
        this.btnSpinner=false;
      });
    }

  }
  getModuleTypes(){
    const apiURL = APIConstant.GET_TYPE_DETAILS.replace("{typeMasterCode}",'menu_type');
    this.apiserviceService.get(apiURL,{}).subscribe((res)=>{
      this.moduleTypes=res.data.dataGet;
    },error=>{
      this.moduleTypes=[];
    });
  }
  getModuleList(page: number){
    let abc=55;
    console.log(abc);
    let dd=this.commonService.encrypt(abc);
    console.log(dd);
    console.log(this.commonService.decrypt(dd) );

    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage,'dd':dd};
    this.apiserviceService.get(APIConstant.GET_MODULE_MASTER,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;
      this.paginateConfig.totalItems=res.data.totalItems;
      this.modules=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;
      this.paginateConfig.totalItems=0;
      this.modules=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteModule(moduleObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_MODULE_MASTER.replace("{moduleID}",moduleObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getModuleList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });
    // }, function () {
    //   return;
    // });
  }

  viewModule(moduleObj,modelContent){
    this.formEditable=false;
    this.moduleForm.disable();
    this.moduleForm.patchValue(moduleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetModule();
      }
    );
  }

  editModule(moduleObj,modelContent){
    this.formEditable=true;
    this.moduleForm.enable();
    this.moduleForm.patchValue(moduleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetModule();
      }
    );
  }

  addModule(modelContent) {
    this.formEditable=true;
    this.moduleForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetModule();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetModule();
      }
    );
  }
  resetModule(){
    this.isModuleFormSubmitted=false;
    this.moduleForm.reset();
    this.moduleForm.setErrors(null);
    this.moduleForm.markAsPristine();
    this.moduleForm.markAsUntouched();
    this.moduleForm.updateValueAndValidity();
  }

}
