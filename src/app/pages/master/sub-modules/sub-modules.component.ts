import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from "ngx-spinner";

import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
import { environment } from "src/environments/environment";

import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/constants/regExConstant';
import { validationMessage } from "src/app/constants/validationMessage";
import { navigationConstants } from "src/app/constants/navigationConstant";
import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-sub-modules',
  templateUrl: './sub-modules.component.html',
  styleUrls: ['./sub-modules.component.css']
})
export class SubModulesComponent implements OnInit {
  breadcrumbObj={
    title:'Manage Submodule',
    classBreadcrumb:'fa-dashboard',
    urls:[
      {urlName:'Master',linkURL:'/master/manage-submodules'}
    ]
  }
  validationMessage=validationMessage;
  closeResult: string;
  paginateConfig:any;
  
  subModuleForm: FormGroup;
  formEditable:boolean=true;
  isSubModuleFormSubmitted = false;
  btnSpinner:boolean=false;
  subModules:any;
  modules:any;

  constructor(
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    private confirmDialogService: ConfirmDialogService,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal
  ) {
    this.paginateConfig={ id: 'server', itemsPerPage: 5, currentPage:0, totalItems:0};
  }

  ngOnInit(): void {
    this.subModuleForm = this.formBuilder.group({
      id:['',[]],
      module_id:['',[Validators.required]],
      submodule_name:['',[Validators.required]],
      submodule_code:['',[Validators.required]],
      submodule_url:['',[Validators.required]],
      submodule_sequence:['',[Validators.required]],
      submodule_icon:['',[]]
    });
    this.getModuleList();
    this.getSubModuleList(1);
  }

  get subModuleFormControl() { return this.subModuleForm.controls; }

  onSubModuleFormSubmit(){    
    this.isSubModuleFormSubmitted=true;
    if (this.subModuleForm.invalid) {
      return;
    } else {
      this.btnSpinner=true;
      let submitData = this.subModuleForm.getRawValue();
      this.apiserviceService.postFile(APIConstant.POST_SUBMODULE_MASTER,submitData,{}).subscribe(res=>{
        this.btnSpinner=false;
        this.modalService.dismissAll();
        this.getSubModuleList(1);
      },error=>{
        this.btnSpinner=false;
      });
    }

  }

  getModuleList(){
    this.apiserviceService.get(APIConstant.GET_MODULE_MASTER,{}).subscribe((res)=>{
      this.modules=res.data.dataGet;
    },error=>{
      this.modules=[];
    });
  }
  getSubModuleList(page: number){
    this.spinner.show('dataTableLoader');
    let params={'page':page,'pageLength':this.paginateConfig.itemsPerPage};
    this.apiserviceService.get(APIConstant.GET_SUBMODULE_MASTER,params).subscribe((res)=>{
      this.paginateConfig.currentPage=res.data.currentPage;     
      this.paginateConfig.totalItems=res.data.totalItems;     
      this.subModules=res.data.dataGet;
      this.spinner.hide('dataTableLoader');
    },error=>{
      this.paginateConfig.currentPage=0;     
      this.paginateConfig.totalItems=0;     
      this.subModules=[];
      this.spinner.hide('dataTableLoader');
    });
  }

  deleteSubModule(subModuleObj){
    // this.confirmDialogService.confirmThis(this.validationMessage.common.deleteConfirmation, function () {
      this.spinner.show('dataTableLoader');
      const apiURL = APIConstant.DELETE_SUBMODULE_MASTER.replace("{submoduleID}",subModuleObj.id);
      this.apiserviceService.delete(apiURL,{}).subscribe(res=>{
        this.spinner.hide('dataTableLoader');
        this.getSubModuleList(this.paginateConfig.currentPage);
      },error=>{
        this.spinner.hide('dataTableLoader');
      });      
    // }, function () {  
    //   return;
    // }); 
  }

  viewSubModule(subModuleObj,modelContent){
    this.formEditable=false;
    this.subModuleForm.disable();
    this.subModuleForm.patchValue(subModuleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetSubModule();
      }
    );
  }

  editSubModule(subModuleObj,modelContent){
    this.formEditable=true;
    this.subModuleForm.enable();
    this.subModuleForm.patchValue(subModuleObj);
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed `;
        this.resetSubModule();
      }
    );
  }

  addSubModule(modelContent) {
    this.formEditable=true;
    this.subModuleForm.enable();
    this.modalService.open(modelContent,{
      size:"lg",
      backdrop : 'static',
      windowClass : "modalClass-700",
      keyboard : false,
      ariaLabelledBy: "modal-basic-title"
    }).result.then(result => {
        this.closeResult = `Closed with: ${result}`;
        this.resetSubModule();
      },reason => {
        this.closeResult = `Dismissed`;
        this.resetSubModule();
      }
    );
  }
  resetSubModule(){ 
    this.isSubModuleFormSubmitted=false;
    this.subModuleForm.reset();
    this.subModuleForm.setErrors(null);
    this.subModuleForm.markAsPristine();
    this.subModuleForm.markAsUntouched();
    this.subModuleForm.updateValueAndValidity();
  }

}
