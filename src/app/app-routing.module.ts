import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { P404Component } from './pages/p404/p404.component';
import { P500Component } from './pages/p500/p500.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
import { ApplicationFormComponent } from './pages/application-form/application-form.component';
import { DashboardTwoComponent } from './pages/dashboard-two/dashboard-two.component';
import { DashboardThreeComponent } from './pages/dashboard-three/dashboard-three.component';
import { DashboardTenComponent } from './pages/dashboard-ten/dashboard-ten.component';

//import{ IngredientsComponent } from './pages/ingredients/ingredients.component';

import { DashboardFiveComponent } from './pages/dashboard-five/dashboard-five.component';
import { DashboardSixComponent } from './pages/dashboard-six/dashboard-six.component';
import { DashboardSevenComponent } from './pages/dashboard-seven/dashboard-seven.component';
import { DashboardEightComponent } from './pages/dashboard-eight/dashboard-eight.component';
import { DashboardNineComponent } from './pages/dashboard-nine/dashboard-nine.component';
import { DashboardElevenComponent } from './pages/dashboard-eleven/dashboard-eleven.component';

import { DashboardTwelveComponent } from './pages/dashboard-twelve/dashboard-twelve.component';
import { ApplicationDetailsComponent } from './pages/application-details/application-details.component';

import { PaymentComponent } from './pages/payment/payment.component';
import { PaymentInvoiceComponent } from './pages/payment-invoice/payment-invoice.component';
import { InvoiceComponent } from './pages/invoice/invoice.component';
import { HomeComponent } from './pages/home/home.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: 'homePage',
    component:HomeComponent

  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'Login Page' }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: { title: 'Register' }
  },
  {
    path: 'reset-password/:tokenNumber',
    component: ResetPasswordComponent,
    data: { title: 'Reset Password' }
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-2',
    component: DashboardTwoComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'dashboard-3',
    component: DashboardThreeComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-5',
    component: DashboardFiveComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-6',
    component: DashboardSixComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-7',
    component: DashboardSevenComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-8',
    component: DashboardEightComponent,
    data: { title: 'Dashboard' }
  },
  {
    //Advisor Dashboard
    path: 'dashboard-9',
    component: DashboardNineComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'dashboard-10',
    component: DashboardTenComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'dashboard-11',
    component: DashboardElevenComponent,
    data: { title: 'Dashboard' }
  },
  {
    path: 'dashboard-12',
    component: DashboardTwelveComponent,
    data: { title: 'Dashboard-JD' }
  },
  {
    path: 'application-form/:param',
    component: ApplicationFormComponent,
    data: { title: 'Application Form' }
  },
  {
    path: 'application-details/:param',
    component: ApplicationDetailsComponent,
    data: { title: 'Application Details' }
  },
  // {
  //   path: 'ingredients',
  //   component: IngredientsComponent,
  //   data: {title: 'Ingredients'}
  // },
  {

    path: 'application-form',
    component: ApplicationFormComponent,
    data: { title: 'Application Form' }
  },
  {

    path: 'payment',
    component: PaymentComponent,
    data: { title: 'Payment Form' }
  },
  // {
  //   path: 'ingredients',
  //   component: IngredientsComponent,
  //   data: {title: 'Ingredients'}
  // },
  {
    path: 'payment-invoice',
    component: PaymentInvoiceComponent,
    data: { title: 'List of Payment Invoice' }
  },
  {
    path: 'invoice',
    component: InvoiceComponent,
    data: { title: 'Invoice' }
  },
  {

    path: "master",
    loadChildren: () => import('./pages/master/master.module').then(m => m.MasterModule)
  },
  {
    path: "report",
    loadChildren: () => import('./pages/reports/reports.module').then(m => m.ReportsModule)
  },
  {
    path: "project",
    loadChildren: () => import('./pages/project/project.module').then(m => m.ProjectModule)
  },
  {
    path: "user",
    loadChildren: () => import('./pages/user/user.module').then(m => m.UserModule)
  },
  {
    path: 'unauthorise',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'error',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: '**',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
