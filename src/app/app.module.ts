import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ToastrModule } from "ngx-toastr";
import { NgxSpinnerModule } from "ngx-spinner";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './modules/shared.module';
import { AngularMaterialModule } from './modules/angular-material.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RecaptchaModule } from 'angular-google-recaptcha';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { NgSelectModule } from '@ng-select/ng-select';

// for intercepter
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RequestInterceptor } from './intercepter/request.interceptor';

// components
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { P404Component } from './pages/p404/p404.component';
import { P500Component } from './pages/p500/p500.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { MenuComponent } from './shared/menu/menu.component';
import { ConfirmDialogComponent } from './shared/confirm-dialog/confirm-dialog.component';
import { ConfirmDialogService } from './services/confirm-dialog.service';
import { RegisterComponent } from './pages/register/register.component';
import { ResetPasswordComponent } from './pages/reset-password/reset-password.component';
// for dateTimepicker
import { OwlDateTimeModule, OwlMomentDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS } from '@danielmoncada/angular-datetime-picker';
import { NgOtpInputModule } from 'ng-otp-input';
import { ApplicationFormComponent } from './pages/application-form/application-form.component';
import { DashboardThreeComponent } from './pages/dashboard-three/dashboard-three.component';
import { DashboardTenComponent } from './pages/dashboard-ten/dashboard-ten.component';

//import { IngredientsComponent } from './pages/ingredients/ingredients.component';

import { DashboardFiveComponent } from './pages/dashboard-five/dashboard-five.component';
import { DashboardSixComponent } from './pages/dashboard-six/dashboard-six.component';
import { DashboardSevenComponent } from './pages/dashboard-seven/dashboard-seven.component';
import { DashboardEightComponent } from './pages/dashboard-eight/dashboard-eight.component';
import { DashboardNineComponent } from './pages/dashboard-nine/dashboard-nine.component';
import { DashboardTwoComponent } from './pages/dashboard-two/dashboard-two.component';
import { DashboardElevenComponent } from './pages/dashboard-eleven/dashboard-eleven.component';
import { DashboardTwelveComponent } from './pages/dashboard-twelve/dashboard-twelve.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ApplicationDetailsComponent } from './pages/application-details/application-details.component';
// import { PaymentComponent } from './pages/payment/payment.component';
import { PaymentInvoiceComponent } from './pages/payment-invoice/payment-invoice.component';
import { InvoiceComponent } from './pages/invoice/invoice.component';
import { HomeComponent } from './pages/home/home.component';


const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};
export const MY_MOMENT_FORMATS = {
  parseInput: 'DD/MM/YYYY HH:mm:ss',
  fullPickerInput: 'DD/MM/YYYY HH:mm:ss',
  datePickerInput: 'DD/MM/YYYY',
  timePickerInput: 'HH:mm:ss',
  monthYearLabel: 'MMM YYYY',
  dateA11yLabel: 'LL',
  monthYearA11yLabel: 'MMMM YYYY',
};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    P404Component,
    P500Component,
    DashboardComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    ConfirmDialogComponent,
    RegisterComponent,
    ResetPasswordComponent,
    ApplicationFormComponent,
    DashboardThreeComponent,
    DashboardTenComponent,
    //IngredientsComponent,


    DashboardFiveComponent,
    DashboardSixComponent,
    DashboardSevenComponent,
    DashboardEightComponent,
    DashboardNineComponent,
    DashboardTwoComponent,
    DashboardElevenComponent,
    DashboardTwelveComponent,
    PaymentComponent,
    ApplicationDetailsComponent,
    // PaymentComponent,
    PaymentInvoiceComponent,
    InvoiceComponent,
    HomeComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    HttpClientModule,
    NgbModule,
    SharedModule,
    NgxPaginationModule,
    NgxSpinnerModule,
    AngularMaterialModule,
    NgxChartsModule,
    NgWizardModule.forRoot(ngWizardConfig),
    RecaptchaModule.forRoot({
      siteKey: '6LfR2xMUAAAAABBy2TBrBKur0_L23TizUGR-M20s',
    }),
    OwlDateTimeModule, OwlNativeDateTimeModule, OwlMomentDateTimeModule,
    NgOtpInputModule,
    NgSelectModule
  ],
  exports: [
    ConfirmDialogComponent,
  ],
  providers: [
    ConfirmDialogService,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_MOMENT_FORMATS }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
