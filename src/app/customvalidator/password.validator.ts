// import { AbstractControl } from '@angular/forms';

// export function PasswordValidator(control: AbstractControl): { [key: string]: boolean } | null {
//   const password = control.get('new_password');
//   const confirmPassword = control.get('confirm_password');
//   if (password.pristine || confirmPassword.pristine) {
//     return null;
//   }
//   return password && confirmPassword && password.value !== confirmPassword.value ? { 'misMatch': true } : null;
// }

import { FormGroup, AbstractControl } from "@angular/forms";

// To validate password and confirm password
export function PasswordValidator(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];

    if (matchingControl.errors && !matchingControl.errors.mustMatch) {
      return;
    }

    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}