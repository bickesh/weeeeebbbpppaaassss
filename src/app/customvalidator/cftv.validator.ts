import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from "@angular/forms";
export function requiredFileType( type: string ) {
    return function (control: FormControl) { 
        const file = control.value; 
        if ( file ) {
          var splitArr =  file.split('.');
          const lastEle = splitArr[splitArr.length - 1];
          const extension = lastEle.toLowerCase();
          console.log('extension ' + extension );
          console.log('type ' + type );
          if ( type.toLowerCase() !== extension.toLowerCase() ) {
            return {
              requiredFileType: true
            };
          }
          
          return null;
        }
    
        return null;
      };
  }