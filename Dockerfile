### STAGE 1: Build ###

FROM node:14.15.0-alpine as builder
WORKDIR /usr/src/app
# Install npm packages
COPY ["package.json", "package-lock.json*", "./"]
RUN npm i @angular/cli --no-progress --loglevel=error
RUN npm i --only=production --no-progress --loglevel=error
# Build
COPY . .
RUN npm run build:app


# Fix Permissions
RUN find /usr/src/app/dist/ -type d -print0 | xargs -0 chmod 755
RUN find /usr/src/app/dist/ -type f -print0 | xargs -0 chmod 644

#SHOW DIR
RUN echo $(ls -1 /usr/src/app/)
RUN echo $(ls -1 /usr/src/app/dist)
RUN echo $(ls -1 /usr/src/app/dist/epaas)


### STAGE 2: Setup ###
FROM nginx:1.18.0-alpine
COPY nginx/default.conf /etc/nginx/conf.d/
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder /usr/src/app/dist /web/dist
#RUN echo "-----------------------------------------Start /web/dist-----------------------------------"
#RUN echo $(ls -1 /web/dist)
#RUN echo "-----------------------------------------End /web/dist-----------------------------------"
RUN mv /web/dist/epaas/index.html /web/dist/epaas/index_busted.html
CMD ["nginx", "-g", "daemon off;"]
